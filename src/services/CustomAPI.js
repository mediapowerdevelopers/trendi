/** @format */
/**
 * TODO: need refactor
 */

import { Config, AppConfig } from "@common";
import { Platform } from "react-native";

/**
 * init class API
 * @param opt
 * @returns {WordpressAPI}
 * @constructor
 */
function WordpressAPI(opt) {
  if (!(this instanceof WordpressAPI)) {
    return new WordpressAPI(opt);
  }
  const newOpt = opt || {};
  this._setDefaultsOptions(newOpt);
}

/**
 * Default option
 * @param opt
 * @private
 */
WordpressAPI.prototype._setDefaultsOptions = async function(opt) {
  this.url = opt.url ? opt.url : AppConfig.WooCommerce.url;
};

WordpressAPI.prototype.createComment = async function(data, callback) {
  let requestUrl = `${this.url}/wp-json/wc/v3/products/reviews/batch`;
  //let requestUrl = 'https://trendi-iq.com/wp-json/wc/v3/products/reviews/batch';
  if (data) {
    //console.log("data",data);
    const myReview = {
      "create": [
         {
           "product_id": data.post_id,
           "review":data.content,
           "reviewer": data.first_name,
           "reviewer_email": data.reviewer_email,
           "rating": data.rating
         }]
      };
      console.log(myReview);
    //const myData = {product_id:data.post_id,review:data.content,rating:data.meta.rating,reviewer:"new user"};
    //requestUrl += `&${this.join(data, "&")}`;
    fetch(requestUrl, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(myReview)
        
      })
      .then((response) => {
       return response;
      });
  }
//  return this._requestReview(requestUrl, data, callback);
};

WordpressAPI.prototype.join = function(obj, separator) {
  const arr = [];
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      arr.push(`${key}=${obj[key]}`);
    }
  }
  return arr.join(separator);
};

WordpressAPI.prototype._request = function(url, callback) {
  const self = this;
  return fetch(url)
    .then((response) => response.text()) // Convert to text instead of res.json()
    .then((text) => {
      if (Platform.OS === "android") {
        text = text.replace(/\r?\n/g, "").replace(/[\u0080-\uFFFF]/g, ""); // If android , I've removed unwanted chars.
      }
      return text;
    })
    .then((response) => JSON.parse(response))

    .catch((error, data) => {
      // console.log('1=error network -', error, data);
    })
    .then((responseData) => {
      if (typeof callback === "function") {
        callback();
      }
      // console.log('request result from ' + url, responseData);

      return responseData;
    })
    .catch((error) => {
      // console.log('2=error network -- ', error.message);
    });
};

export default new WordpressAPI();
