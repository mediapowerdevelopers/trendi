/**
 * Created by InspireUI on 01/03/2017.
 * An API for JSON API Auth Word Press plugin.
 * https://wordpress.org/plugins/json-api-auth/
 *
 * @format
 */

import { AppConfig } from "@common";
import { request, error } from "./../Omni";

const url = AppConfig.WooCommerce.url;
const isSecured = url.startsWith("https");
const secure = isSecured ? "" : "&insecure=cool";
const cookieLifeTime = 120960000000;

const WPUserAPI = {
  login: async (username, password) => {
    const _url = `${url}/api/mstore_user/generate_auth_cookie/?second=${cookieLifeTime}&username=${username}&password=${password}${secure}`;
    console.log(_url);
    return await request(_url);
  },
  loginFacebook: async (token) => {
    const _url = `${url}/api/mstore_user/fb_connect/?second=${cookieLifeTime}&access_token=${token}${secure}`;
    return await request(_url);
  },
  loginSMS: async (token) => {
    const _url = `${url}/api/mstore_user/sms_login/?access_token=${token}${secure}`;
    return await request(_url);
  },
  
  register: async ({
    username,
    email,
    firstName,
    lastName,
    password = undefined,
  }) => {
  console.log(`${url}/wp-json/wc/v3/customers`);  
  const response =    await  fetch(`${url}/wp-json/wc/v3/customers`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email:email,
          password: password,
          first_name:firstName,
          last_name:lastName,
          username:`Trendi ${username}`
        })
      })
     
      return await response.json();
   
  },

  // register: async ({
  //   username,
  //   email,
  //   firstName,
  //   lastName,
  //   password = undefined,
  // }) => {
  //   try {
  //     const nonce = await WPUserAPI.getNonce();
  //     const _url =
  //       `${`${url}/api/mstore_user/register/?` +
  //       `username=${username}` +
  //       `&user_login=${username}` +
  //       `&email=${email}` +
  //       `&user_email=${email}` +
  //       `&display_name=${`${firstName}+${lastName}`}` +
  //       `&first_name=${firstName}` +
  //       `&last_name=${lastName}`}${
  //       password ? `&user_pass=${password}` : ""
  //       }&nonce=${nonce}` + `&notify=both${secure}`;
  //       console.log(_url);
  //     return await request(_url);
  //   } catch (err) {
  //     error(err);
  //     return { error: err };
  //   }
  // },
  getNonce: async () => {
    const _url = `${url}/api/get_nonce/?controller=mstore_user&method=register`;
    //console.log(_url);
    const json = await request(_url);
    return json && json.nonce;
  },
};

export default WPUserAPI;
