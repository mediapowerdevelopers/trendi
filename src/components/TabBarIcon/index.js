/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { View, StyleSheet, Text, Image, I18nManager } from "react-native";
import { Styles, Color } from "@common";
import { connect } from "react-redux";

class TabBarIcon extends PureComponent {
  static propTypes = {
    icon: PropTypes.any,
    tintColor: PropTypes.string,
    css: PropTypes.any,
    carts: PropTypes.object,
    cartIcon: PropTypes.any,
    wishList: PropTypes.any,
    wishlistIcon: PropTypes.any,
  };

  render() {
    const {
      icon,
      tintColor,
      css,
      carts,
      cartIcon,
      wishList,
      wishlistIcon,
      title
    } = this.props;

    const numberWrap = (number = 0) => (
      <View style={styles.numberWrap}>
        <Text style={styles.number}>{number}</Text>
      </View>
    );
    const numberWrap2 = (number = 0) => (
      <View style={styles.numberWrap2}>
        <Text style={styles.number}>{number}</Text>
      </View>
    );
    return (
      
      <View style={{ alignItems:'center'}}>
      
        <Image
          ref={(comp) => (this._image = comp)}
          source={icon}
          style={[styles.icon, { tintColor }, css]}
        />
        <Text style={{ fontSize:10,textAlign:'center',color:tintColor}}>{title}</Text>
        {wishlistIcon && wishList.total > 0 && numberWrap(wishList.total || 0)}
        {cartIcon && carts.total > 0 && numberWrap2(carts.total || 0)}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  icon: {
    width: 20,
    height: 20,
    
    //resizeMode: "contain",
  },
  numberWrap: {
    ...Styles.Common.ColumnCenter,
    position: "absolute",
    top: -8,
    right: -1,
    height: 16,
    minWidth: 16,
    backgroundColor: Color.error,
    borderRadius: 8,
  },
  numberWrap2: {
    ...Styles.Common.ColumnCenter,
    position: "absolute",
    top: -8,
    right: I18nManager.isRTL?9:-6,
    height: 16,
    minWidth: 16,
    backgroundColor: Color.error,
    borderRadius: 8,
  },
  number: {
    color: "white",
    fontSize: 10,
    marginLeft: 3,
    marginRight: 3,
  }
});

const mapStateToProps = ({ carts, wishList }) => ({ carts, wishList });
export default connect(
  mapStateToProps,
  null,
  null
)(TabBarIcon);
