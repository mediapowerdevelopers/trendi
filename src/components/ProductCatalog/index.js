/** @format */

import React from "react";
import { View, Text, I18nManager } from "react-native";

import { Languages, withTheme } from "@common";
import styles from "./style";
import Item from "../ChipItem";

class ProductCatalog extends React.PureComponent {
  state = {
    selectedId: -1,
  };

  static defaultProps = {
    categories: [],
  };

  render() {
    const { categories } = this.props;
    const { selectedId } = this.state;
    const {
      theme: {
        colors: { text },
      },
    } = this.props;
    const newCategories = categories.filter(item => {
      if( item.id == 1790 || item.id == 1794 || item.id == 1795 || item.id == 1796 || item.id == 1789 || item.id == 1791 || item.id == 1792 || item.id == 1793 || item.id == 432 || item.id == 433 || item.id == 434 || item.id == 435 || item.id == 15 || item.id == 1884) {
      } else {
        return item;
      }
    })
    return (
      <View>
        <View style={styles.header}>
          <Text style={[styles.text, { color: text }]}>
            {Languages.ProductCatalog}
          </Text>
        </View>

        <View style={styles.container}>
          {newCategories.map((item, index) => {
            
            if( item && typeof item != "undefined" && typeof item.name!=="undefined"){
              var t = item.name.split("|");
              titleEn = t[0];
              if( I18nManager.isRTL ) {
                if( t[1] && t[1] != "" && typeof t[1] != "undefined" ) {
                  titleAr = t[1];
                } else return;
                if( t[2] && t[2] != "" && typeof t[2] != "undefined" ) {
                  titleKr = t[2];
                } else return;
              }

            }else{
              titleEn = "";
              titleAr = "";
              titleKr = "";
            }
            
            return (
            <Item
              item={item}
              key={index}
              label={I18nManager.isRTL?(Languages._language==='ar'?titleAr:titleKr):titleEn}
              onPress={this.onPress}
              selected={selectedId == item.id}
            />
          )})}
        </View>
      </View>
    );
  }

  onPress = (item) => {
    this.setState({ selectedId: item.id });
    this.props.onSelectCategory(item);
  };
}

export default withTheme(ProductCatalog);
