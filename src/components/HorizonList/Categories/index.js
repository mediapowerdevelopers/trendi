/** @format */

import React, { PureComponent } from "react";
import { FlatList } from "react-native";
import { withTheme } from "@common";
import Item from "./Item";

class Categories extends PureComponent {
  static defaultProps = {
    categories: [],
    items: [],
  };

  render() {
    const { categories, items, type, onPress, onKidPress, config, isKid, kidChildren } = this.props;
    const mapping = {};
    const image_mapping = {};
    
    categories.forEach((item) => {
      //console.log("itemmmmmmmmmmmm",item);
      mapping[item.id] = item.name;
      let catImage = item.image && (typeof item.image !="undefined")?item.image.src:"" 
      image_mapping[item.id] = catImage;
      //console.log(catImage);
    });

    // categories.forEach((item) => {
    //   //console.log("itemmmmmmmmmmmm",item.image[0]);
    //   image_mapping[item.image] = item.image;
    // });
    const column = typeof config.column !== 'undefined' ? config.column : 1

    return categories.length > 0 && <FlatList
    keyExtractor={(item, index) => `${index}`}
    contentContainerStyle={styles.flatlist}
    showsHorizontalScrollIndicator={false}
    extraData={this.props.items}
    horizontal={column === 1}
    numColumns={column}

    data={items}
    renderItem={({ item }) => {
      return (
        <Item item={item} isKid={isKid} kidChildren={kidChildren} type={type} label={mapping[item.category]} image={image_mapping[item.category]} onPress={onPress} onKidPress={onKidPress}  />
      )
    }}
  />
  }
}

const styles = {
  flatlist: {
    marginBottom: 10,
  }
}

export default withTheme(Categories);
