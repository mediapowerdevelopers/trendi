/** @format */

import React, { PureComponent } from "react";
import { Text, TouchableOpacity, View, Image, I18nManager } from "react-native";
import { withTheme, Tools, Config, Languages  } from "@common";
import {TouchableScale} from '@components'
import styles from "./styles";
import { LinearGradient } from "@expo";
import imageCacheHoc from 'react-native-image-cache-hoc';

const CacheableImage = imageCacheHoc(Image, {
  //fileHostWhitelist: ['trendi-iq.com'],
  validProtocols: ['http', 'https'],
});

class Item extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {loaded:false};
  }

  render() {
    const {
      item,
      label,
      image,
      onPress,
      isKid,
      kidChildren,
      type,
      onKidPress,
      theme: {
        colors: { text },
      },
    } = this.props;
   
    
    //console.log(Languages._language,"langgggg");  
    if(label!==undefined){
      var t = label.split("|");
      //console.log(t);
      titleEn = t[0];
      titleAr = t[1];
      titleKr = t[2];
    }else{
      titleEn = "";
      titleAr = "";
      titleKr = "";
    }

    if(isKid===1){
      return (  <View style={{justifyContent:'space-between',flex:1, shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 1,
      },
      alignItems:'center',
      height:50,
      //borderWidth:0.5,
      margin:0.5,
      paddingTop:2,
      paddingBottom:2,
      shadowOpacity: 0.22,
      shadowRadius: 2.22,}}><TouchableOpacity onPress={()=>onKidPress({kid:item.category, ...item, circle: true, name: label})} style={styles.kidsMainCat}><Image style={{width:20,height:15,marginRight:2}} source={{uri:image}} /><Text style={{fontSize:11}}>{label}</Text></TouchableOpacity></View>);
    }
  //   }else if(isKid===1 && kidChildren===1){
  //     console.log("one one")
  //     return (
        
  //       <View style={styles.container}>
  //       <View style={{justifyContent:'space-between',flex:1, shadowColor: "#000",
  //       shadowOffset: {
  //         width: 0,
  //         height: 1,
  //       },
  //       shadowOpacity: 0.22,
  //       shadowRadius: 2.22,
    
  //       elevation: 3}}><TouchableOpacity onPress={()=>onKidPress({kid:item.category, ...item, circle: true, name: label})} style={styles.kidsMainCat}><Image style={{width:20,height:15,marginRight:2}} source={{uri:image}} /><Text style={{fontSize:11}}>{label}</Text></TouchableOpacity>
  //       </View>
  //         <TouchableScale
  //           scaleTo={0.7}
  //           style={styles.wrap}
  //           onPress={() => onPress({ ...item, circle: true, name: label })}>
  //           <View style={[styles.background, {opacity: 1, backgroundColor: item.colors[0]} ]}/>
  
  //           <View style={styles.iconView}>
  //             <Image
  //                 source={{uri:image}}
  //                 style={[styles.icon2, { tintColor: item.colors[1] }]}/>
  //           </View>
  //           <Text style={[styles.title, { color: text }]}>{label}</Text>
  //         </TouchableScale>
  //       </View>
  //     )
  // }
  else{
   //I18nManager.isRTL?titleAr:titleEn
    return (
      <View style={styles.container}>
        <TouchableScale
          scaleTo={0.7}
          style={styles.wrap}
          onPress={() => onPress({ ...item, circle: true, name: label })}>
          {/* <View style={[styles.background, {opacity: 1, backgroundColor: item.colors[0]} ]}/> */}

          <View style={[styles.iconView, { backgroundColor: item.colors[0] }]}>
            <CacheableImage 
                source={{uri:image, cache: 'default'}}
                style={[styles.icon2]}/>
          </View>
          <Text style={[styles.title, { color: text }]}>{I18nManager.isRTL?(Languages._language==='ar'?titleAr:titleKr):titleEn}</Text>
        </TouchableScale>
      </View>
    )
    }
  }
}

export default withTheme(Item);
