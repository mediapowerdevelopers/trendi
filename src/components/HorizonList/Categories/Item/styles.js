import {StyleSheet,Dimensions} from 'react-native'

export default StyleSheet.create({
  container:{
    paddingLeft: 5,
    paddingRight: 6,
    width: 75,
    marginTop: 8
  },
  kidsMainCat:{
    //flex:1,
    //marginTop:5,
    flexDirection:'row',
    backgroundColor:'#fff',
    width:Dimensions.get("window").width/2,
    padding:10,
    justifyContent:'center',
    alignItems:'center',
    elevation:6
   // borderBottomWidth:0.5,
   // borderColor:'#000',
   

  },
  button:{
    width: 60,
    height: 60,
    borderRadius: 30,
    alignItems:'center',
    justifyContent:'center'
  },
  wrap:{
    alignItems:'center',
    justifyContent:'center',
    //flex:1,
    //backgroundColor:'red'
  },
  title:{
    marginTop: 6,
    fontSize: 11,
    textAlign:'center',
    // fontFamily: Constants.fontHeader,
    opacity: 0.9
  },

  iconView: {
    width: 60,
    height: 60,
    borderRadius: 30,
    alignItems:'center',
    justifyContent:'center',
   // position: 'absolute',
    top: 0, 
    left: 0, 
    right: 0, 
    bottom: 0, 
    //justifyContent: 'center', 
    alignItems: 'center',
    backgroundColor:'red'
  },
  icon:{
    width: 28,
    height: 28,
    resizeMode:'contain',
    tintColor: '#FFF', 
    marginBottom: 18
  },
  icon2:{
    width: 60,
    height: 60,
   // resizeMode:'contain',
    //tintColor: '#FFF', 
    marginTop: 0
  },

  background: {
    backgroundColor: '#f1f1f1',

    width: 60,
    height: 60,
    borderRadius: 30,
    alignItems:'center',
    justifyContent:'center'
  }
})
