/** @format */

import React from "react";
import { Text, TouchableOpacity, View, Image } from "react-native";
import styles from "./styles";
import {  Languages } from "@common";

export default CustomIndex = (props) => {
    const {
        item,
        label,
        onPress,
        type,
        index,
        selectedIndex,
    } = props;

   
   if(label==="Women | النساء | ژنان"){
     title=Languages.Women
   }else if(label==="Men | الرجال | پیاوان"){
    title=Languages.Men
   }else{
     title=Languages.Kids
   }

    return (
        <View style={styles.container}>
          <TouchableOpacity style={item.category == selectedIndex ? {
            backgroundColor: '#000',
            shadowOffset: {
              width: 0,
              height: 2.0,
            },
            shadowOpacity: 0.10,
            shadowRadius: 3.56,
            elevation:6,
            width: 90,
            height: 35,
            borderRadius:8,
            justifyContent: 'center',
            alignItems: 'center',
            borderColor:"#000",
           // borderWidth:0.5,
            marginBottom:5
          }: {
            backgroundColor: '#fff',
            shadowOffset: {
              width: 2.0,
              height: 2.0,
            },
            shadowOpacity: 0.15,
            shadowRadius: 3.56,
            elevation:6,
            width: 90,
            height: 35,
            borderRadius:8,
            justifyContent: 'center',
            alignItems: 'center',
            borderColor:"#000",
          //  borderWidth:0.5,
            marginBottom:5
          }
        }
        onPress={()=>onPress({ ...item, circle: true, name: label })}
        >
            <Text style={item.category == selectedIndex ? {color: '#fff'}: {color: '#000'}}>{title}</Text>
          </TouchableOpacity>
        </View>
    )
}