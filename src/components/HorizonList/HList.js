/** @format */

import React, { Component } from "react";
import PropTypes from "prop-types";
import { FlatList, View, ActivityIndicator, LayoutAnimation } from "react-native";
import { Constants, Images, Config, Languages, withTheme, AppConfig } from "@common";
import { HorizonLayout, AdMob, BannerSlider, BannerImage, PostList, BlogList } from "@components";
import { find } from "lodash";
import styles from "./styles";
import Categories from "./Categories";
import MainCategories from "./MainCategories";
import HHeader from "./HHeader";
import { warn } from "@app/Omni";

var CATEGORIES_CONST = 28;
var BANNER_CONST = 28;
var NEW_CAT = 28;
class HorizonList extends Component {
  static propTypes = {
    config: PropTypes.object,
    index: PropTypes.number,
    fetchPost: PropTypes.func,
    fetchNews: PropTypes.func,
    onShowAll: PropTypes.func,
    list: PropTypes.array,
    fetchProductsByCollections: PropTypes.func,
    setSelectedCategory: PropTypes.func,
    onViewProductScreen: PropTypes.func,
    showCategoriesScreen: PropTypes.func,
    collection: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.state = {
      customSelectedCategory: 28,
      catLoading:false,
      fetchedCategories: null,
    }
    console.log('constructor')
    this.myCat = null;;
    this.isKid= 0;
    this.kidChildren = 0;
    this.page = 1;
    this.limit = Constants.pagingLimit;
    this.defaultList = [
      {
        id: 1,
        name: Languages.loading,
        images: [Images.PlaceHolder],
      },
      {
        id: 2,
        name: Languages.loading,
        images: [Images.PlaceHolder],
      },
      {
        id: 3,
        name: Languages.loading,
        images: [Images.PlaceHolder],
      },
    ];
  }

  async componentDidMount() {
    const myUrl = AppConfig.WooCommerce.url;
    const womenDataRequest = `${myUrl}/wp-json/wc/v3/products/categories/?parent=28&consumer_key=ck_b6012a1db2b289e44f0e08aeb16625940435d5b9&consumer_secret=cs_7ff7ae186e82919aa642ac7491acd95ecb5a2a2e&per_page=100&page=1&lang=en&oauth_signature=undefined`;
    const menDataRequest = `${myUrl}/wp-json/wc/v3/products/categories/?parent=29&consumer_key=ck_b6012a1db2b289e44f0e08aeb16625940435d5b9&consumer_secret=cs_7ff7ae186e82919aa642ac7491acd95ecb5a2a2e&per_page=100&page=1&lang=en&oauth_signature=undefined`;
    const kidsDataRequest = `${myUrl}/wp-json/wc/v3/products/categories/?parent=30&consumer_key=ck_b6012a1db2b289e44f0e08aeb16625940435d5b9&consumer_secret=cs_7ff7ae186e82919aa642ac7491acd95ecb5a2a2e&per_page=100&page=1&lang=en&oauth_signature=undefined`;
    fetch(womenDataRequest, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
    .then((response) => response.json())
    .then((womenData) => {
      if (womenData){
        fetch(menDataRequest, {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
        })
        .then((response) => response.json())
        .then((menData) => {
          if( menData ) {
            fetch(kidsDataRequest, {
              method: 'GET',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
            })
            .then((response) => response.json())
            .then((kidsData) => {
              if( kidsData ) {
                const data = {
                  "women": womenData,
                  "men": menData,
                  "kids": kidsData
                }
                this.setState({ fetchedCategories: data })
              }
            })
          }
        }) 
      }
    });
  }

  /**
   * handle load more
   */
  _nextPosts = () => {
    const { config, index, fetchPost, fetchNews, collection } = this.props;
    this.page += 1;
    if (!collection.finish) {
      fetchPost({ config, index, page: this.page });
    }
  };

  _viewAll = () => {
    const {
      config,
      onShowAll,
      index,
      list,
      fetchProductsByCollections,
      setSelectedCategory,
      fetchNews
    } = this.props;
    let selectedCategory = find(
      list,
      (category) => category.id === config.category
    );
    let categoryId = null, slug = "", name = "";
    if( config.name == "flashSale" ) {
      slug = "flash-sale"
      if( BANNER_CONST == 28 ) {
        categoryId = 185;
        name = "Flash Sale - Women";
      } else if( BANNER_CONST == 29 ) {
        categoryId = 5677;
        name = "Flash Sale - Men";
      } else {
        categoryId = 5678;
        name = "Flash Sale - Kids";
      }
    } else {
      slug = "most-popular"
      if( BANNER_CONST == 28 ) {
        categoryId = 186;
        name = "Most Popular - Women";
      } else if( BANNER_CONST == 29 ) {
        categoryId = 5680;
        name = "Most Popular - Men";
      } else {
        categoryId = 5681;
        name = "Most Popular - Kids";
      }
    }
    selectedCategory = { ...selectedCategory, id: categoryId, slug: slug, name: name }
    setSelectedCategory(selectedCategory);
    fetchProductsByCollections(config.category, config.tag, this.page, index);
    onShowAll(config, index);
  };

  _viewAll2 = () => {
    const {
      config,
      onShowAll,
      index,
      list,
      fetchProductsByCollections,
      setSelectedCategory,
      fetchNews
    } = this.props;
    let categoryId = null;
    if(BANNER_CONST===28){
      if( config.name == "Top" ) categoryId = 433;
      else if( config.name == "Middle" ) categoryId = 434;
      else if( config.name == "Bottom" ) categoryId = 435;
    }else if(BANNER_CONST === 29){
      if( config.name == "Top" ) categoryId = 1791;
      else if( config.name == "Middle" ) categoryId = 1792;
      else if( config.name == "Bottom" ) categoryId = 1793;
    }else{
      if( config.name == "Top" ) categoryId = 1794;
      else if( config.name == "Middle" ) categoryId = 1795;
      else if( config.name == "Bottom" ) categoryId = 1796;
    }
    setSelectedCategory({ id: categoryId });
    fetchProductsByCollections({ id: categoryId }, config.tag, this.page, index);
    onShowAll(config, index);
  };

  showProductsByCategory = (config) => {
    const {
      onShowAll,
      index,
      list,
      fetchProductsByCollections,
      setSelectedCategory,
    } = this.props;
    const selectedCategory = find(
      list,
      (category) => category.id === config.category
    );
    setSelectedCategory(selectedCategory);
    fetchProductsByCollections(config.category, config.tag, this.page, index);
    onShowAll(config, index);
  };

  showProductsBySelection = (config) => {
    if( config.category == 29 ) {
      this.props.showMostPopularCategory(5680)
      this.props.showFlashSaleCategory(5677)
    } else if( config.category == 30 ) {
      this.props.showMostPopularCategory(5681)
      this.props.showFlashSaleCategory(5678)
    } else {
      this.props.showMostPopularCategory(186)
      this.props.showFlashSaleCategory(185)
    }
    if( config.category != this.state.customSelectedCategory ) {
      const {
        onShowAll,
        index,
        list,
        fetchProductsByCollections,
        setSelectedCategory,
      } = this.props;
      const selectedCategory = find(
        list,
        (category) => category.id === config.category
      );
      setSelectedCategory(selectedCategory);
      fetchProductsByCollections(config.category, config.tag, this.page, index);
      CATEGORIES_CONST = config.category;
      BANNER_CONST = config.category;
      this.setState({customSelectedCategory: config.category })
    }
    NEW_CAT = config.category
  }

  getRandomColor = () => {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  checkCategories = () => {
    const myUrl = AppConfig.WooCommerce.url;
    //console.log(`${myUrl}/wp-json/wc/v3/products/categories/?parent=${CATEGORIES_CONST}&consumer_key=ck_b6012a1db2b289e44f0e08aeb16625940435d5b9&consumer_secret=cs_7ff7ae186e82919aa642ac7491acd95ecb5a2a2e&per_page=100&page=1&lang=en&oauth_signature=undefined`);
     // this.setState({catLoading:true});
      fetch(`${myUrl}/wp-json/wc/v3/products/categories/?parent=${CATEGORIES_CONST}&consumer_key=ck_b6012a1db2b289e44f0e08aeb16625940435d5b9&consumer_secret=cs_7ff7ae186e82919aa642ac7491acd95ecb5a2a2e&per_page=100&page=1&lang=en&oauth_signature=undefined`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      // body: JSON.stringify({
      //   //password: password,
      // })
    })
      .then((response) => response.json())
      .then((responseJson) => {
      if (responseJson){
        //console.log(responseJson);
        var catArr = [];
        responseJson.map((item,value)=>{
            catArr.push({"category":item.id,"colors":["#fff","#fff"]});
            //console.log(item.id);
        });
        //this.setState({});
        CATEGORIES_CONST = catArr;
        this.setState({catLoading:true});
       // this.setState({});
       
      

      }
     
      });
      return CATEGORIES_CONST;  
      //console.log("cons",catArr);
      //return data;
      //CATEGORIES_CONST =  catArr;

      
     
     // CATEGORIES_CONST = Config.KidsCategories;
      //this.isKid = 0;
      //this.kidChildren = 0;
   // }
    // } else if( CATEGORIES_CONST === 31 ) {
    //   CATEGORIES_CONST = Config.HomeCategories;
    //   this.isKid = 0;
    //   this.kidChildren = 0;
    // }
    // else if( CATEGORIES_CONST === 32 ) {
    //   CATEGORIES_CONST = Config.KidsGirls;
    //   this.isKid = 0;
    //   this.kidChildren = 0;
    // }else if( CATEGORIES_CONST === 33 ) {
    //   CATEGORIES_CONST = Config.KidsBoys;
    //   this.isKid = 0;
    //   this.kidChildren = 0;
    // }

    
   // return CATEGORIES_CONST;
    
    // == 0 ? Config.HomeCategories : Config.MensCategories;
  }

  async getSubCategory(id) {
    const url = AppConfig.WooCommerce.url;
    var catArr = [];
      fetch(`${url}/wp-json/wc/v3/products/categories/?parent=${id}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      // body: JSON.stringify({
      //   //password: password,
      // })
    })
      .then((response) => response.json())
      .then((responseJson) => {
      if (responseJson){
        //console.log(responseJson);
       
        responseJson.map((item,value)=>{
            catArr.push({"category":item.id,"colors":["#000","#fff"]});
            //console.log(item.id);
        });
        //console.log("cat arrrr",catArr);
        
        //this.setState({isLoading:false});
        //alert("Response");
        return catArr;

      }
      console.log('end')
      });
      console.log('after')
 
  }

  

  onViewProductScreen = (product) => {
    this.props.onViewProductScreen({ product });
  };

  showKidsCategory = (item) => {
    //console.log("item",item);
    // this.isKid = 0;
    this.showProductsBySelection(item);
    //this.checkCategories(id);
    
  }
  
  renderItem = ({ item, index }) => {
    const { layout } = this.props.config;
    // console.log('item', item)
    if (item === null) return <View key="post_" />;

    return (
      // <View></View>
      <HorizonLayout
        product={item}
        key={`post-${index}`}
        onViewPost={this.onViewProductScreen}
        layout={layout}
        config={this.props.config}
      />
    );
  };

  renderHeader = () => {
    const { showCategoriesScreen, config, theme } = this.props;
    return (
      <HHeader
        showCategoriesScreen={showCategoriesScreen}
        config={config}
        theme={theme}
        viewAll={this._viewAll}
      />
    );
  };

  showCategories() {
    var catArr = [];
    if( NEW_CAT == 28 ) {
      this.state.fetchedCategories.women.map((item,value)=>{
        catArr.push({"category":item.id,"colors":["#fff","#fff"]});
      });
      return catArr;
    } else if( NEW_CAT == 29 ) {
      this.state.fetchedCategories.men.map((item,value)=>{
        catArr.push({"category":item.id,"colors":["#fff","#fff"]});
      });
      return catArr;
    } else {
      this.state.fetchedCategories.kids.map((item,value)=>{
        catArr.push({"category":item.id,"colors":["#fff","#fff"]});
      });
      return catArr;
    }
  }

  render() {
    const {
      onViewProductScreen,
      collection,
      config,
      isLast,
      theme: {
        colors: { text },
      },
      news
    } = this.props;

    const { VerticalLayout } = AppConfig

    const list =
      typeof collection != "undefined" && typeof collection.list != 'undefined' ? collection.list : this.defaultList;

    const isPaging = !!config.paging;

    // console.log('mosat', Constants.Layout)

    switch (config.layout) {
      case Constants.Layout.mainCategory:
        return (
          <MainCategories
            config={config}
            categories={this.props.list}
            items={Config.MainCategories}
            type={config.theme}
            selectedCategory={this.state.customSelectedCategory}
            onPress={this.showProductsBySelection}
          />
        );
      case Constants.Layout.circleCategory:
        return (
          <Categories
            config={config}
            categories={this.props.list}
            items={this.state.fetchedCategories ? this.showCategories() :[]}
            type={config.theme}
            isKid={this.isKid}
            kidChildren ={this.kidChildren}
            onPress={this.showProductsByCategory}
            onKidPress={this.showKidsCategory}
          />
        );
      case Constants.Layout.BannerSlider:
        return (
          <BannerSlider data={list} onViewPost={this.onViewProductScreen} />
        );
      case Constants.Layout.BannerImage:
         // alert("ban ");
        return (
          <BannerImage
            viewAll={this._viewAll2}
            config={config}
            category = {BANNER_CONST}
            data={list}
            onViewPost={this.onViewProductScreen}
          />
        );
      case Constants.Layout.Blog:
        return (
          <BlogList news={news}
            theme={this.props.theme}
            fetchNews={this.props.fetchNews}
            navigation={this.props.navigation}
            config={config} />
        );
    }

    if( config.name == "mostPopular" && this.props.initialCategory != 28 ) {
      return (
        <View>
          {config.name && this.renderHeader()}
          <FlatList
            overScrollMode="never"
            contentContainerStyle={styles.flatlist}
            data={this.props.mostPopularList}
            keyExtractor={(item, index) => `post__${index}`}
            renderItem={this.renderItem}
            showsHorizontalScrollIndicator={false}
            horizontal
            pagingEnabled={isPaging}
            onEndReached={false && this._nextPosts}
          />
        </View>
      )
    }
    if( config.name == "flashSale" && this.props.initialCategory != 28 ) {
      return (
        <View>
          {config.name && this.renderHeader()}
          <FlatList
            overScrollMode="never"
            contentContainerStyle={styles.flatlist}
            data={this.props.flashSaleList}
            keyExtractor={(item, index) => `post__${index}`}
            renderItem={this.renderItem}
            showsHorizontalScrollIndicator={false}
            horizontal
            pagingEnabled={isPaging}
            onEndReached={false && this._nextPosts}
          />
        </View>
      )
    }

    return (
      <View
        style={[
          styles.flatWrap,
          config.color && {
            backgroundColor: config.color,
          },
        ]}>
        {config.name && this.renderHeader()}
        <FlatList
          overScrollMode="never"
          contentContainerStyle={styles.flatlist}
          data={list}
          keyExtractor={(item, index) => `post__${index}`}
          renderItem={this.renderItem}
          showsHorizontalScrollIndicator={false}
          horizontal
          pagingEnabled={isPaging}
          onEndReached={false && this._nextPosts}
        />

        {isLast && (
          <FlatList
            overScrollMode="never"
            contentContainerStyle={styles.flatlist}
            data={[1, 2, 3, 4, 5]}
            keyExtractor={(item, index) => `post__${index}`}
            renderItem={() => <AdMob adSize="largeBanner" />}
            showsHorizontalScrollIndicator={false}
            horizontal
            pagingEnabled={isPaging}
          />
        )}

        {typeof VerticalLayout != 'undefined' &&
          <PostList
            parentLayout={VerticalLayout.layout}
            headerLabel={VerticalLayout.name}
            onViewProductScreen={onViewProductScreen} />
        }
        {this.state.catLoading&&<ActivityIndicator size="small" color="#00ff00" />}
      </View>
    );
  }
}

export default withTheme(HorizonList);