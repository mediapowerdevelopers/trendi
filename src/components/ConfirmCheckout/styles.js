/** @format */

import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    margin: 15,
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingVertical: 10,
  },
  label: {
    color: "#919191",
    fontSize: 14,
    padding:4
  },
  value: {
    fontSize: 13,
  },
  divider: {
    height: 0.5,
    backgroundColor: "#919191",
  },
});
