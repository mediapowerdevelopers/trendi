/** @format */

import { StyleSheet, Platform, Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");
import { Color, Config, Constants, Device, Styles } from "@common";

export default StyleSheet.create({
  container: {
    marginBottom: 2,
  },
  fullName: {
    fontWeight: "600",
    color: Color.blackTextPrimary,
    backgroundColor: "transparent",
    fontSize: 30,
    marginBottom: 6,
  },
  address: {
    backgroundColor: "transparent",
    fontSize: 15,
    color: "#9B9B9B",
    fontWeight: "600",
  },
  textContainer: {
    marginLeft: 20,
    justifyContent: "center",
  },
  header: {
    flexDirection: "row",
    //backgroundColor: "#FFF",
    padding: 20,
  },
  avatar: {
    height: width / 4,
    width: width / 4,
    borderRadius: 4,
  },
  loginText: {
    color: "#666",
  },
  inputContainer: {
    width:'50%',
    height:40,
    textAlign:'center',
    borderRadius: 8,
    paddingLeft: 10,
    paddingRight: 10,
    borderColor: "#000",
    borderWidth: 0.3,
  },

  submitContainer:{
    width:'50%',
    height:40,
    textAlign:'center',
    borderRadius: 8,
    paddingLeft: 10,
    paddingRight: 10,
    borderColor: "#000",
    borderWidth: 0.3,
    backgroundColor:'#000',
    justifyContent:'center',
    alignItems:'center'
  },


loginInputTextStyle: {
    textAlign:'center',
    height:40,
    //fontFamily: 'Opensans-Regular',
    fontSize: 13
  },
});
