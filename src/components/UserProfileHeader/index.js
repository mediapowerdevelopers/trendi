/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { View, Text, TouchableOpacity, Image, Modal, TextInput, ActivityIndicator } from "react-native";
import { Languages, Tools, withTheme } from "@common";
//import {ModalBox} from "@components";
import styles from "./styles";

class UserProfileHeader extends PureComponent {
  //   constructor(props) {
  //     super(props)
  //   }

  state={
    modalVisible:false,
    isLoading:false,
    password:"",
    confirmPass:""

  }

  static propTypes = {
    onLogin: PropTypes.func.isRequired,
    onLogout: PropTypes.func.isRequired,
    user: PropTypes.object,
  };

  loginHandle = () => {
    if (this.props.user.name === Languages.Guest) {
      this.props.onLogin();
    } else {
      this.props.onLogout();
    }
  };

  _updatePassword = () => {
    const id = (this.props.user.id);
    const {password, confirmPass} = this.state;
    if (password.length===0) {
      alert('Please enter your password ');
    } else if (password.length<=5) {
      alert('Password in length should be greater than 5 ');
    } else if (password !== confirmPass) {
      alert('Password mismatch');
    } else {
      const payload = {
        new_password: password,
        new_password_confirmation: confirmPass,
      };
      this.setState({isLoading:true});
      fetch(`https://trendi-iq.com/wp-json/wc/v3/customers/${id}`, {
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          password: password,
        })
      })
        .then((response) => response.json())
        .then((responseJson) => {
        if (responseJson){
          console.log(responseJson);
          this.setState({isLoading:false});
          alert("Password Changed Successfully");

        }
        });
   
      
    }
  }

  reset = () => {
    this.setState({modalVisible:true});
  };

  render() {
    const { user } = this.props;
    const avatar = Tools.getAvatar(user);
    const {
      theme:{
        colors:{
          background, text
        }
      }
    } = this.props

    return (
      <View style={[styles.container, {backgroundColor: background}]}>
        <View style={styles.header}>
          {/* <Image source={avatar} style={styles.avatar} /> */}
          <View style={styles.textContainer}>
            <Text style={[styles.fullName, {color: text}]}>{user.name}</Text>
            <Text style={[styles.address, {color: text}]}>{user ? user.address : ""}</Text>


            <TouchableOpacity style={{marginBottom:10}} onPress={this.reset}>
              <Text style={styles.loginText}>
                {user.name === Languages.Guest
                  ? ""
                  : Languages.changePassword}
              </Text>
            </TouchableOpacity>  

            <TouchableOpacity onPress={this.loginHandle}>
              <Text style={styles.loginText}>
                {user.name === Languages.Guest
                  ? Languages.Login
                  : Languages.Logout}
              </Text>
            </TouchableOpacity>

                  

          </View>
        </View>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={{marginTop: 60,flex:1}}>
                <TouchableOpacity style={{backgroundColor:'#000',width:70,margin:5,borderRadius:8,justifyContent:'center',alignItems:'center'}}
                onPress={() => {
                  this.setState({modalVisible:false});
                }}>
                <Text style={{padding:8,color:"#fff",fontSize:16}}>Close</Text>
              </TouchableOpacity>
            <View style={{backgroundColor:'#aeaeae20',flex:1,justifyContent:'center',alignItems:'center'}}>
                
            <View style={styles.inputContainer}>
                  <TextInput placeholder="New Password" placeholderTextColor="#1A1A1A" style={styles.loginInputTextStyle} keyboardType="default" 
                  secureTextEntry={true}
                  returnKeyType="next"
                  onSubmitEditing={() => { this.secondTextInput.focus(); }}
                  onChangeText={password => this.setState({password}) }/>
                </View>
                <View style={{height: '4%'}}/>
                <View style={styles.inputContainer}>
                  <TextInput placeholder="Confirm Password" placeholderTextColor="#1A1A1A" style={styles.loginInputTextStyle} keyboardType="default" 
                  secureTextEntry={true}
                  ref={(input) => { this.secondTextInput = input; }}
                  onChangeText={confirmPass => this.setState({confirmPass})} />
                </View>
                <View style={{height: '8%'}}/>
                <TouchableOpacity style={styles.submitContainer} onPress={()=>this._updatePassword()}>{this.state.isLoading?<ActivityIndicator size="small" color="#fff" />:<Text style={{fontSize:16,color:'#fff',textAlign:'center'}}>Change Password</Text>}</TouchableOpacity>
              
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

export default withTheme(UserProfileHeader)
