/** @format */

export default {
  mainContent: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-around",
  },
  image: {
    width: 320,
    height: 320,
  },
  text: {
    color: "rgba(255, 255, 255, 0.8)",
    backgroundColor: "transparent",
    textAlign: "center",
    paddingHorizontal: 16,
  },
  title: {
    fontSize: 22,
    color: "white",
    backgroundColor: "transparent",
    textAlign: "center",
    marginBottom: 16,
  },
  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: "rgba(0, 0, 0, .2)",
    borderRadius: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  trendiLogoContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  trendiLogo: {
    width: '50%',
    height: '50%',
    resizeMode: 'contain'
  },
  modalContainer: {
    flex: 1,
    backgroundColor: 'transparent',
    justifyContent: 'flex-end'
  },
  modalContent: {
    flex: 0.65,
    backgroundColor: 'white', 
    alignItems: "center"
  },
  topArrowButton: {
    position: 'absolute', 
    top: -20,
    borderTopLeftRadius: 80,
    borderTopRightRadius: 80,
    width: 110,
    height: 50,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center'
  },
  topArrowImage: {
    width: 20,
    height: 20, 
    resizeMode: 'contain'
  },
  singleViewContainer: {
    flex: 1, 
    justifyContent: 'center',
    alignItems: 'center',
    width: '85%'
  },
  headingText: {
    fontSize: 21,
    fontWeight: 'bold',
    letterSpacing: 2,
    color: '#111'
  },
  headingDetailsText: {
    fontSize: 12,
    color: '#aeaeae',
    textAlign: 'center',
    marginTop: 5,
    lineHeight: 18
  },
  imageIcons: {
    width: 30,
    height: 30,
    resizeMode: 'contain'
  },
  detailsText: {
    fontSize: 12,
    color: '#000',
    textAlign: 'center',
    marginTop: 5
  },
  bottomButtonContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '70%'
  },
  bottomButton: {
    backgroundColor: '#000',
    height: 45,
    width: '100%',
    borderRadius: 7,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonText: {
    fontSize: 12,
    color: '#fff',
    fontWeight: 'bold'
  }
};
