/** @format */

import React, { PureComponent } from "react";
import Ionicons  from "react-native-vector-icons/Ionicons";
import { View, Text, Image, I18nManager, Animated, Dimensions, TouchableOpacity } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
import { LinearGradient } from "@expo";
import AppIntroSlider from "react-native-app-intro-slider";
import styles from "./styles";
import { Config, Languages } from "@common";
import { connect } from "react-redux";
import resolveAssetSource from 'resolveAssetSource';
import Modal from 'react-native-modalbox';
import RNRestart from "react-native-restart";
import PropTypes from 'prop-types'


const { width, height } = Dimensions.get('window');
const splash_logo = require("./../../images/intro/logo_splash.png");
var CHECK_INTRO;
var CHECK_LANG;
class AppIntro extends PureComponent {
  static propTypes = {
    switchLanguage: PropTypes.func,
  }
  constructor(props) {
    super(props);
    this.state = {
      isAnimationComplete: false,
      introImages: [
        require('./../../images/intro/01.png') ,
        require('./../../images/intro/02.png') ,
        require('./../../images/intro/03.png') ,
        require('./../../images/intro/04.png') ,
        require('./../../images/intro/05.png') ,
      ],
      showModal: false,
      showLanguage: false,
      slideCounter: 0,
      heightState: new Animated.Value(height),
      widthState: new Animated.Value(width),
      selectedOption: this.props.language.lang,
    }
    // this.heightState= new Animated.Value(height),
    // this.widthState= new Animated.Value(width),
    this.opacity = new Animated.Value(1);
    this.counter = 1;
    this.newWidth = 1;
    this.isMaximum = true;
  }

  async componentWillMount() {
    this.playAnimation();
    var myLang = await AsyncStorage.getItem('@current_language');
    this.setState({selectedOption:myLang?myLang:this.props.language.lang});
  }

  _handlePressLang = async(lang) => {
   
    const { switchLanguage, switchRtl, language } = this.props;
    const  selectedOption  = lang;
   
  // if (selectedOption !== language.lang) {
      const isRtl = (selectedOption === "ar" || selectedOption === "kr");
      if(selectedOption==="ar"){
        this.setState({ isLoadingar: true });
      }else if(selectedOption==="en"){
        this.setState({ isLoadingen: true });
      }else{
        this.setState({ isLoadingkr: true });
      }
      this.setState({ loading: true });
      //alert(selectedOption);
      //return;
      try {
        await AsyncStorage.setItem('@current_language', selectedOption);
      } catch (e) {
        // saving error
      }
      //alert("here1");
      switchLanguage({
        lang: selectedOption,
        rtl: isRtl,
      });

      
      //alert("here");  
      I18nManager.forceRTL(isRtl);
      this.langDone();
      setTimeout(() => {
        this.setState({ isLoading: false });
        RNRestart.Restart();
      }, 400);
      
   // }
  };

  playAnimation() {
    Animated.timing(
      this.state.widthState, // The animated value to drive
      {
        toValue: this.isMaximum ? width * 1.2 : width, // Animate to opacity: 1 (opaque)
        duration: 1200, // Make it take a while
      }
    ).start(()=> {
      this.counter++;
      if( this.counter == 5 ) {
        // this.counter = 0;
        this.checkIntro()
        // if(CHECK_INTRO=='true'){
        //   this.setState({ showModal: true  })
        // }else{
        //   this.setState({  showLanguage:true })
        // }
        return;
      }
      if( this.imageRef ) {
        this.imageRef.setNativeProps({
          source: [resolveAssetSource(this.state.introImages[this.counter])]
        })
      }
      // this.state.heightState.setValue(height);
      // this.state.widthState.setValue(width);
      this.isMaximum = this.isMaximum == true ? false : true; 
      this.playAnimation();
    });
  }

  goToNext(counter) {
    let count = counter + 1;
    this.setState({ slideCounter: parseInt(count) })
    this.sliderRef.goToSlide(count);
  }

  _renderItem = (props) => {
    const {item, index, dimensions} = props;
    // console.log('props', props)
    return <View style={{flex: 1}}>
      <Image source={item.image} style={{width: '100%', height: '100%', resizeMode: 'cover'}}/>
    </View>
  };

  slideChange(index) {
    if( (this.state.slideCounter - 1) == index ) {
      let dec = this.state.slideCounter - 1;
      this.setState({ slideCounter: dec })
    } else {
      let inc = this.state.slideCounter + 1;
      this.setState({ slideCounter: inc })
    }
  }

  _showFinish() {
    return (
      <View style={{alignSelf: 'center', width: '90%', position: 'absolute', top: '10%', justifyContent: 'space-between', flexDirection: 'row'}}>
        <TouchableOpacity>
        </TouchableOpacity>
        <TouchableOpacity style={{justifyContent: 'center', alignItems: 'center', flexDirection: 'row'}}
        onPress={()=> this.skipAnimation()}>
          <Text style={{fontSize: 13, color: '#000', fontWeight: 'bold', textAlign: 'center'}}> {Languages.FINISH} </Text>
        </TouchableOpacity>
      </View>
    )
  }

  _renderButtons() {
    return(
      <View style={{alignSelf: 'center', width: '90%', position: 'absolute', top: '10%', justifyContent: 'space-between', flexDirection: 'row'}}>
        <TouchableOpacity onPress={()=>this.skipAnimation()}>
          <Text style={{fontSize: 12}}> {Languages.SKIP} </Text>
        </TouchableOpacity>
        <TouchableOpacity style={{justifyContent: 'center', alignItems: 'center', flexDirection: 'row'}}
        onPress={()=> this.goToNext(this.state.slideCounter)}>
          <Text style={{fontSize: 13, color: '#000', fontWeight: 'bold', textAlign: 'center'}}> {Languages.NEXT} </Text>
        </TouchableOpacity>
      </View>
    )
  }

  async skipAnimation() {
    try {
      await AsyncStorage.setItem('@first_used', 'true')
    } catch (e) {
      // saving error
    }
    this.props.finishIntro();
  }

  async langDone() {
    try {
      await AsyncStorage.setItem('@lang_done', 'true')
    } catch (e) {
      // saving error
    }
    //this.props.finishIntro();
  }

  async checkIntro(){
    
    CHECK_INTRO =    await AsyncStorage.getItem('@first_used');
    CHECK_LANG =    await AsyncStorage.getItem('@lang_done');

    if(CHECK_INTRO=='true'){
      this.props.finishIntro();
    }
    //alert(CHECK_LANG);
    if(CHECK_LANG===null){
      this.setState({  showLanguage:true })
      return;
    }
    //if(CHECK_INTRO=='true'){
     // this.props.finishIntro();
      this.setState({ showModal: true  })
      //return
    //}
    
  }

  async closeModal(){
    this.setState({isAnimationComplete: true});
  }

  render() {
    let { heightState, widthState } = this.state;
    if( this.state.isAnimationComplete === false ) {
      if(this.state.showLanguage){
        return (
          <View style={{flex: 1}}>
            <View style={{flex:1.5,justifyContent:'flex-end',padding:10}}>
              {/* <Text style={{fontSize:20}}>Language - زمان - لغة</Text> */}
            </View>
            <View style={{flex:3,backgroundColor:'#E8E8E8',justifyContent:'center',alignItems:'center'}}>
               
               <Text style={{writingDirection:'rtl',textAlign: "left",fontSize:15,marginRight:15}}> For a better shopping experience please select {'\n'} your language below</Text>
               <Text style={{writingDirection:'rtl',fontSize:15,textAlign:'right',marginTop:10}}> للحصول على تجربة تسوق أفضل ، يرجى اختيار لغتك أدناه</Text>
               <Text style={{writingDirection:'rtl',fontSize:15,textAlign:'right',marginTop:10}}> بۆيێك باشتر ئەزموون بازاڕ دەكات تكايە زمانت هەڵ ببژێرێت</Text>
  
               {/* <Text style={{fontSize:14,textAlign:'left',padding:5}}><Text style={{fontSize:10}}>{'\u2B24'}</Text>  للحصول على تجربة تسوق أفضل ، يرجى اختيار لغتك أدناه</Text>
               <Text style={{fontSize:14,textAlign:'right',padding:5}}><Text style={{fontSize:10}}>{'\u2B24'}</Text>  بۆيێك باشتر ئەزموون بازاڕ دەكات تكايە لەخوارەوە زمانت هەڵ ببژێرێت</Text> */}
            </View>
            <View style={{flex:6,marginTop:20}}>
                <View style={{marginLeft:20,marginRight:20,height:70,borderWidth:0.5,borderColor:'#aeaeae',justifyContent:'center',alignItems:'center',padding:10,borderRadius:5}}>
                <TouchableOpacity onPress={(lang)=>this._handlePressLang('ar')} style={styles.flagContainer}>
                  <Image
                  source={require('../../images/iraq_flag_side.png')}
                  style={[styles.avatar]}
                  /> 
                </TouchableOpacity>
                </View>
                <View style={{marginLeft:20,marginRight:20,marginTop:10,height:70,borderWidth:0.5,borderColor:'#aeaeae',justifyContent:'center',alignItems:'center',padding:10,borderRadius:5}}>
                <TouchableOpacity onPress={(lang)=>this._handlePressLang('en')} style={styles.flagContainer}>
                  <Image
                  source={require('../../images/us_flag_side.png')}
                  style={[styles.avatar]}
                  /> 
                </TouchableOpacity>
                </View>
                <View style={{marginLeft:20,marginRight:20,marginTop:10,height:70,borderWidth:0.5,borderColor:'#aeaeae',justifyContent:'center',alignItems:'center',padding:10,borderRadius:5}}>
                <TouchableOpacity onPress={(lang)=>this._handlePressLang('kr')} style={styles.flagContainer}>
                  <Image
                  source={require('../../images/uae_flag_side.png')}
                  style={[styles.avatar]}
                  /> 
                </TouchableOpacity>
                </View>
            </View>
          </View>
        );
      }
      return ( 
        <View style={{flex: 1}}>
          <Animated.Image 
          ref={(imageRef)=> this.imageRef = imageRef} 
          source={require(`./../../images/intro/01.png`)} 
          style={{flex: 1, width: widthState, height: heightState }}/>
          <View style={styles.trendiLogoContainer}>
            <Image source={splash_logo} style={styles.trendiLogo}/>
          </View>
          <Modal
            isOpen={this.state.showModal}
            position={"bottom"}
            backdropPressToClose={false}
            backdropColor="transparent"
            swipeToClose={false}
            style={styles.modalContainer}>
            <View style={styles.modalContent}>
              <TouchableOpacity style={styles.topArrowButton}
              activeOpacity={1}>
                <Image source={require('../../images/intro/why_us/down.png')} style={styles.topArrowImage}/>
              </TouchableOpacity>
              <View style={{flex: 0.35}}></View>
              <View style={styles.singleViewContainer}>
                <Text style={styles.headingText}> {Languages.welcome} </Text>
                <Text style={styles.headingDetailsText}> {Languages.discover} </Text>
              </View>
              <View style={{ flex: 0.4, justifyContent: 'flex-end', alignItems: 'baseline'}}>
                <Text style={styles.headingText}> {Languages.whyUs} </Text>
              </View>
              <View style={styles.singleViewContainer}>
                <Image source={require('../../images/intro/why_us/delivery.png')} style={styles.imageIcons}/>
                <Text style={styles.detailsText}> {Languages.freeDelivery} </Text>
              </View>
              <View style={styles.singleViewContainer}>
                <Image source={require('../../images/intro/why_us/brands.png')} style={styles.imageIcons}/>
                <Text style={styles.detailsText}> {Languages.browse} </Text>
              </View>
              <View style={styles.bottomButtonContainer}>
                <TouchableOpacity style={styles.bottomButton} onPress={()=> this.closeModal()}>
                  <Text style={styles.buttonText}> {Languages.continue} </Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
        </View>
      )
    }
  // console.log("check intro",CHECK_INTRO); 
  else if(this.state.isAnimationComplete === true && (CHECK_INTRO==null || CHECK_INTRO=="undefined") ){
    
    return (
      <View style={{flex: 1}}>
        <AppIntroSlider
          slides={I18nManager.isRTL?(Languages._language==='ar'?Config.intro_newAR:Config.intro_newKR):Config.intro_newEN}
          renderItem={this._renderItem}
          // renderDoneButton={this._renderDoneButton}
          // renderNextButton={this._renderNextButton}
          hidePagination={true}
          onDone={this.props.finishIntro}
          ref={slide=> this.sliderRef = slide}
          onSlideChange={(index)=> this.slideChange(index)}
        />
        {this.state.slideCounter == 5 ? this._showFinish() : this._renderButtons()}
      </View>
    );
   } 
    
  }
}

const mapStateToProps = ({  language }) => ({
  language: language.lang,
})

const mapDispatchToProps = (dispatch) => {
  const { actions } = require("@redux/UserRedux");
  //const { Langactions } = require("@redux/LangRedux");
  return {
    finishIntro: () => dispatch(actions.finishIntro()),
   // switchLanguage: (language) => dispatch(Langactions.switchLanguage(language)),
   // switchRtl: (rtl) => dispatch(Langactions.switchRtl(rtl)),
  };
};

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps
  const { actions } = require('@redux/LangRedux')
  const { userActions } = require("@redux/UserRedux");

  return {
    ...ownProps,
    ...stateProps,
    switchLanguage: (language) => actions.switchLanguage(dispatch, language),
    switchRtl: (rtl) => actions.switchRtl(dispatch, rtl),
    //finishIntro: () => userActions.finishIntro(dispatch),
    finishIntro: () => dispatch(userActions.finishIntro()),
  };
}
export default connect(
  mapStateToProps,
  null,
  mergeProps
)(AppIntro);



// _renderItem = (props) => {
//   const {item, index, dimensions} = props;

//   return <LinearGradient
//     style={[
//       styles.mainContent,
//       {
//         paddingTop: props.topSpacer,
//         paddingBottom: props.bottomSpacer,
//         width: props.width,
//         height: props.height,
//       },
//     ]}
//     colors={item.colors}
//     start={{ x: 0, y: 0.1 }}
//     end={{ x: 0.1, y: 1 }}>
//     <Ionicons
//       style={{ backgroundColor: "transparent" }}
//       name={item.icon}
//       size={200}
//       color="white"
//     />
//     <View>
//       <Text style={styles.title}>{item.title}</Text>
//       <Text style={styles.text}>{item.text}</Text>
//     </View>
//   </LinearGradient>
// };

// _renderNextButton = () => {
//   return (
//     <View style={styles.buttonCircle}>
//       <Ionicons
//         name={
//           I18nManager.isRTL ? "md-arrow-round-back" : "md-arrow-round-forward"
//         }
//         color="rgba(255, 255, 255, .9)"
//         size={24}
//         style={{ backgroundColor: "transparent" }}
//       />
//     </View>
//   );
// };

// _renderDoneButton = () => {
//   return (
//     <View style={styles.buttonCircle}>
//       <Ionicons
//         name="md-checkmark"
//         color="rgba(255, 255, 255, .9)"
//         size={24}
//         style={{ backgroundColor: "transparent" }}
//       />
//     </View>
//   );
// };