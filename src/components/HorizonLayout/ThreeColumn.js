/** @format */

import React, { PureComponent } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { Images, withTheme } from "@common";
import { ProductPrice, ImageCache, WishListIcon } from "@components";
import { getProductImage } from "@app/Omni";
import css from "./style";

class ThreeColumn extends PureComponent {
  render() {
    const {
      viewPost,
      title,
      product,
      theme: {
        colors: { text },
      },
    } = this.props;
    const imageURI =
      typeof product.images[0] !== "undefined"
        ? getProductImage(product.images[0].src, 200)
        : Images.PlaceHolderURL;
     //console.log(product);   
    return (
      product ?
      (<TouchableOpacity
        activeOpacity={0.9}
        style={css.panelThree}
        onPress={viewPost}>
        <View style={css.Shadow}>
          <ImageCache uri={imageURI}  style={css.imagePanelThree} />
        </View>
        {/* <Text numberOfLines={2} style={[css.nameThree, { color: text }]}>
          {title}
        </Text> */}
        <View style={{flex:1,flexDirection:'row',marginTop:8}}>
        
        
        <View style={{flex:1,alignItems:'flex-start',justifyContent:'flex-start',flexDirection:'row'}}>
        <ProductPrice product={product} hideDisCount  />
        </View>


        
          <View style={{marginTop:1,height:32,backgroundColor:'red',justifyContent:'center',alignItems:'flex-end',padding:3}}><Text style={{color:"#fff",fontWeight:'700',fontSize:16,fontFamily:'OpenSans-Bold'}}>SALE</Text></View>
          </View>
        
        <WishListIcon product={product} />
      </TouchableOpacity>): (<View />)
    );
  }
}

export default withTheme(ThreeColumn);
