/** @format */

import React, { PureComponent } from "react";
import { View, Dimensions, I18nManager, Image , Text } from "react-native";
import { Languages,Images } from "@common";
import { ImageCache, TouchableScale } from "@components";
const { width } = Dimensions.get("window");
import imageCacheHoc from 'react-native-image-cache-hoc';

const CacheableImage = imageCacheHoc(Image, {
  //fileHostWhitelist: ['trendi-iq.com'],
  validProtocols: ['http', 'https'],
});

export default class BannerImage extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {loaded:false};
  }

  render() {
    const { viewAll, config , category } = this.props;
    const column = config.column || 1;
    const height = config.height || 200;
    const resizeMode =  'cover';


    
    //console.log("banner image category",category);
    //console.log("banner image config",config);

    
    //alert(Languages._language);
    if(category===28){
      imageUri =  I18nManager.isRTL ? (Languages._language==='ar' ?  config.imageBannerWomenAr : config.imageBannerWomenKr) :  config.imageBannerWomenEn;
    }else if(category===29){
      imageUri =  I18nManager.isRTL ? (Languages._language==='ar' ? config.imageBannerManAr : config.imageBannerManKr) : config.imageBannerManEn;
    }else{
      imageUri =  I18nManager.isRTL ? (Languages._language==='ar' ? config.imageBannerkidsAr : config.imageBannerkidsKr) :  config.imageBannerkidsEn;
    } 

    //console.log("category",category);

    //console.log("View All",viewAll);


    return (
      <TouchableScale onPress={viewAll}>
        <View
          activeOpacity={1}
          style={styles.imageBannerView(column, height)}>
          <CacheableImage   source={{uri:imageUri}} style={styles.imageBanner(resizeMode)} />
        </View>
      </TouchableScale>
    );
  }
}

const styles={
  imageBannerView: (column, height) => ({
      width: (width/column) - 0,
      height: height,
      borderRadius: 0,
      marginLeft: 0,
      marginRight: 0,
      marginTop: 10,
      overflow: 'hidden',

      backgroundColor: '#eee'
  }),
  imageBanner: (resizeMode) => ({
    flex: 1,
    resizeMode: resizeMode,
  }),
}