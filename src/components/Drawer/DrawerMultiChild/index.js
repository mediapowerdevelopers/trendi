/**
 * Created by InspireUI on 27/02/2017.
 *
 * @format
 */

import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { View, ScrollView, Image, I18nManager, ActivityIndicator } from 'react-native'
import { connect } from 'react-redux'
import RNRestart from "react-native-restart";
import AsyncStorage from '@react-native-community/async-storage';
import { Accordion, Empty, Text } from '@components'
import { toast } from '@app/Omni'
import { Color, Icons, Config, Languages, Tools, Constants } from '@common'
import { DrawerButton, DrawerButtonChild } from '../DrawerButton'
import styles from './styles'
import { TouchableOpacity } from 'react-native-gesture-handler'


class DrawerMultiChild extends PureComponent {
  static propTypes = {
    backgroundMenu: PropTypes.string,
    colorTextMenu: PropTypes.string,
    userProfile: PropTypes.object,
    fetchCategories: PropTypes.func.isRequired,
    goToScreen: PropTypes.func.isRequired,
    setSelectedCategory: PropTypes.func.isRequired,
    categories: PropTypes.any,
    language: PropTypes.object,
    switchLanguage: PropTypes.func,
  }

  static defaultProps = {
    backgroundMenu: '#FFF',
    colorTextMenu: Color.blackTextPrimary,
  }

  constructor(props) {
    super(props)

    const { user } = props.userProfile
    // Config Menu
    if (user) {
      this.buttonList = [...Config.menu.listMenu, ...Config.menu.listMenuLogged]
    } else {
      this.buttonList = [
        ...Config.menu.listMenu,
        ...Config.menu.listMenuUnlogged,
      ]
    }

    this.state = { selectedOption: this.props.language.lang,
      isLoadingar: false,
      isLoadingen: false,
      isLoadingkr: false
    }
  }

  // componentDidMount() {
  //   //const { fetchCategories } = this.props
  //   //fetchCategories()
  // }

  async componentDidMount(){
    var myLang = await AsyncStorage.getItem('@current_language');
    this.setState({selectedOption:myLang?myLang:this.props.language.lang});
  }

  _handlePressLang = async(lang) => {
    
    const { switchLanguage, switchRtl, language } = this.props;
    const  selectedOption  = lang;
   
  // if (selectedOption !== language.lang) {
      const isRtl = (selectedOption === "ar" || selectedOption === "kr");
      if(selectedOption==="ar"){
        this.setState({ isLoadingar: true });
      }else if(selectedOption==="en"){
        this.setState({ isLoadingen: true });
      }else{
        this.setState({ isLoadingkr: true });
      }
      this.setState({ loading: true });
      //alert(selectedOption);
      //return;
      try {
        await AsyncStorage.setItem('@current_language', selectedOption);
      } catch (e) {
        // saving error
      }
      
      switchLanguage({
        lang: selectedOption,
        rtl: isRtl,
      });

      

      I18nManager.forceRTL(isRtl);

      setTimeout(() => {
        this.setState({ isLoading: false });
        RNRestart.Restart();
      }, 3000);
      
   // }
  };

  /**
   * Update when logged in
   */
  componentWillReceiveProps(props) {
    const { userProfile } = props
    const { error } = props.categories
    if (error) toast(error)

    if (userProfile && userProfile.user) {
      this.buttonList = [...Config.menu.listMenu, ...Config.menu.listMenuLogged]
    }
  }

  /**
   * Render header of accordion menu
   */
  _renderHeader = (section, index, isActive) => {
    const { colorTextMenu } = this.props

    return (
      <DrawerButtonChild
        iconRight={isActive ? Icons.Ionicons.Remove : Icons.Ionicons.Add}
        text={section.name}
        uppercase
        key={index}
        colorText={colorTextMenu}
        {...section}
      />
    )
  }

  /**
   * Render content of accordion menu
   */
  _renderContent = (section) => {
    const { categories, selectedCategory, colorTextMenu } = this.props
    const subCategories = this._getCategories(categories, section)

    return (
      <View>
        <View key={-1} style={{ marginLeft: 20 }}>
          <DrawerButton
            {...section}
            onPress={() => this._handlePress(section, section)}
            text={Languages.seeAll}
            textStyle={styles.textItem}
            colorText={colorTextMenu}
          />
        </View>
        {subCategories.map((cate, index) => {
          return (
            <View key={index} style={{ marginLeft: 20 }}>
              <DrawerButton
                {...section}
                onPress={() => this._handlePress(cate, section)}
                text={cate.name}
                textStyle={styles.textItem}
                colorText={colorTextMenu}
                isActive={
                  selectedCategory ? selectedCategory.id === cate.id : false
                }
              />
            </View>
          )
        })}
      </View>
    )
  }

  _renderRowCategories = () => {
    const { categories, colorTextMenu } = this.props
    const mainCategories = this._getCategories(categories)

    if (
      categories.error ||
      !categories ||
      (categories && categories.list === 0)
    ) {
      return <Empty />
    }

    return (
      <View>
        {mainCategories && mainCategories.length ? (
          <View>
            {/* <View style={styles.headerCategory}>
              <Text
                style={[
                  styles.textHeaderCategory,
                  {
                    color: colorTextMenu,
                  },
                ]}>
                {Languages.Category && Languages.Category.toUpperCase()}
              </Text>
            </View>
            <Accordion
              underlayColor={Color.lightTextDisable}
              sections={mainCategories}
              renderHeader={this._renderHeader}
              renderContent={this._renderContent}
            /> */}
          </View>
        ) : null}
      </View>
    )
  }

  _getCategories = (categories, section) => {
    if (categories && categories.list.length) {
      return categories.list.filter((cate, index) => {
        if (section) {
          return cate.parent === section.id // check is sub category
        }
        return cate.parent === 0
      })
    }

    return []
  }

  _handlePress = (item, section) => {
    const { goToScreen, setSelectedCategory, categories } = this.props

    if (section) {
      const params = {
        ...item,
        mainCategory: section,
      }
      setSelectedCategory(params)
      goToScreen('CategoryDetail', params, false)
    } else {
      //alert('here');
      goToScreen(item.routeName, item.params, false)
    }
  }

  // goProfile = () => {
  //   this.props.navigation.navigate('UserProfile')
  // }

  render() {
    const {
      userProfile,
      backgroundMenu,
      colorTextMenu,
      categories,
    } = this.props
    const user = userProfile.user
    const avatar = Tools.getAvatar(user)
    const name = Tools.getName(user)

    return (
      <View
        style={[
          styles.container,
          {
            marginRight:5,
            elevation:10,
            shadowOffset:{width:2.5,height:0},
            shadowOpacity:0.3,
            shadowRadius:4,
            backgroundColor: backgroundMenu,
          },
        ]}>
        <View
          style={[
            styles.avatarBackground,
            {
              backgroundColor: backgroundMenu,
            },
          ]}>
          {/* <Image
            source={avatar}
            style={[styles.avatar, I18nManager.isRTL && { left: -20 }]}
          /> */}

          <View style={styles.textContainer}>
          <TouchableOpacity onPress={()=>this.props.goToScreen('UserProfile', false, false)}>
            <Text
              style={[
                styles.fullName,
                {
                  color: "#fff",
                  padding:5
                },
              ]}>
              {name}
            </Text>
            </TouchableOpacity>
            <Text
              style={[
                styles.email,
                {
                  color: "#fff",
                  padding:5
                },
              ]}>
              {user ? user.email : ''}
            </Text>
          </View>
          
        </View>
          <View style={styles.languageContainer}>
          {this.state.isLoadingkr?<ActivityIndicator size="small" style={styles.flagContainer}  color="#000" />:
              <TouchableOpacity onPress={(lang)=>this._handlePressLang('kr')} style={styles.flagContainer}>
              <Image
                source={require('../../../images/uae_flag.png')}
                style={[styles.avatar]}
                /> 
                {/* <Text style={[
                styles.email,
                {
                  padding:3,
                  fontSize:10,
                  fontFamily: Constants.fontFamily,
                },
                ]}>
                كردي
                </Text> */}
              </TouchableOpacity>}
              {this.state.isLoadingen?<ActivityIndicator size="small" style={styles.flagContainer}  color="#000" />:
              <TouchableOpacity onPress={(lang)=>this._handlePressLang('en')} style={styles.flagContainer}>
                <Image
                source={require('../../../images/us_flag.png')}
                style={[styles.avatarEng]}
                /> 
                {/* <Text style={[
                styles.email,
                {
                  padding:3,
                  //fontFamily: Constants.fontFamily,
                  fontSize:10
                },
                ]}>
                English
                </Text> */}
              </TouchableOpacity>}
          {this.state.isLoadingar?<ActivityIndicator size="small" style={styles.flagContainer}  color="#000" />:
              <TouchableOpacity onPress={(lang)=>this._handlePressLang('ar')} style={styles.flagContainer}>
                <Image
                source={require('../../../images/iraq_flag.png')}
                style={[styles.avatar]}
                /> 
                {/* <Text style={[
                styles.email,
                {
                  padding:3,
                  fontFamily: Constants.fontFamily,
                  fontSize:10
                },
                ]}>
                عربى
                </Text> */}
              </TouchableOpacity>}
              
             
              <View style={styles.flagContainerExtra}></View>
          </View>
        <ScrollView>
          {this.buttonList.map((item, index) => (
            <DrawerButton
              {...item}
              key={index}
              onPress={() => this._handlePress(item)}
              icon={null}
              uppercase
              colorText={colorTextMenu}
              textStyle={styles.textItem}
            />
          ))}
          {/* {this._renderRowCategories()} */}
        </ScrollView>
      </View>
    )
  }
}

const mapStateToProps = ({ user, categories, netInfo, language }) => ({
  userProfile: user,
  netInfo,
  categories,
  selectedCategory: categories.selectedCategory,
  language: language.lang,
})

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { netInfo } = stateProps
  const { dispatch } = dispatchProps
  //const { actions } = require('@redux/CategoryRedux')
  const { actions } = require('@redux/LangRedux')
  // return {
  //   ...ownProps,
  //   ...stateProps,
  //   setSelectedCategory: (category) =>
  //     dispatch(actions.setSelectedCategory(category)),
  //   fetchCategories: () => {
  //     if (!netInfo.isConnected) return toast(Languages.noConnection)
  //     actions.fetchCategories(dispatch)
  //   },
  // }
  return {
    ...ownProps,
    ...stateProps,
    switchLanguage: (language) => actions.switchLanguage(dispatch, language),
    switchRtl: (rtl) => actions.switchRtl(dispatch, rtl),
  };
}

export default connect(mapStateToProps, null, mergeProps)(DrawerMultiChild)
