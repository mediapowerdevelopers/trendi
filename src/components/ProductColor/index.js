/** @format */

import React from "react";
import { StyleSheet, TouchableOpacity, View, Image } from "react-native";

const hitSlop = { top: 2, right: 20, bottom: 2, left: 20 };

const ProductColor = (props) => {
  //console.log("prop imageeee",props.image)
  return (
    <TouchableOpacity
      activeOpacity={0.9}
      hitSlop={hitSlop}
      style={[props.selected?styles.productColorSelected:styles.productView, {margin:2}]}
      onPress={() => props.onPress()}>
      {/* <View
        style={[
          styles.productColor,
          { backgroundColor: props.color },
          
          props.selected && [
            styles.productColorSelected,
            { borderColor: props.color },
          ],
          props.style,
        ]}
        <Image source={{uri:props.image}} />
      /> */}
      <Image style={props.selected?styles.productColorSelected:styles.productView} source={{uri:props.image}} width={40} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  productView: {
   
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    borderRadius:20,
    width: 40,
    height: 40,
    resizeMode:'cover'
  },
  productColor: {
    width: 16,
    height: 16,
    borderRadius: 8,
  },
  productColorSelected: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    resizeMode:'cover',
    backgroundColor: "white",
    width: 42,
    height: 42,
    borderRadius: 21,
    borderWidth: 2,
    borderColor:'#000'
  },
});

export default ProductColor;
