/** @format */

import React from "react";
import {
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Animated,
  Text,
  I18nManager
} from "react-native";
import styles from "./style";
import Icon from "react-native-vector-icons/Ionicons";
import { Icons, Languages, Color, withTheme } from "@common";

@withTheme
class SearchBar extends React.Component {
  render() {
    let {
      autoFocus,
      value,
      onChangeText,
      onSubmitEditing,
      scrollY,
      onClear,
      onFilter,
      isShowFilter,
      haveFilter,
    } = this.props;

    const {
      theme: {
        colors: {text, lineColor },
      },
    } = this.props;

    const transformY = scrollY.interpolate({
      inputRange: [0, 50],
      outputRange: [50, 0],
      extrapolate: "clamp",
    });

    return (
      <Animated.View
        style={[
          styles.container,
          {
            // transform: [{ translateY: transformY }],
          },
          { backgroundColor: lineColor },
        ]}>
        <Icon name={Icons.Ionicons.Search} size={20} color={text} />
        {I18nManager.isRTL?
        (<TouchableOpacity onPress={onFilter} style={{flex: 1, marginLeft: 10, height: 35, justifyContent: 'center',alignItems:'flex-start'}}>
          <Text>{Languages.SearchPlaceHolder}</Text>
        </TouchableOpacity>):
           (<TextInput
            placeholder={Languages.SearchPlaceHolder}
            editable={true}
            placeholderTextColor={text}
            style={[styles.input, {color: text} ]}
            underlineColorAndroid="transparent"
            autoFocus={true}
            value={value}
            onChangeText={onChangeText}
            onSubmitEditing={onSubmitEditing}
          />)
        }
        {value.length > 0 && (
          <TouchableOpacity onPress={onClear}>
            <Image
              source={require("@images/ic_clear_search.png")}
              style={[styles.icon, { tintColor: text }]}
            />
          </TouchableOpacity>
        )}


          <View style={[styles.separator, { tintColor: text }]} />

          <TouchableOpacity onPress={onFilter}>
            <Image
              source={require("@images/ic_filter_search.png")}
              style={[
                styles.icon,
                { tintColor: haveFilter ? Color.primary : text },
              ]}
            />
          </TouchableOpacity>

      </Animated.View>
    );
  }
}

export default SearchBar;
