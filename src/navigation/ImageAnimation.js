/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { View, Text, Image, TouchableOpacity, Animated, Dimensions, StyleSheet, I18nManager } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
import { Icons, Languages, Color, withTheme } from "@common";

import resolveAssetSource from 'resolveAssetSource';
import Modal from 'react-native-modalbox';


const { width, height } = Dimensions.get('window');

const splash_logo = require("./../images/intro/logo_splash.png");

export default class ImageAnimation extends PureComponent {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
  };

  state = {
    showModal: false,
    heightState: new Animated.Value(height),
    widthState: new Animated.Value(width),
    introImages: [
      require('./../images/intro/01.png') ,
      require('./../images/intro/02.png') ,
      require('./../images/intro/03.png') ,
      require('./../images/intro/04.png') ,
      require('./../images/intro/05.png') ,
    ],
  }
  isMaximum = true;
  counter = 1;

  async componentWillMount() {
    try {
        const value = await AsyncStorage.getItem('@first_used')
        if(value !== null && value == 'true') {
            this.props.navigation.navigate('AppNavigator')
          // value previously stored
        }
      } catch(e) {
        // error reading value
    }
    this.playAnimation();
  }

  playAnimation() {
    Animated.timing(
      this.state.widthState, // The animated value to drive
      {
        toValue: this.isMaximum ? width * 1.2 : width, // Animate to opacity: 1 (opaque)
        duration: 900, // Make it take a while
      }
    ).start(()=> {
      this.counter++;
      if( this.counter == 5 ) {
        // this.counter = 0;
        this.setState({ showModal: true })
        return;
      }
      if( this.imageRef ) {
        this.imageRef.setNativeProps({
          source: [resolveAssetSource(this.state.introImages[this.counter])]
        })
      }
      this.isMaximum = this.isMaximum == true ? false : true; 
      this.playAnimation();
    });
  }

  renderAnimation() {
    let { heightState, widthState } = this.state;
    return ( 
      <View style={{flex: 1}}>
        <Animated.Image 
        ref={(imageRef)=> this.imageRef = imageRef} 
        source={require(`./../images/intro/01.png`)} 
        style={{flex: 1, width: widthState, height: heightState }}/>
        <View style={animationStyles.trendiLogoContainer}>
          <Image source={splash_logo} style={animationStyles.trendiLogo}/>
        </View>
        <Modal
          isOpen={this.state.showModal}
          position={"bottom"}
          backdropPressToClose={false}
          backdropColor="transparent"
          swipeToClose={false}
          style={animationStyles.modalContainer}>
          <View style={animationStyles.modalContent}>
            <TouchableOpacity style={animationStyles.topArrowButton}
            activeOpacity={1}>
              <Image source={require('../images/intro/why_us/down.png')} style={animationStyles.topArrowImage}/>
            </TouchableOpacity>
            <View style={{flex: 0.35}}></View>
            <View style={animationStyles.singleViewContainer}>
              <Text style={animationStyles.headingText}> WELCOME </Text>
              {/* <Text style={animationStyles.headingDetailsText}> {I18nManager.isRTL?  }Discover the best luxury clothes and accessories for{'\n'}Men, Women & Kids. </Text> */}
            </View>
            <View style={{ flex: 0.4, justifyContent: 'flex-end', alignItems: 'baseline'}}>
              <Text style={animationStyles.headingText}> WHY US? </Text>
            </View>
            <View style={animationStyles.singleViewContainer}>
              <Image source={require('../images/intro/why_us/delivery.png')} style={animationStyles.imageIcons}/>
              <Text style={animationStyles.detailsText}> Free Delivery*{'\n'}& Cash on Delivery </Text>
            </View>
            <View style={animationStyles.singleViewContainer}>
              <Image source={require('../images/intro/why_us/brands.png')} style={animationStyles.imageIcons}/>
              <Text style={animationStyles.detailsText}> Browse over 10,000{'\n'}designer clothes & accessories </Text>
            </View>
            <View style={animationStyles.bottomButtonContainer}>
              <TouchableOpacity style={animationStyles.bottomButton} onPress={()=> this.props.navigation.navigate('AppNavigator')}>
                <Text style={animationStyles.buttonText}> CONTINUE </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    )
  }

  render() {
    let { heightState, widthState } = this.state;
    return ( 
      <View style={{flex: 1}}>
        <Animated.Image 
        ref={(imageRef)=> this.imageRef = imageRef} 
        source={require(`./../images/intro/01.png`)} 
        style={{flex: 1, width: widthState, height: heightState }}/>
        <View style={animationStyles.trendiLogoContainer}>
          <Image source={splash_logo} style={animationStyles.trendiLogo}/>
        </View>
        <Modal
          isOpen={this.state.showModal}
          position={"bottom"}
          backdropPressToClose={false}
          backdropColor="transparent"
          swipeToClose={false}
          style={animationStyles.modalContainer}>
          <View style={animationStyles.modalContent}>
            <TouchableOpacity style={animationStyles.topArrowButton}
            activeOpacity={1}>
              <Image source={require('../images/intro/why_us/down.png')} style={animationStyles.topArrowImage}/>
            </TouchableOpacity>
            <View style={{flex: 0.35}}></View>
            <View style={animationStyles.singleViewContainer}>
              <Text style={animationStyles.headingText}> {Languages.welcome} </Text>
              {/* <Text style={animationStyles.headingDetailsText}> Discover the best luxury clothes and accessories for{'\n'}Men, Women & Kids. </Text> */}
              <Text style={animationStyles.headingDetailsText}> {Languages.discover} </Text>
            </View>
            <View style={{ flex: 0.4, justifyContent: 'flex-end', alignItems: 'baseline'}}>
              {/* <Text style={animationStyles.headingText}> WHY US? </Text> */}
              <Text style={animationStyles.headingText}> {Languages.whyUs} </Text>
            </View>
            <View style={animationStyles.singleViewContainer}>
              <Image source={require('../images/intro/why_us/delivery.png')} style={animationStyles.imageIcons}/>
              <Text style={animationStyles.detailsText}> {Languages.freeDelivery} </Text>
              {/* <Text style={animationStyles.detailsText}> Free Delivery*{'\n'}& Cash on Delivery </Text> */}
            </View>
            <View style={animationStyles.singleViewContainer}>
              <Image source={require('../images/intro/why_us/brands.png')} style={animationStyles.imageIcons}/>
              <Text style={animationStyles.detailsText}> {Languages.browse} </Text>
              {/* <Text style={animationStyles.detailsText}> Browse over 10,000{'\n'}designer clothes & accessories </Text> */}
            </View>
            <View style={animationStyles.bottomButtonContainer}>
              <TouchableOpacity style={animationStyles.bottomButton} onPress={()=> this.props.navigation.navigate('AppNavigator')}>
                <Text style={animationStyles.buttonText}> {Languages.continue} </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    )
  }
}


const animationStyles = StyleSheet.create(
  {
    mainContent: {
      flex: 1,
      alignItems: "center",
      justifyContent: "space-around",
    },
    image: {
      width: 320,
      height: 320,
    },
    text: {
      color: "rgba(255, 255, 255, 0.8)",
      backgroundColor: "transparent",
      textAlign: "center",
      paddingHorizontal: 16,
    },
    title: {
      fontSize: 22,
      color: "white",
      backgroundColor: "transparent",
      textAlign: "center",
      marginBottom: 16,
    },
    buttonCircle: {
      width: 40,
      height: 40,
      backgroundColor: "rgba(0, 0, 0, .2)",
      borderRadius: 20,
      justifyContent: "center",
      alignItems: "center",
    },
    trendiLogoContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'transparent',
      position: 'absolute',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0
    },
    trendiLogo: {
      width: '50%',
      height: '50%',
      resizeMode: 'contain'
    },
    modalContainer: {
      flex: 1,
      backgroundColor: 'transparent',
      justifyContent: 'flex-end'
    },
    modalContent: {
      flex: 0.65,
      backgroundColor: 'white', 
      alignItems: "center"
    },
    topArrowButton: {
      position: 'absolute', 
      top: -20,
      borderTopLeftRadius: 80,
      borderTopRightRadius: 80,
      width: 110,
      height: 50,
      backgroundColor: '#fff',
      justifyContent: 'center',
      alignItems: 'center'
    },
    topArrowImage: {
      width: 20,
      height: 20, 
      resizeMode: 'contain'
    },
    singleViewContainer: {
      flex: 1, 
      justifyContent: 'center',
      alignItems: 'center',
      width: '85%'
    },
    headingText: {
      fontSize: 21,
      fontWeight: 'bold',
      letterSpacing: 2,
      color: '#111'
    },
    headingDetailsText: {
      fontSize: 12,
      color: '#aeaeae',
      textAlign: 'center',
      marginTop: 5,
      lineHeight: 18
    },
    imageIcons: {
      width: 30,
      height: 30,
      resizeMode: 'contain'
    },
    detailsText: {
      fontSize: 12,
      color: '#000',
      textAlign: 'center',
      marginTop: 5
    },
    bottomButtonContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      width: '70%'
    },
    bottomButton: {
      backgroundColor: '#000',
      height: 45,
      width: '100%',
      borderRadius: 7,
      justifyContent: 'center',
      alignItems: 'center'
    },
    buttonText: {
      fontSize: 12,
      color: '#fff',
      fontWeight: 'bold'
    }
  }
  
)