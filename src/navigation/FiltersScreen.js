/** @format */

import React, { PureComponent } from "react";

import { Images, Styles, withTheme, Languages, Color } from "@common";
import { Filters } from "@containers";
import { Back,Title,Logo } from "./IconNav";

@withTheme
export default class FiltersScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => {
    const dark = navigation.getParam("dark", false);
    const text = navigation.getParam("text", Color.headerTintColor);
    return {
      headerLeft: Back(navigation, Images.icons.back, dark),
      headerStyle:{
        borderBottomWidth:0,
        shadowOffset: {
        width: 0,
        height: 2.0,
      },
      shadowOpacity: 0.10,
      shadowRadius: 3.56,elevation:6},
      headerTitle: Logo(),
     // headerTransparent: true,
    };
  };

  componentWillMount() {
    const {
      theme: { dark },
    } = this.props;

    this.props.navigation.setParams({
      dark,
    });
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.theme.dark !== nextProps.theme.dark) {
      const {
        theme: { dark },
      } = nextProps;
      this.props.navigation.setParams({
        dark,
      });
    }
  }

  render() {
    const { navigation } = this.props;

    return (
      <Filters navigation={navigation} onBack={() => navigation.goBack()} />
    );
  }
}
