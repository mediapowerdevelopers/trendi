/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { View, TouchableOpacity, Image, I18nManager } from "react-native";
import { connect } from 'react-redux'
import AsyncStorage from '@react-native-community/async-storage';
import RNRestart from "react-native-restart";

import { warn } from "@app/Omni";
import { Color, Styles, withTheme } from "@common";
import { Home } from "@containers";
import { Menu, NavBarLogo, HeaderHomeRight, Logo, EmptyView } from "./IconNav";
import styles from './styles'

let _this = null;

//@withTheme
class HomeScreen extends PureComponent {

  constructor(props) {
    super(props)
    this._handlePressLang = this._handlePressLang.bind(this);
    this.state = { selectedOption: this.props.language.lang,
    isLoadingar: false,
    isLoadingen: false,
    isLoadingkr: false
  }
  }

  async componentDidMount(){
    _this = this;
    var myLang = await AsyncStorage.getItem('@current_language');
    this.setState({selectedOption:myLang?myLang:this.props.language.lang});
    //alert("mount");
  }

  async _handlePressLang(lang) {
    //alert(lang);
  
    const { switchLanguage, switchRtl, language } = this.props;
    const  selectedOption  = lang;
   
  // if (selectedOption !== language.lang) {
      const isRtl = (selectedOption === "ar" || selectedOption === "kr");
      if(selectedOption==="ar"){
        this.setState({ isLoadingar: true });
      }else if(selectedOption==="en"){
        this.setState({ isLoadingen: true });
      }else{
        this.setState({ isLoadingkr: true });
      }
      this.setState({ loading: true });
      //alert(selectedOption);
      //return;
      try {
        await AsyncStorage.setItem('@current_language', selectedOption);
      } catch (e) {
        // saving error
      }
      
      switchLanguage({
        lang: selectedOption,
        rtl: isRtl,
      });

      

      I18nManager.forceRTL(isRtl);
     // alert("here");
      setTimeout(() => {
        
        //this.setState({ isLoading: false });
        RNRestart.Restart();
      }, 1000);
      
   // }
  };

  static navigationOptions = ({ navigation }) => {
    const headerStyle = navigation.getParam(
      "headerStyle",
      Styles.Common.toolbar()
    );


    
    //const dark = navigation.getParam("dark", false);

    return {
      headerTitle: Logo(),
      headerLeft: Menu(),
      // headerRight: (<View style={styles.languageContainer}>
            
      //       <TouchableOpacity onPress={(lang)=>_this._handlePressLang('kr')} style={styles.flagContainer}>
      //       <Image
      //         source={require('../images/uae_flag.png')}
      //         style={[styles.avatar]}
      //         />
      //       </TouchableOpacity>
           
      //       <TouchableOpacity onPress={(lang)=>_this._handlePressLang('en')} style={styles.flagContainer}>
      //         <Image
      //         source={require('../images/us_flag.png')}
      //         style={[styles.avatarEng]}
      //         />
      //       </TouchableOpacity>
        
      //       <TouchableOpacity onPress={(lang)=>_this._handlePressLang('ar')} style={styles.flagContainer}>
      //         <Image
      //         source={require('../images/iraq_flag.png')}
      //         style={[styles.avatar]}
      //         />
      //       </TouchableOpacity>
      //   </View>),

      headerTintColor: Color.headerTintColor,
      headerStyle:{
        borderBottomWidth:0,
        shadowOffset: {
        width: 0,
        height: 2.0,
      },
      shadowOpacity: 0.10,
      shadowRadius: 3.56,elevation:6},
      headerTitleStyle: Styles.Common.headerStyle,
      headerRight:EmptyView(),

      // use to fix the border bottom
      headerTransparent: false,
    };
  };

  static propTypes = {
    navigation: PropTypes.object.isRequired,
  };

  componentWillMount() {
    // const {
    //   theme: {
    //     colors: { background },
    //     //dark,
    //   },
    // } = this.props;

    this.props.navigation.setParams({
      //headerStyle: Styles.Common.toolbar(background, dark),
      headerStyle:{borderBottomWidth:0.3,elevation:3},
      //dark,
    });
  }

  componentWillReceiveProps(nextProps) {
    // if (this.props.theme.dark !== nextProps.theme.dark) {
    //   const {
    //     theme: {
    //       colors: { background },
    //       dark,
    //     },
    //   } = nextProps;
    //   this.props.navigation.setParams({
    //     headerStyle: Styles.Common.toolbar(background, dark),
    //     dark,
    //   });
    // }
  }

  render() {
    const { navigate } = this.props.navigation;

    return (
      <Home
        onShowAll={(config, index) =>
          navigate("CategoryScreen", { config, index })
        }
        showCategoriesScreen={() => navigate("CategoriesScreen")}
        onViewProductScreen={(item) => {
          this.props.navigation.tabBarVisible = false;
          navigate("DetailScreen", item);
        }}
      />
    );
  }
}

const mapStateToProps = ({ user, categories, netInfo, language }) => ({
  userProfile: user,
  netInfo,
  categories,
  selectedCategory: categories.selectedCategory,
  language: language.lang,
})

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { netInfo } = stateProps
  const { dispatch } = dispatchProps
  //const { actions } = require('@redux/CategoryRedux')
  const { actions } = require('@redux/LangRedux')
  // return {
  //   ...ownProps,
  //   ...stateProps,
  //   setSelectedCategory: (category) =>
  //     dispatch(actions.setSelectedCategory(category)),
  //   fetchCategories: () => {
  //     if (!netInfo.isConnected) return toast(Languages.noConnection)
  //     actions.fetchCategories(dispatch)
  //   },
  // }
  return {
    ...ownProps,
    ...stateProps,
    switchLanguage: (language) => actions.switchLanguage(dispatch, language),
    switchRtl: (rtl) => actions.switchRtl(dispatch, rtl),
  };
}

export default connect(mapStateToProps, null, mergeProps)(HomeScreen)
