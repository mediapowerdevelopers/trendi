/** @format */

import React, { PureComponent } from "react";
import { Menu, EmptyView,Logo,Back,Back3} from "./IconNav";

import { Images, Config, Constants, Color, Styles, Languages } from "@common";
import { WishList } from "@containers";

export default class WishListScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => {
    const dark = navigation.getParam("dark", false);
    const headerStyle = navigation.getParam(
      "headerStyle",
      Styles.Common.toolbar()
    );

    return {
      headerLeft: Back3(navigation, null, dark),
      headerRight: EmptyView(),
      headerTitle: Logo(),

      headerTintColor: Color.headerTintColor,
      headerStyle:{
        borderBottomWidth:0,
        shadowOffset: {
        width: 0,
        height: 2.0,
      },
      shadowOpacity: 0.10,
      shadowRadius: 3.56,elevation:6},
      headerTitleStyle: Styles.Common.headerStyle,
    };
  };

  render() {
    const { navigate } = this.props.navigation;
    // const rootNavigation = this.props.screenProps.rootNavigation;

    return (
      <WishList
        onViewProduct={(product) => navigate("Detail", product)}
        onViewHome={() => navigate("Default")}
      />
    );
  }
}
