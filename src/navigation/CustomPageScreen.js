/** @format */

import React, { PureComponent } from "react";
import { View,I18nManager } from "react-native";
import { WebView } from "react-native-webview";
import { Color, Styles, withTheme, Languages} from "@common";
import { CustomPage } from "@containers";
import { Menu, NavBarLogo, Back2,EmptyView, Back,Logo } from "./IconNav";


@withTheme
export default class CustomPageScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => {
    const headerStyle = navigation.getParam(
      "headerStyle",
      Styles.Common.toolbar()
    );
    const dark = navigation.getParam("dark", false);
    const isBack = navigation.getParam("isBack", false);
    return {
      headerTitle: Logo(),
      headerLeft:Back2(navigation, null, dark),
      headerRight:EmptyView(),
      headerTintColor: Color.headerTintColor,
      headerStyle:{
        borderBottomWidth:0,
        shadowOffset: {
        width: 0,
        height: 2.0,
      },
      shadowOpacity: 0.10,
      shadowRadius: 3.56,elevation:6},
    };
  };

  componentWillMount() {
    const {
      theme: {
        colors: { background },
        dark,
      },
    } = this.props;

    this.props.navigation.setParams({
      headerStyle: Styles.Common.toolbar(background, dark),
      dark,
    });
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.theme.dark !== nextProps.theme.dark) {
      const {
        theme: {
          colors: { background },
          dark,
        },
      } = nextProps;
      this.props.navigation.setParams({
        headerStyle: Styles.Common.toolbar(background, dark),
        dark,
      });
    }
  }

  render() {
    const { state } = this.props.navigation;
    if (typeof state.params === "undefined") {
      return <View />;
    }

    if (typeof state.params.url !== "undefined") {
      //alert(state.params.title);
      var url;
      if(state.params.title==="aboutus"){
        url = I18nManager.isRTL?(Languages._language==='ar'?"https://trendi-iq.com/about-us-arabic/":"https://trendi-iq.com/about-us-kurdish/"):"https://trendi-iq.com/about-us/";
      }else if(state.params.title==="shipping"){
        url = I18nManager.isRTL?(Languages._language==='ar'?"https://trendi-iq.com/shipping-policy-arabic/":"https://trendi-iq.com/shipping-policy-kurdish/"):"https://trendi-iq.com/shipping-policy/";  
      }else if(state.params.title==="privacy"){
        url = I18nManager.isRTL?(Languages._language==='ar'?"https://trendi-iq.com/privacy-policy-arabic/":"https://trendi-iq.com/privacy-policy-kurdish/"):"https://trendi-iq.com/privacy-policy/";  
      }else if(state.params.title==="terms"){
        url = I18nManager.isRTL?(Languages._language==='ar'?"https://trendi-iq.com/terms-conditions-arabic/":"https://trendi-iq.com/terms-conditions-kurdish/"):"https://trendi-iq.com/terms-conditions/";
      }else if(state.params.title==="return"){
        url = I18nManager.isRTL?(Languages._language==='ar'?"https://trendi-iq.com/return-exchange-arabic/":"https://trendi-iq.com/return-exchange-kurdish/"):"https://trendi-iq.com/return-exchange/";
      }else{
        url = state.params.url;
      }
        return (
          <View style={{ flex: 1 }}>
            <WebView startInLoadingState source={{ uri: url }} />
          </View>
        );
    }

    return (
      // <View>
        <CustomPage id={state.params.id} />
      // </View>
    );
  }
}
