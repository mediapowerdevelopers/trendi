/** @format */

import React, { Component } from "react";
import PropTypes from "prop-types";
import { Logo, Back, EmptyView } from "./IconNav";

import { Color, Styles, withTheme } from "@common";

import { MyOrders } from "@containers";

export default class MyOrdersScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const dark = navigation.getParam("dark", false);
    const headerStyle = navigation.getParam(
      "headerStyle",
      Styles.Common.toolbar()
    );

    return {
      headerTitle: Logo(),
      tabBarVisible: false,
      headerLeft: Back(navigation, null, dark),
      headerRight: EmptyView(),
      headerTintColor: Color.headerTintColor,
      headerStyle:{
        borderBottomWidth:0,
        shadowOffset: {
        width: 0,
        height: 2.0,
      },
      shadowOpacity: 0.10,
      shadowRadius: 3.56,elevation:6},
      headerTitleStyle: Styles.Common.headerStyle,
    };
  };


  static propTypes = {
    navigation: PropTypes.object,
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <MyOrders
        navigate={this.props.navigation}
        onViewHomeScreen={() => navigate("Default")}
        onViewOrderDetail={(id) => navigate("OrderDetailScreen", { id })}
      />
    );
  }
}
