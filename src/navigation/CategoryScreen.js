/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import { Color, Images, Styles, withTheme } from "@common";
import { TabBarIcon } from "@components";
import { Category } from "@containers";
import { Logo, Back,Back2, EmptyView } from "./IconNav";

@withTheme
export default class CategoryScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => {

    const dark = navigation.getParam("dark", false);
    const headerStyle = navigation.getParam(
      "headerStyle",
      Styles.Common.toolbar()
    );

    return {
      headerTitle: Logo(),
      headerLeft: Back2(navigation, null, dark),
      //headerLeft: EmptyView(),
      headerRight: EmptyView(),
      tabBarIcon: ({ tintColor }) => (
        <TabBarIcon
          css={{ width: 18, height: 18 }}
          icon={Images.IconCategory}
          tintColor={tintColor}
        />
      ),

      headerTintColor: Color.headerTintColor,
      headerStyle:{
        borderBottomWidth:0,
        shadowOffset: {
        width: 0,
        height: 2.0,
      },
      shadowOpacity: 0.10,
      shadowRadius: 3.56,elevation:6},
      headerTitleStyle: Styles.Common.headerStyle,
    };
  };

  static propTypes = {
    navigation: PropTypes.object.isRequired,
  };

  componentWillMount() {
    const {
      theme: {
        colors: { background },
        dark,
      },
    } = this.props;

    this.props.navigation.setParams({
      headerStyle: Styles.Common.toolbar(background, dark),
      dark,
    });
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.theme.dark !== nextProps.theme.dark) {
      const {
        theme: {
          colors: { background },
          dark,
        },
      } = nextProps;
      this.props.navigation.setParams({
        headerStyle: Styles.Common.toolbar(background, dark),
        dark,
      });
    }
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Category
        onViewProductScreen={(item) => {
          navigate("DetailScreen", item);
        }}
        onSearchPress={() => {
          navigate("Search");
        }}
      />
    );
  }
}
