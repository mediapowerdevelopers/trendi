/** @format */

import React, { PureComponent } from "react";

import { Cart } from "@containers";
import { Menu, NavBarLogo, HeaderHomeRight,  Logo , EmptyView, Back} from "./IconNav";

import { Color, Styles, withTheme } from "@common";


export default class CartScreen extends PureComponent {
  
  static navigationOptions = ({ navigation }) => {
    const dark = navigation.getParam("dark", false);
    const headerStyle = navigation.getParam(
      "headerStyle",
      Styles.Common.toolbar()
    );

    return {
      headerTitle: Logo(),
      headerLeft:Back(navigation, null, dark),
      headerRight:EmptyView(),
      headerTintColor: Color.headerTintColor,
      headerStyle:{
        borderBottomWidth:0,
        shadowOffset: {
        width: 0,
        height: 2.0,
      },
      shadowOpacity: 0.10,
      shadowRadius: 3.56,elevation:6},
    };
  };


  render() {
    const { navigate } = this.props.navigation;

    return (
      <Cart
        onMustLogin={() => {
          navigate("LoginScreen", { onCart: true });
        }}
        onBack={() => navigate("Default")}
        onFinishOrder={() => navigate("MyOrders")}
        onViewHome={() => navigate("Default")}
        onViewProduct={(product) => navigate("Detail", product)}
        navigation={this.props.navigation}
      />
    );
  }
}
