/** @format */

import React, { Component } from "react";
import wp from "@services/PostAPI";
import WebView from "@components/WebView/WebView";

export default class CustomPage extends Component {
  constructor(props) {
    super(props);
    this.state = { html: "Loading" };
    this.fetchPage = this.fetchPage.bind(this);
  }

  componentDidMount() {
    console.log("id",this.props.id);
    this.fetchPage(this.props.id);
  }

  componentWillReceiveProps(nextProps) {
    this.fetchPage(nextProps.id);
  }

  fetchPage(id) {
    wp.pages()
      .id(id)
      .get((err, data) => {
        console.log(data);
        if (data) {
          this.setState({
            html:
              typeof data.content.rendered !== "undefined"
                ? data.content.rendered
                : "Content is updating",
          });
        }else{
          this.setState({html:"No page found"});
        }
      });
  }

  render() {
    return (<WebView html={this.state.html} />);
  }
}
