/** @format */

import React from "react";
import { View, Text } from "react-native";

// import { log } from "@app/Omni";
import { Languages, Tools } from "@common";
import { ConfirmCheckout } from "@components";
import styles from "./styles";

export default class LineItemsAndPrice extends React.PureComponent {
  render() {
    const {
      order,
      theme: {
        colors: { text, primary },
      },
    } = this.props;
    let myTotal = 0;  
    //console.log(order);
    //console.log("discount total",order.discount_total);
    return (
      <View>
        <View style={styles.header}>
          <Text style={styles.label(text)}>
            {Languages.OrderId} #{order.id}
          </Text>
        </View>
        <View style={styles.itemContainer}>
          {order.line_items.map((o, i) => {
            myTotal+=parseFloat(o.total);
            var  order = o.name.split("-");
            var name = order[0];
            var color = "";
            var size = "";
           if(o.meta_data.length>0){
            for(p=0;p<o.meta_data.length;p++){
              if(o.meta_data[p].key=="pa_color"){
                color=o.meta_data[p].value;
                color = color.replace(/\-[0-9]+/, '').replace(/\-/g, ' ').replace(/[a-z]/i, function(t) { return t.toUpperCase() });
              }
              if(o.meta_data[p].key=="pa_size"){
                size=o.meta_data[p].value.toUpperCase();
              }
            }
           }
            return (
              <View key={i.toString()} style={styles.lineItem}>
                <Text
                  style={styles.name(text)}
                  //numberOfLines={2}
                  ellipsizeMode="tail">
                  {name}
                  {color!="" &&  `\nColor : ${color}` }
                  {size!="" &&  `\nSize : ${size}` }
                </Text>
                {/* {o.meta_data.map((p, i) => {  return  (   <Text style={{width:'100%',textAlign:'left'}}  numberOfLines={1}>{p.key}</Text>) })} */}
                <Text style={styles.text(text)}>{`x${o.quantity}`}</Text>
                <Text style={styles.text(text)}>
                  {Tools.getCurrecyFormatted(o.total)}
                </Text>
              </View>
            );
          })}
          
        </View>
        <ConfirmCheckout
          shippingMethod={order.shipping_total}
          totalPrice={myTotal}
          discountTotal={order.discount_total}
          style={{ margin: 0 }}
          totalStyle={{ color: "#000", fontSize:16, fontWeight: "bold" }}
          labelStyle={{ color: text, fontWeight: "bold" }}
        />
      </View>
    );
  }
}
