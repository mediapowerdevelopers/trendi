/** @format */

import { StyleSheet, Platform, Dimensions } from "react-native";
import { Color, Constants } from "@common";

const { width } = Dimensions.get("window");
const cardMargin = Constants.Dimension.ScreenWidth(0.05);

export default StyleSheet.create({
  container: (background) => ({
    backgroundColor: background,
  }),
  contentContainer: {
   // flex:1,
    marginHorizontal: 7,
  },
  name: (text) => ({
    marginBottom: 4,
    color: text,
    width: Constants.Dimension.ScreenWidth(0.6),
    textAlign:'left',
    padding:4
  }),
  title: (text) => ({
    //marginBottom: 4,
    textAlign:'left',
    color: text,
    width: Constants.Dimension.ScreenWidth(0.3),
    textTransform: "uppercase",
    fontWeight: "bold",
    padding:4
  }),
  text: (text) => ({
    
    marginBottom: 4,
    color: text,
    alignSelf: "center",
  }),
  header: {
    marginTop: 20,
    marginBottom: 10,
  },
  label: (text) => ({
    textAlign:'left',
    width: Constants.Dimension.ScreenWidth(0.8),
    fontFamily: Constants.fontHeader,
    fontSize: 18,
    color: text,
  }),
  label2: (text) => ({
    //textAlign:'center',
    textAlign:'left',
    //width: Constants.Dimension.ScreenWidth(0.2),
    fontFamily: Constants.fontHeader,
    fontSize: 18,
    color: text,
    padding:5
  }),
  label3: (text) => ({
    textAlign:'left',
    width: 200,
    fontFamily: Constants.fontHeader,
    fontSize: 20,
    color: text,
    padding:4
  }),
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 8,
  },
  rowLabel: {
    fontSize: 16,
    fontFamily: Constants.fontFamily,
  },
  itemContainer: {},
  lineItem: {
    marginBottom: cardMargin / 2,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  footer: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 20,
  },
  button: {
    width: "40%",
    paddingHorizontal: 12,
    paddingVertical: 6,
    borderRadius: 20,
  },
  noteItem: {
    marginBottom: 15
  },
  noteContent: (text) =>({
    fontSize: 14,
    color: text,
    marginTop: 3,
    textAlign:'left',
    width: Constants.Dimension.ScreenWidth(1),
    padding:4
  }),
  noteTime: {
    fontSize: 12,
    color: "#4a4a4a",
    marginTop: 3
  },
});
