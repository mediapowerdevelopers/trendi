/**
 * Created by InspireUI on 19/02/2017.
 *
 * @format
 */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { View, Text,I18nManager,Image, ImageBackground, Linking, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { Constants, withTheme, AppConfig, Languages } from "@common";
import { HorizonList, ModalLayout, PostList, TouchableScale } from "@components";
import { isEmpty } from "lodash";
import styles from "./styles";
import { warn } from '@app/Omni';
import AsyncStorage from '@react-native-community/async-storage';

class Home extends PureComponent {
  static propTypes = {
    fetchAllCountries: PropTypes.func.isRequired,
    layoutHome: PropTypes.any,
    onViewProductScreen: PropTypes.func,
    onShowAll: PropTypes.func,
    showCategoriesScreen: PropTypes.func,
  };

   componentDidMount() {
    
    const { fetchAllCountries, isConnected, fetchCategories, countries, language } = this.props;
    if (isConnected) {
      const { list } = countries;
      if (!list || isEmpty(list)) {
        fetchCategories();
        fetchAllCountries();
      }
    }

    //Languages.setLanguage(language.lang);

    // Enable for mode RTL
    //I18nManager.forceRTL(language.rtl);
  }

  render() {
    const {
      layoutHome,
      onViewProductScreen,
      showCategoriesScreen,
      onShowAll,
      theme: {
        colors: { background },
      },
      language
    } = this.props;

    const isHorizontal = layoutHome == Constants.Layout.horizon || layoutHome == 7;
    const url = AppConfig.WooCommerce.url;
    const imageUrl = I18nManager.isRTL?(Languages._language==='ar'?`${url}/wp-content/uploads/banners/topAdAr.png`:`${url}/wp-content/uploads/banners/topAdKr.png`):`${url}/wp-content/uploads/banners/topAdEn.png`;
    return (
      <View style={[styles.container, { backgroundColor: background }]}>
        <View style={styles.topAdImageContainer}>
          <ImageBackground source={{uri:imageUrl}} style={{width:'100%',height:45}} imageStyle={{resizeMode: 'contain'}}>
            <TouchableOpacity 
            style={{backgroundColor: 'transparent', opacity: 0.4, width: '52%', height: 45}} 
            onPress={()=> Linking.openURL('instagram://user?username=trendi_shopping_app')}>
            </TouchableOpacity>
          </ImageBackground>
        </View>
        {isHorizontal && (
          <HorizonList
            onShowAll={onShowAll}
            onViewProductScreen={onViewProductScreen}
            showCategoriesScreen={showCategoriesScreen}
          />
        )}

        {!isHorizontal && (
          <PostList parentLayout={layoutHome} onViewProductScreen={onViewProductScreen} />
        )}
        <ModalLayout />
      </View>
    );
  }
}

const mapStateToProps = ({ products, countries, netInfo, language }) => ({
  layoutHome: products.layoutHome,
  countries,
  isConnected: netInfo.isConnected,
  language
});

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps;
  const CountryRedux = require("@redux/CountryRedux");
  const { actions } = require("@redux/CategoryRedux");

  return {
    ...ownProps,
    ...stateProps,
    fetchCategories: () => actions.fetchCategories(dispatch),
    fetchAllCountries: () => CountryRedux.actions.fetchAllCountries(dispatch),
  };
}

export default withTheme(
  connect(
    mapStateToProps,
    undefined,
    mergeProps
  )(Home)
);
