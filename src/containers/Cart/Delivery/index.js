/** @format */

import React, { PureComponent } from "react";
import { Text, View, ScrollView, StyleSheet, Picker as Pic ,ActionSheetIOS, Platform,TextInput} from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from "react-redux";
import Tcomb from "tcomb-form-native";
import { cloneDeep } from "lodash";
import { TextInputMask } from "react-native-masked-text";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import CountryPicker from "react-native-country-picker-modal";

import Buttons from "@cart/Buttons";
import { ShippingMethod } from "@components";
import { Config, Validator, Languages, withTheme, Styles } from "@common";
import { toast } from "@app/Omni";
import css from "@cart/styles";
import styles from "./styles";
import { TouchableOpacity } from "react-native-gesture-handler";


const Form = Tcomb.form.Form;

const customStyle = cloneDeep(Tcomb.form.Form.stylesheet);
const customInputStyle = cloneDeep(Tcomb.form.Form.stylesheet);
//var city="Erbilm";
var selectedCity = "Baghdad";
class Delivery extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      value: {
        first_name: "",
        last_name: "",
        address_1: "",
        state: "",
        postcode: "",
        country: "",
        email: "",
        phone: "",
        note: "",
       
      },
      cca2: "EN",
      city:"Baghdad",
    };

    this.initFormValues();
  }

  componentDidMount() {
    const { getShippingMethod } = this.props;

    this.fetchCustomer(this.props);
    //console.log("city",selectedCity);
    getShippingMethod(Config.shipping.zoneId);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.user !== this.props.user) {
      this.fetchCustomer(nextProps);
    }
  }

  openActionSheet= (locals) => {
    //alert("hello")
    const { getShippingMethod } = this.props;
      ActionSheetIOS.showActionSheetWithOptions(
        {
          options: ['Baghdad','Dohuk', 'Erbil','Mosul','Zakho','Sulaymaniyah'],
          //destructiveButtonIndex: 1,
          //cancelButtonIndex: 0,
        },
        (buttonIndex) => {
          //console.log('button', buttonIndex)
          if (buttonIndex === 0) {
            selectedCity = "Baghdad";
            getShippingMethod(4);
            //city = "Erbil";
            //this.setState({city:'Erbil'});
            locals.onChange("Baghdad");

          }else if (buttonIndex === 1) {
            
            selectedCity = "Dohuk";
            getShippingMethod(5);
            //city = "Dohuk";
            //this.setState({city:'Dohuk'});
            locals.onChange("Dohuk");

          }else if (buttonIndex === 2) {
           
            selectedCity = "Erbil";
            getShippingMethod(6);
            //city = "Kirkuk";
            //this.setState({city:'Kirkuk'});
            locals.onChange("Erbil");

          }else if (buttonIndex === 3) {
            
            selectedCity = "Mosul";
            getShippingMethod(7);
            //city = "Mosul";
            //this.setState({city:'Mosul'});
            locals.onChange("Mosul");

          }else if (buttonIndex === 4) {
            
            selectedCity = "Zakho";
            getShippingMethod(8);
            //city = "Zakho";
            //this.setState({city:'Zakho'});
            locals.onChange("Zakho");

          }else if (buttonIndex === 5) {
            
            selectedCity = "Sulaymaniyah";
            getShippingMethod(9);
            //city = "Zakho";
            //this.setState({city:'Zakho'});
            locals.onChange("Sulaymaniyah");

          }
          
          this.forceUpdate();
        },
      );
  
     // console.log(city);

  }

  _getCustomStyle = () => {
    const {
      theme: {
        colors: { text },
      },
    } = this.props;
    // Customize Form Stylesheet
    customStyle.textbox.normal = {
      ...customStyle.textbox.normal,
      height: 150,
      marginBottom: 200,
      color: text,
      textAlign: "left"
    };
    customStyle.enums = {
      ...customStyle.enums,
      height: 50,
      marginBottom: 200,
      color: text,
      textAlign: "left"
    };
    customStyle.controlLabel.normal = {
      ...customStyle.controlLabel.normal,
      fontSize: 15,
      color: text,
      textAlign: "left"
    };

    return customStyle;
  };

  _getCustomInputStyle = () => {
    const {
      theme: {
        colors: { text },
      },
    } = this.props;

    customInputStyle.controlLabel.normal = {
      ...customInputStyle.controlLabel.normal,
      fontSize: 15,
      color: text,
      textAlign: "left"
    };
    customInputStyle.textbox.normal = {
      ...customInputStyle.textbox.normal,
      color: text,
      width: Styles.width / 2,
      textAlign: "center"
    };
    customInputStyle.textbox.error = {
      ...customInputStyle.textbox.normal,
      color: text,
      width: Styles.width / 2,
      textAlign: "center",
    };
    customInputStyle.formGroup.normal = {
      ...customInputStyle.formGroup.normal,
      flexDirection: "row",
      flexWrap: "wrap",
      alignItems: "center",
      justifyContent: "space-between",
    };
    customInputStyle.formGroup.error = {
      ...customInputStyle.formGroup.normal,
      flexDirection: "row",
      flexWrap: "wrap",
      alignItems: "flex-start",
      justifyContent: "space-between",
    };
    customInputStyle.errorBlock = {
      ...customInputStyle.errorBlock,
      fontSize:14,
      width:'100%',
      textAlign:'right',
      fontStyle:'italic'
     // width: Styles.width,
    };

    return customInputStyle;
  };

  onChange = (value) => this.setState({ value });

  onPress = () => this.refs.form.getValue();

  initFormValues = () => {
    const {
      theme: {
        colors: { placeholder },
      },
    } = this.props;
    // const countries = this.props.countries;
    // override the validate method of Tcomb lib for multi validate requirement.
    // const Countries = Tcomb.enums(countries);
    const Email = Tcomb.refinement(
      Tcomb.String,
      (s) => Validator.checkEmail(s) === undefined
    );
    Email.getValidationErrorMessage = (s) => Validator.checkEmail(s);
    const Phone = Tcomb.refinement(
      Tcomb.String,
      (s) => Validator.checkPhone(s) === undefined
    );
    Phone.getValidationErrorMessage = (s) => Validator.checkPhone(s);

    var City = Tcomb.enums({
      E: 'Erbil',
      D: 'Dohuk',
      K: 'Kirkuk',
      M: 'Mosul',
      Z: 'Zakho'
    });

    // define customer form
    this.Customer = Tcomb.struct({
     // first_name: Tcomb.maybe(Tcomb.String),
     // last_name: Tcomb.maybe(Tcomb.String),
      address_1: Tcomb.String,
      ...(Config.DefaultCountry.hideCountryList
        ? {}
        : { 
          //country: Tcomb.maybe(Tcomb.String) 
        }),
        
      //state: Tcomb.maybe(Tcomb.String),
      city: Tcomb.String,
      phone: Tcomb.String,  
      //postcode: Tcomb.maybe(Tcomb.String),
      //email: Tcomb.maybe(Email),
      
      //note: Tcomb.maybe(Tcomb.String), // maybe = optional
    });

    // form options
    this.options = {
      auto: "none", // we have labels and placeholders as option here (in Engrish, ofcourse).
      // stylesheet: css,
      fields: {
        // first_name: {
        //   label: Languages.FirstName,
        //   placeholder: Languages.TypeFirstName,
        //   error: Languages.EmptyError, // for simple empty error warning.
        //   underlineColorAndroid: "transparent",
        //   stylesheet: this._getCustomInputStyle(),
        //   placeholderTextColor: placeholder,
        // },
        // last_name: {
        //   label: Languages.LastName,
        //   placeholder: Languages.TypeLastName,
        //   error: Languages.EmptyError,
        //   underlineColorAndroid: "transparent",
        //   stylesheet: this._getCustomInputStyle(),
        //   placeholderTextColor: placeholder,
        // },
        address_1: {
          label: Languages.Area,
          placeholder: Languages.TypeAddress,
          error: Languages.EmptyError,
          underlineColorAndroid: "transparent",
          stylesheet: this._getCustomInputStyle(),
          placeholderTextColor: placeholder,
        },
        ...(Config.DefaultCountry.hideCountryList
          ? {}
          : {
            // country: {
            //   label: Languages.TypeCountry,
            //   placeholder: Languages.Country,
            //   error: Languages.NotSelectedError,
            //   stylesheet: this._getCustomInputStyle(),
            //   template: this.renderCountry,
            //   placeholderTextColor: placeholder,
            // },
          }),
         
        // state: {
        //   label: Languages.State,
        //   placeholder: Languages.TypeState,
        //   error: Languages.EmptyError,
        //   underlineColorAndroid: "transparent",
        //   stylesheet: this._getCustomInputStyle(),
        //   autoCorrect: false,
        //   placeholderTextColor: placeholder,
        // },
        city: {
          label: Languages.City,
          placeholder: Languages.TypeCity,
          error: Languages.EmptyError,
          underlineColorAndroid: "transparent",
          stylesheet: this._getCustomInputStyle(),
          template: this.renderCity,
          //autoCorrect: false,
          placeholderTextColor: placeholder,
          //label: 'City'
        },
        phone: {
          label: Languages.Phone,
          placeholder: Languages.TypePhone,
          underlineColorAndroid: "transparent",
          error: Languages.EmptyError,
          stylesheet: this._getCustomInputStyle(),
          template: this.renderPhoneInput,
          autoCorrect: false,
          placeholderTextColor: placeholder,
        },
        // postcode: {
        //   label: Languages.Postcode,
        //   placeholder: Languages.TypePostcode,
        //   error: Languages.EmptyError,
        //   underlineColorAndroid: "transparent",
        //   stylesheet: this._getCustomInputStyle(),
        //   autoCorrect: false,
        //   placeholderTextColor: placeholder,
        // },
        // email: {
        //   label: Languages.Email,
        //   placeholder: Languages.TypeEmail,
        //   underlineColorAndroid: "transparent",
        //   stylesheet: this._getCustomInputStyle(),
        //   autoCorrect: false,
        //   placeholderTextColor: placeholder,
        // },
        
        // note: {
        //   label: Languages.Note,
        //   placeholder: Languages.TypeNote,
        //   underlineColorAndroid: "transparent",
        //   multiline: true,
        //   stylesheet: this._getCustomStyle(),
        //   autoCorrect: false,
        //   placeholderTextColor: placeholder,
        // },
      },
    };
  };

  _getCustomInputStyle2 = () => {
     return StyleSheet.create(
        {width: 100},
        {textAlign: "left"}
        );
  }

  renderPhoneInput = (locals) => {
    const {
      theme: {
        colors: { placeholder },
      },
    } = this.props;
    const stylesheet = locals.stylesheet;
    let formGroupStyle = stylesheet.formGroup.normal;
    let controlLabelStyle = stylesheet.controlLabel.normal;
    let textboxStyle = stylesheet.textbox.normal;
    let helpBlockStyle = stylesheet.helpBlock.normal;
    const errorBlockStyle = stylesheet.errorBlock;

    if (locals.hasError) {
      formGroupStyle = stylesheet.formGroup.error;
      controlLabelStyle = stylesheet.controlLabel.error;
      textboxStyle = stylesheet.textbox.error;
      helpBlockStyle = stylesheet.helpBlock.error;
    }

    const label = locals.label ? (
      <Text style={controlLabelStyle}>{locals.label}</Text>
    ) : null;
    
    const help = locals.help ? (
      <Text style={helpBlockStyle}>{Languages.helpPhone}</Text>
    ) : null;
   
    const error =
      locals.hasError && locals.error ? (
        <Text accessibilityLiveRegion="polite" style={[errorBlockStyle,{fontSize:14,
          width:'100%',
          textAlign:'right',
          fontStyle:'italic'}]}>
          {Languages.helpPhone}
        </Text>
      ) : null;

    return (
      <View style={formGroupStyle}>
        {label}
        {/* <Text style={{fontSize:8}}>+964</Text> */}
        <TextInput
          keyboardType={'phone-pad'}
          defaultValue="+964"
          style={textboxStyle}
          onChangeText={(value) => locals.onChange(value)}
          onChange={locals.onChangeNative}
          placeholder={locals.placeholder}
          value={locals.value}
          placeholderTextColor={placeholder}
        />
        {help}
        {error}
      </View>
    );
  };

  renderCountry = (locals) => {
    const {
      theme: {
        colors: { placeholder },
      },
    } = this.props;
    const stylesheet = locals.stylesheet;
    let formGroupStyle = stylesheet.formGroup.normal;
    let controlLabelStyle = stylesheet.controlLabel.normal;
    let textboxStyle = stylesheet.textbox.normal;
    let helpBlockStyle = stylesheet.helpBlock.normal;
    const errorBlockStyle = stylesheet.errorBlock;

    if (locals.hasError) {
      formGroupStyle = stylesheet.formGroup.error;
      controlLabelStyle = stylesheet.controlLabel.error;
      textboxStyle = stylesheet.textbox.error;
      helpBlockStyle = stylesheet.helpBlock.error;
    }

    const label = locals.label ? (
      <Text style={controlLabelStyle}>{locals.label}</Text>
    ) : null;
    const help = locals.help ? (
      <Text style={helpBlockStyle}>{locals.help}</Text>
    ) : null;
    const error =
      locals.hasError && locals.error ? (
        <Text accessibilityLiveRegion="polite" style={errorBlockStyle}>
          {locals.error}
        </Text>
      ) : null;

    return (
      <View style={formGroupStyle}>
        {label}
        <CountryPicker
          onChange={(value) => {
            this.setState({ cca2: value.cca2 });
            locals.onChange(value.name);
          }}
          cca2={this.state.cca2}
          filterable>
          <Text
            style={[
              textboxStyle,
              locals.value === "" && { color: placeholder },
            ]}>
            {locals.value === "" ? locals.placeholder : locals.value}
          </Text>
        </CountryPicker>
        {help}
        {error}
      </View>
    );
  };

  renderCity = (locals) => {
    const {
      theme: {
        colors: { placeholder },
      },
    } = this.props;
    const stylesheet = locals.stylesheet;
    let formGroupStyle = stylesheet.formGroup.normal;
    let controlLabelStyle = stylesheet.controlLabel.normal;
    let textboxStyle = stylesheet.textbox.normal;
    let helpBlockStyle = stylesheet.helpBlock.normal;
    const errorBlockStyle = stylesheet.errorBlock;

    //console.log("locals",locals);
    if (locals.hasError) {
      formGroupStyle = stylesheet.formGroup.error;
      controlLabelStyle = stylesheet.controlLabel.error;
      textboxStyle = stylesheet.textbox.error;
      helpBlockStyle = stylesheet.helpBlock.error;
    }

    const label = locals.label ? (
      <Text style={controlLabelStyle}>{locals.label}</Text>
    ) : null;
    const help = locals.help ? (
      <Text style={helpBlockStyle}>{locals.help}</Text>
    ) : null;
    const error =
      locals.hasError && locals.error ? (
        <Text accessibilityLiveRegion="polite" style={errorBlockStyle}>
          {Languages.helpCity}
        </Text>
      ) : null;
     if(locals.value===""){
      locals.value="";
      locals.onChange("");
     }
     const { getShippingMethod } = this.props;
    return (
      <View style={[formGroupStyle]}>
       {label}
       {Platform.OS==='android'?
       (<View style={textboxStyle}><Pic
          selectedValue={locals.value === "" ? locals.placeholder : locals.value}
          style={[
              textboxStyle,
              locals.value === "" && { color: placeholder },
            ]}
          mode="dropdown"
          onValueChange={(itemValue, itemIndex) => {
            //this.setState({city: itemValue})
            locals.onChange(itemValue)
            if(itemValue === "Baghdad"){
            getShippingMethod(4);
            }else if(itemValue === "Dohuk"){
            getShippingMethod(5);
            }else if(itemValue === "Erbil"){
            getShippingMethod(6);
            }else if(itemValue === "Mosul"){
            getShippingMethod(7);
            }else if(itemValue === "Zakho"){
            getShippingMethod(8);
            }else if(itemValue === "Sulaymaniyah"){
            getShippingMethod(9);
            }
          }
          }>
          <Pic.Item label="Baghdad" value="Baghdad" />
          <Pic.Item label="Dohuk" value="Dohuk" />
          <Pic.Item label="Erbil" value="Erbil" />
          <Pic.Item label="Mosul" value="Mosul" />
          <Pic.Item label="Zakho" value="Zakho" />
          <Pic.Item label="Sulaymaniyah" value="Sulaymaniyah" />
          
        </Pic></View>):(<TouchableOpacity onPress={()=>this.openActionSheet(locals)}><Text style={[
              textboxStyle,
              locals.value === "" && { color: placeholder },
            ]}>{locals.value === "" ? locals.placeholder : locals.value}</Text></TouchableOpacity>)}  
        {help}
        {error}
       
        
        {/* <CountryPicker
          onChange={(value) => {
            this.setState({ cca2: value.cca2 });
            locals.onChange(value.name);
          }}
          cca2={this.state.cca2}
          filterable>
          <Text
            style={[
              textboxStyle,
              locals.value === "" && { color: placeholder },
            ]}>
            {locals.value === "" ? locals.placeholder : locals.value}
          </Text>
        </CountryPicker> */}
        
      </View>
    );
  };

  fetchCustomer = async (props) => {
    const { selectedAddress } = props;
    const { user: customer } = props.user;

    let value = selectedAddress;
    if ((!selectedAddress || Object.keys(selectedAddress).length === 0) && customer) {
      value = {
        first_name:
          customer.billing.first_name === ""
            ? customer.first_name
            : customer.billing.first_name,
        last_name:
          customer.billing.last_name === ""
            ? customer.last_name
            : customer.billing.last_name,
        email:
          customer.billing.email === ""
            ? customer.email
            : customer.billing.email,
        address_1: customer.billing.address_1,
        city: customer.billing.city,
        state: customer.billing.state,
        postcode: customer.billing.postcode,
        country: customer.billing.country,
        phone: customer.billing.phone,
      };
    }

    this.setState({ value });
  };

  validateCustomer = async (customerInfo) => {
    await this.props.validateCustomerInfo(customerInfo);
    if (this.props.type === "INVALIDATE_CUSTOMER_INFO") {
      toast(this.props.message);
      return false;
    }
    this.props.onNext();
  };

  saveUserData = async (userInfo) => {
    this.props.updateSelectedAddress(userInfo);
    try {
      await AsyncStorage.setItem("@userInfo", JSON.stringify(userInfo));
    } catch (error) {

    }
  };

  // selectShippingMethod = (item) => {
  //   console.log("itemmmmm",item);
  //   this.props.selectShippingMethod(item);
  // };

  nextStep = () => {
    const value = this.refs.form.getValue();
    console.log(value);
    if (value) {
      // if(value.city==="Choose city"){
      //   alert(Languages.cityError);
      //   return false;
      // }
      let country = "";
      if (Config.DefaultCountry.hideCountryList) {
        country = Config.DefaultCountry.countryCode.toUpperCase();
      } else {
        // Woocommerce only using cca2 to checkout
        country = this.state.cca2;
      }
      // if validation fails, value will be null
      this.props.onNext({ ...this.state.value, country });

      // save user info for next use
      this.saveUserData({ ...this.state.value, country });
    }
  };

  render() {
    const { shippings, shippingMethod, totalPrice } = this.props;
    const isShippingEmpty = typeof shippingMethod.id === "undefined";
    const {
      theme: {
        colors: { text },
      },
      currency,
    } = this.props;
    //console.log(shippings);
    
    var shippingCost
    if(Object.entries(shippingMethod).length!=0){
        if(shippingMethod.settings.cost){
       shippingCost = shippingMethod.settings.cost.value;
        }else{
          shippingCost = 0;
        }
    }else{
      shippingCost = 0; 
    }

    {totalPrice<200000?this.props.selectShippingMethod(shippings[1]):this.props.selectShippingMethod(shippings[0])}
    return (
      <View style={styles.container}>
        <KeyboardAwareScrollView style={styles.form} enableOnAndroid>
          {Config.shipping.visible && shippings.length > 0 && (
            <View>
             

              {/* <ScrollView contentContainerStyle={styles.shippingMethod}>
                {shippings.map((item, index) => this.props.totalPrice<200000 && (
                 
                  <ShippingMethod
                    item={item}
                    currency={currency}
                    key={`${index}shipping`}
                    onPress={this.selectShippingMethod}
                    selected={
                      (index === 0 && isShippingEmpty) ||
                      item.id === shippingMethod.id
                    }
                  />
                  
                  
                )) }
              </ScrollView> */}
            </View>
          )}

          <View style={css.rowEmpty}>
            <Text style={[css.label, { color: text }]}>
              {Languages.YourDeliveryInfo}
             
            </Text>
            
          </View>

          <View style={styles.formContainer}>
            <Form
              ref="form"
              type={this.Customer}
              options={this.options}
              value={this.state.value}
              onChange={this.onChange}
            />
            <View style={{height:15}}>
                   
            </View>
          </View>
          <View style={css.rowEmpty}>
                <Text style={[css.label, { color: text , fontWeight:'bold' , fontSize :14}]}>
                  {Languages.ShippingType}
                  {this.props.totalPrice<200000?` : IQD ${shippingCost}`:" : IQD 0.00"}
                </Text>
          </View>
          <View style={css.rowEmpty}>
            <Text style={[css.label, { color: text,fontWeight:'normal' ,  fontSize :12}]}>
                    {Languages.ShippingDuration}
                     :  {Languages.ShippingDurationText}
            </Text>
          </View>
        </KeyboardAwareScrollView>

        <Buttons
          isAbsolute
          onPrevious={this.props.onPrevious}
          onNext={this.nextStep}
        />
      </View>
    );
  }
}

Delivery.defaultProps = {
  shippings: [],
  shippingMethod: {},
  selectedAddress: {},
};

const mapStateToProps = ({ carts, user, countries, addresses, currency }) => {
  //console.log(carts.totalPrice);
  return {
    user,
    customerInfo: carts.customerInfo,
    totalPrice:carts.totalPrice,
    message: carts.message,
    type: carts.type,
    isFetching: carts.isFetching,
    shippings: carts.shippings,
    shippingMethod: carts.shippingMethod,
    countries: countries.list,
    selectedAddress: addresses.selectedAddress,
    currency
  };
};

function mergeProps(stateProps, dispatchProps, ownProps) {
 
  const { dispatch } = dispatchProps;
  const CartRedux = require("@redux/CartRedux");
  const AddressRedux = require("@redux/AddressRedux");

  return {
    ...ownProps,
    ...stateProps,
    validateCustomerInfo: (customerInfo) => {
      CartRedux.actions.validateCustomerInfo(dispatch, customerInfo);
    },
    getShippingMethod: (zoneId) => {
      CartRedux.actions.getShippingMethod(dispatch, zoneId);
    },
    selectShippingMethod: (shippingMethod) => {
     
      CartRedux.actions.selectShippingMethod(dispatch, shippingMethod);
    },
    updateSelectedAddress: (address) => {
      AddressRedux.actions.updateSelectedAddress(dispatch, address);
    },
  };
}

export default connect(
  mapStateToProps,
  undefined,
  mergeProps
)(withTheme(Delivery));
