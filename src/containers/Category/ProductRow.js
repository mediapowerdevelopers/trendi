/**
 * Created by InspireUI on 06/03/2017.
 *
 * @format
 */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { View, TouchableOpacity,Image } from "react-native";

import { Styles, Color, withTheme, Tools, Images } from "@common";
import { getProductImage } from "@app/Omni";
import { Rating, ImageCache, Text } from "@components";
import FontAwesome from "@expo/vector-icons/FontAwesome";
import { DisplayMode } from "@redux/CategoryRedux";
import styles from "./styles";

class ProductRow extends PureComponent {
  constructor(props){
    super(props);
    this.state = {
      isInWishList : false,
      loaded:false
    }
  }
  componentDidMount() {
    const { product, wishListItems } = this.props;
    const isInWishList =
      wishListItems.find((item) => item.product.id == product.id) !=
      undefined;
      this.setState({isInWishList})
  }
  render() {
    const { product, onPress, displayMode } = this.props;
    const {isInWishList} = this.state;
    const {
      theme: {
        colors: { background, text },
        dark: isDark,
      },
    } = this.props;

    const isListMode =
      displayMode === DisplayMode.ListMode ||
      displayMode === DisplayMode.CardMode;
    const isCardMode = displayMode === DisplayMode.CardMode;

    const textStyle = isListMode ? styles.text_list : styles.text_grid;
    const imageStyle = isListMode ? styles.image_list : styles.image_grid;
    const image_width = isListMode
      ? Styles.width * 0.9 
      : Styles.width * 0.45 - 2;

    return (
      <TouchableOpacity
        activeOpacity={0.9}
        onPress={onPress}
        style={[
          styles.container_product,
          isListMode ? styles.container_list : styles.container_grid,
          { backgroundColor: background },
        ]}>
         
       
       <Image  onLoadEnd={()=>this.setState({loaded:true})} source={this.state.loaded?{uri:product.images[0].src}:require('../../images/placeholderImage.png')} style={{height:140,width:110,borderRadius:8, elevation:2,
        shadowColor: "rgba(0, 0, 0, 0.5)",
        shadowOpacity: 0.5,
        shadowRadius: 3,
        shadowOffset: {
            height: 2,
            width: 1
        }}} />
        <View style={{ paddingHorizontal: 10 }}>
        {isListMode&&
          (<Text ellipsizeMode="tail"
            style={[textStyle, isCardMode && styles.cardText, { color: text }]}>
            {product.name}
          </Text>)}

          <View
            style={{
              flexDirection: isCardMode ? "column" : "column",
              justifyContent:
                displayMode === DisplayMode.ListMode
                  ? "center"
                  : "center",
              alignItems: isCardMode ? "center" : "center",
              marginTop: 0,
            }}>
            <View
              style={[!isListMode?styles.price_wrapper:styles.price_wrapper2, !isListMode && { marginTop: 0 }]}>
              <Text
                style={[
                  styles.text_list2,
                  styles.price,
                  isCardMode && styles.cardPrice,
                  !isListMode && { color: Color.black },
                  isDark && { color: "rgba(255,255,255,0.8)" },
                ]}>
                {`${Tools.getPriceIncluedTaxAmount(product)} `}
              </Text>
              <View style={{flex:1}}></View>

              {/* <Text
                style={[
                  textStyle,
                  styles.sale_price,
                  isCardMode && styles.cardPriceSale,
                  isDark && { color: "rgba(255,255,255,0.6)" },
                ]}>
                {product.on_sale && product.regular_price > 0
                  ? Tools.getCurrecyFormatted(product.regular_price)
                  : ""}
              </Text> */}

              {/* {product.on_sale && product.regular_price > 0 && (
                <View style={styles.saleWrap}>
                  <Text style={[textStyle, styles.sale_off]}>
                    {`-${(
                      (1 -
                        Number(product.price) / Number(product.regular_price)) *
                      100
                    ).toFixed(0)}%`}
                  </Text>
                </View>
              )} */}
            </View>

            {/* {isListMode && ( */}
              <View style={!isListMode?styles.price_wrapper:styles.price_wrapper2}>
                <Rating
                  rating={product.average_rating>0?Number(product.average_rating):Number(0.1)}
                  size={
                    (isListMode
                      ? Styles.FontSize.medium
                      : Styles.FontSize.tiny) + 3
                  }
                />
                <Image
                source={Images.IconAddToCart}
                style={isListMode?styles.iconStyle2:styles.iconStyle}></Image>
                {/* <Text style={[textStyle, styles.textRating, { color: text }]}>
                  {`(${product.rating_count})`}
                </Text> */}
              </View>
            {/* )} */}
          </View>
        </View>
        {/** ** add wish list *** */}
        {/* <TouchableOpacity
          style={styles.btnWishList}
          onPress={() => {
            !isInWishList
              ? this.setState({isInWishList: !isInWishList}, () => this.props.addToWishList(product))
              : this.setState({isInWishList: !isInWishList}, () => this.props.removeWishListItem(product));
          }}>
          {isInWishList && <FontAwesome name="heart" size={20} color="red" />}
          {!isInWishList && (
            <FontAwesome name="heart-o" size={20} color="#b5b8c1" />
          )}
        </TouchableOpacity> */}
      </TouchableOpacity>
    );
  }
}

ProductRow.propTypes = {
  product: PropTypes.object.isRequired,
  onPress: PropTypes.func,
  displayMode: PropTypes.string,
};

export default withTheme(ProductRow);
