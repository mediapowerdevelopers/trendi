import { StyleSheet,I18nManager } from "react-native";
import { Color, Styles, Constants } from "@common";

const styles = StyleSheet.create({
    //main
    listView: {
      alignItems: "flex-start",
      paddingBottom: Styles.navBarHeight + 10,
    },
    container: {
      flexGrow: 1,
      backgroundColor: Color.background,
    },

    //ProductRows
    container_product: {
        backgroundColor: "white",
        paddingBottom: 10,
        marginHorizontal: Styles.width / 20,
        marginTop: 10,
        
      },
      container_list: {
        width: Styles.width * 0.9,
       // marginLeft: Styles.width * 0.05,
       // marginRight: Styles.width * 0.05,
        marginTop: Styles.width * 0.05,
       // backgroundColor:'red',
        flexDirection:'row',
        alignItems:'center',
        borderBottomWidth: 0.5,
        borderColor: "#CED7DD",
      },
      container_grid: {
        width: (Styles.width * 0.9) / 3,
        marginLeft: (Styles.width * 0.1) / 4.5,
        marginRight: 0,
        marginTop: (Styles.width * 0.1) / 3,
      },
      image: {
        marginBottom: 8,
      },
      image_list: {
        resizeMode:'cover',
        width: Styles.width * 0.31 ,
        height: Styles.width * 0.36 * Styles.thumbnailRatio,
        borderRadius:8,
        //elevation:8,
       //backgroundColor:'yellow'
      },
      image_grid: {
        resizeMode:'stretch',
        width: Styles.width * 0.31 ,
        height: Styles.width * 0.33 * Styles.thumbnailRatio,
        borderRadius:8,
        //backgroundColor:'yellow'
        
      },
      text_list: {
        width: Styles.width * 0.5 ,
        color: Color.black,
        fontSize: Styles.FontSize.medium,
        fontFamily: Constants.fontHeader,
        marginTop:'3%',
        justifyContent:'center',
        alignItems:'center',
        //textAlign:'right'
       // backgroundColor:'red'
      },
      text_list2: {
       // width: Styles.width * 0.44 ,
        color: Color.black,
        fontSize: Styles.FontSize.medium,
        fontFamily: Constants.fontFamily,
        //flexDirection:'row',
        //marginTop:'3%',
        //justifyContent:'flex-start',
        //alignItems:'flex-start',
       // backgroundColor:'blue'
      },
      text_grid: {
        color: Color.black,
        fontSize: Styles.FontSize.small,
        fontFamily: Constants.fontFamily,
      },
      textRating: {
        fontSize: Styles.FontSize.small,
      },
      price_wrapper: {
        //...Styles.Common.Row,
        width: Styles.width * 0.31 ,
        flexDirection:'row',
         marginLeft:10,
        // justifyContent:'flex-start',
        top: 0,
       // backgroundColor:'red'
      },
      price_wrapper2: {
        //...Styles.Common.Row,
       // width: Styles.width * 0.31 ,
        flexDirection:'row',
         //marginLeft:10,
        justifyContent:'center',
        top: 0,
       // backgroundColor:'green'
      },
      cardWraper: {
        flexDirection: "column",
      },
      sale_price: {
        textDecorationLine: "line-through",
        color: Color.blackTextDisable,
        marginLeft: 0,
        marginRight: 0,
        fontSize: Styles.FontSize.small,
      },
      cardPriceSale: {
        fontSize: 15,
        marginTop: 2,
        fontFamily: Constants.fontFamily,
        color: Color.black,
      },
      price: {
        color: "#aeaeae",
        fontSize: Styles.FontSize.small,
      },
      saleWrap: {
        zIndex: 1000,
        position: 'absolute',
        right: 2,
        bottom: 20,
        borderRadius: 5,
        backgroundColor: Color.primary,
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: 3,
        // marginLeft: 5,
      },
      sale_off: {
        color: Color.lightTextPrimary,
        fontSize: Styles.FontSize.small,
      },
      cardText: {
        fontSize: 20,
        textAlign: "center",
      },
      cardPrice: {
        fontSize: 18,
        marginBottom: 8,
        fontFamily: Constants.fontFamily,
      },
      btnWishList: {
        position: "absolute",
        top: 5,
        right: 5,
        backgroundColor: "transparent",
        justifyContent: "center",
        alignItems: "center",
        width: 30,
        height: 30,
      },
      iconStyle:{
        width:15,
        height:15,
        marginRight:8,
        //paddingBottom:15
        //marginLeft:10
      },
      iconStyle2:{
        width:20,
        height:20,
        marginRight:5,
        //marginLeft:'5%'
      },
      navi: {
        flexDirection: "row",
        height: Platform.OS === "ios" ? 44 : 44,
       // borderBottomWidth: 0.5,
        borderColor: "#cccccc",
        paddingLeft: 15,
        paddingTop:1,
        marginTop: 10,
        marginBottom:5,
        marginLeft:10,
        marginRight:10,
        borderRadius:20,
        zIndex:10,
        alignItems:'center',
        //justifyContent: "center",
        //marginHorizontal: 20,
        backgroundColor:'#f8f8f8',
        elevation:2,
        shadowColor: "rgba(0, 0, 0, 0.3)",
        shadowOpacity: 0.5,
        shadowRadius: 3,
        shadowOffset: {
            height: 1,
            width: 1
        },
      },
      btnClose: {
        marginTop: 18,
        height: 46,
        width: 60,
        justifyContent: "center",
        alignItems: "center",
        borderRightWidth: 0.5,
        borderColor: "#cccccc",
      },
      inputSearch: {
        flex: 1,
        textAlign:I18nManager.isRTL ? "left" : "left",
       // justifyContent: "center",
       // alignItems:'center',
        marginTop: 28,
        height: 46,
        marginLeft: 15,
       // marginRight: 8,
        color:'#000',
       // backgroundColor:'red'
        //fontWeight:'bold'
      }
  });

export default styles;