/**
 * Created by InspireUI on 27/02/2017.
 *
 * @format
 */

import React, { PureComponent } from "react";
import {
  View,
  RefreshControl,
  ScrollView,
  Animated,
  FlatList,
  TouchableOpacity,
  TextInput,
  Text,
  I18nManager,
  ActivityIndicator
} from "react-native";
import { connect } from "react-redux";
import { isObject } from "lodash";
import EvilIcons from "@expo/vector-icons/EvilIcons";
import Icon from "react-native-vector-icons/Ionicons";



import { Languages, withTheme, Icons } from "@common";
import { Timer, toast, BlockTimer } from "@app/Omni";
import LogoSpinner from "@components/LogoSpinner";
import Empty from "@components/Empty";
import { DisplayMode } from "@redux/CategoryRedux";
import FilterPicker from "@containers/FilterPicker";
import ProductRow from "./ProductRow";
import ControlBar from "./ControlBar";
import styles from "./styles";
import {NavigationEvents} from 'react-navigation';


class CategoryScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      scrollY: new Animated.Value(0),
      loadingBuffer: true,
      modalVisible: false,
      displayControlBar: true,
      isFirst: 0
    };
    this.pageNumber = 1;

    this.renderList = this.renderList.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.renderScrollComponent = this.renderScrollComponent.bind(this);
    this.onRowClickHandle = this.onRowClickHandle.bind(this);
    this.onEndReached = this.onEndReached.bind(this);
    this.onRefreshHandle = this.onRefreshHandle.bind(this);
    this.onListViewScroll = this.onListViewScroll.bind(this);

    this.openCategoryPicker = () => this.setState({ modalVisible: true });
    this.closeCategoryPicker = () => this.setState({ modalVisible: false });
  }

  componentWillMount() {
    this.props.clearProducts();
  }

  focusedNow(){
    this.setState({ loadingBuffer: true });
    Timer.setTimeout(() => this.setState({ loadingBuffer: false }), 3000);
  }

  componentDidMount() {
    //alert("mounted");
    Timer.setTimeout(() => this.setState({ loadingBuffer: false }), 2000);
    //this.setState({isFirst:1});
    const {
      fetchProductsByCategoryId,
      clearProducts,
      selectedCategory,
    } = this.props;
    clearProducts();
    if (selectedCategory) {
      fetchProductsByCategoryId(selectedCategory.id, this.pageNumber++);
    }
  }

  componentWillReceiveProps(nextProps) {
    const props = this.props;
    const { error } = nextProps.products;
    if (error) toast(error);

    if (props.filters !== nextProps.filters) {
      this.newFilters = this._getFilterId(nextProps.filters);

      this.pageNumber = 1;
      props.clearProducts();
      props.fetchProductsByCategoryId(null, this.pageNumber++, 20, this.newFilters);
    }

    if (props.selectedCategory != nextProps.selectedCategory) {
      this.pageNumber = 1;
      props.clearProducts();
      props.fetchProductsByCategoryId(nextProps.selectedCategory.id, this.pageNumber++);
    }

  }

  renderFooter() {
    const { products } = this.props;
    //alert(products.isFetching);
    return (
      <View style={{padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        width:'100%'}}>
        {(products.isFetching && this.pageNumber>2) ? (
          <ActivityIndicator color="black" style={{ margin: 15 }} />
        ) : null}
      </View>
    );
  }

  renderHeader() {
    const { products } = this.props;
   // alert(this.state.isFirst);
    return (
      <View style={{padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        width:'100%'}}>
        {(products.isFetching) ? (
          <ActivityIndicator color="black" style={{ margin: 15 }} />
        ) : null}
      </View>
    );
  }

  _getFilterId = (filters) => {
    let newFilters = {};
    Object.keys(filters).forEach((key) => {
      const value = filters[key];
      if (value) {
        newFilters = {
          ...newFilters,
          [key]: isObject(value) ? value.id || value.term_id : value,
        };
      }
    });
    // warn(newFilters);
    if (newFilters.price) {
      newFilters.max_price = newFilters.price
      delete newFilters.price
    }
    if (!newFilters.category) {
      newFilters.category = this.props.selectedCategory.id
    }
    return newFilters;
  };

  render() {
    const { modalVisible, loadingBuffer, displayControlBar } = this.state;
    const { products, selectedCategory, filters, fetchProductsByCategoryId } = this.props;
   //console.log(products, 'producastas');
    const {
      theme: {
        colors: { background },
      },
    } = this.props;

    if (!selectedCategory) return null;

    if (products.error) {
      return <Empty text={products.error} />;
    }

    if (this.state.loadingBuffer) {
      return <LogoSpinner fullStretch />;
    }

    const marginControlBar = this.state.scrollY.interpolate({
      inputRange: [-100, 0, 40, 50],
      outputRange: [0, 0, -50, -50],
    });

    const name =
      (filters && filters.category && filters.category.name) ||
      selectedCategory.name;

      if(name!==undefined){
        var t = name.split("|");
        //console.log(t);
        titleEn = t[0];
        titleAr = t[1];
        titleKr = t[2]
      }else{
        titleEn = "";
        titleAr = "";
        titleKr = "";
      }

    return (
      
      <View style={[styles.container, { backgroundColor: background }]}>
      <NavigationEvents onWillFocus={() => this.focusedNow()} />
        <TouchableOpacity onPress={()=>this.props.onSearchPress()} style={styles.navi}>
        <Icon name={Icons.Ionicons.Search} size={20} color="#000" />
          
          <Text
            style={styles.inputSearch}
            
          >{Languages.SearchPlaceHolder}</Text>
        </TouchableOpacity>
        <Animated.View style={{ marginTop: marginControlBar }}>
        
          <ControlBar
            openCategoryPicker={this.openCategoryPicker}
            isVisible={displayControlBar}
            fetchProductsByCategoryId={fetchProductsByCategoryId}
            name={I18nManager.isRTL?(Languages._language==='ar'?titleAr:titleKr):titleEn}
          />
        </Animated.View>
        {this.renderList(products.list)}
        <FilterPicker
          closeModal={this.closeCategoryPicker}
          visible={modalVisible}
        />
      </View>
    );
  }

  renderList = (data) => {
    const { products, displayMode } = this.props;
    const isCardMode = displayMode == DisplayMode.GridMode;
   // const random = isCardMode?3:2;
   // alert(isCardMode);
    return (
      <FlatList
        key={isCardMode}
        keyExtractor={(item, index) => `${item.id}`}
        data={data}
        renderItem={this.renderRow}
        enableEmptySections
        onEndReached={this.onEndReached}
        numColumns={isCardMode?3:1}
        refreshControl={
          <RefreshControl
            refreshing={ products.isFetching}
            onRefresh={this.onRefreshHandle}
          />
        }
        contentContainerStyle={styles.listView}
        initialListSize={6}
        pageSize={2}
        //extraData={isCardMode}
        renderScrollComponent={this.renderScrollComponent}
        ListFooterComponent={this.renderFooter.bind(this)}
        //ListHeaderComponent={this.renderHeader.bind(this)}
      />
    );
  }

  renderRow = (product) => {
    const { displayMode } = this.props;
    const onPress = () => this.onRowClickHandle(product.item);
    const isInWishList =
      this.props.wishListItems.find((item) => item.product.id === product.id) !==
      undefined;

    return (
      <ProductRow
        product={product.item}
        onPress={onPress}
        displayMode={displayMode}
        wishListItems={this.props.wishListItems}
        isInWishList={isInWishList}
        addToWishList={this.addToWishList}
        removeWishListItem={this.removeWishListItem}
      />
    );
  }

  renderScrollComponent = (props) => {
    const { displayMode } = this.props;
    const mergeOnScroll = (event) => {
      props.onScroll(event);
      this.onListViewScroll(event);
    };

    if (displayMode == DisplayMode.CardMode) {
      return (
        <ScrollView
          horizontal
          pagingEnabled
          showsHorizontalScrollIndicator={false}
          props
          {...props}
          onScroll={mergeOnScroll}
        />
      );
    }

    return <ScrollView props {...props} onScroll={mergeOnScroll} />;
  }

  addToWishList = (product) => {
    this.props.addWishListItem(product);
  };

  removeWishListItem = (product) => {
    this.props.removeWishListItem(product);
  };

  onRowClickHandle = (product) => {
    BlockTimer.execute(() => {
      this.props.onViewProductScreen({ product });
    }, 500);
  }

  onEndReached = () => {
    const {
      products,
      fetchProductsByCategoryId,
      selectedCategory,
    } = this.props;
    if (!products.isFetching && products.stillFetch) {
     // alert("here");
      if (this.newFilters) {
        fetchProductsByCategoryId(selectedCategory.id, this.pageNumber++, 20, this.newFilters);
      } else {
        fetchProductsByCategoryId(selectedCategory.id, this.pageNumber++);
      }
    }
  }

  onRefreshHandle = () => {
    const {
      fetchProductsByCategoryId,
      clearProducts,
      selectedCategory,
    } = this.props;
    this.pageNumber = 1;
    clearProducts();
    fetchProductsByCategoryId(selectedCategory.id, this.pageNumber++, 20, this.newFilters);
  }

  onListViewScroll(event: Object) {
    this.state.scrollY.setValue(event.nativeEvent.contentOffset.y);
  }
}

const mapStateToProps = (state) => {
  return {
    selectedCategory: state.categories.selectedCategory,
    netInfo: state.netInfo,
    displayMode: state.categories.displayMode,
    products: state.products,
    wishListItems: state.wishList.wishListItems,
    filters: state.filters,
  };
};

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { netInfo } = stateProps;
  const { dispatch } = dispatchProps;
  const { actions } = require("@redux/ProductRedux");
  const WishListRedux = require("@redux/WishListRedux");
  return {
    ...ownProps,
    ...stateProps,
    fetchProductsByCategoryId: (
      categoryId,
      page,
      per_page = 30,
      filters = {}
    ) => {
      if (!netInfo.isConnected) return toast(Languages.noConnection);
      actions.fetchProductsByCategoryId(
        dispatch,
        categoryId,
        per_page,
        page,
        filters
      );
    },
    clearProducts: () => dispatch(actions.clearProducts()),
    addWishListItem: (product) => {
      WishListRedux.actions.addWishListItem(dispatch, product, null);
    },
    removeWishListItem: (product, variation) => {
      WishListRedux.actions.removeWishListItem(dispatch, product, null);
    },
  };
}

export default connect(
  mapStateToProps,
  undefined,
  mergeProps
)(withTheme(CategoryScreen));
