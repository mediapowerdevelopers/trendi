/** @format */

import { StyleSheet, Dimensions } from "react-native";
var { height, width } = Dimensions.get("window");

export default StyleSheet.create({
  container: {
    width: width,
    backgroundColor: "white",
  },
  name: {
    fontSize: 17,
    margin: 10,
    color: "#000",
    textAlign: 'left'
  },
  review: {
    marginLeft:10,
    marginTop:15,
    marginRight: 10,
    fontSize: 16,
    color: "black",
    textAlign: 'left',
    height:'auto',
   // backgroundColor:'green'
  },
  date_created: {
    marginLeft:10,
   // marginTop:5,
    marginBottom:7,
    fontSize: 15,
    color: "#b2b2b2",
  },
  rating: {
    marginLeft:10,
   // marginTop:5
  },
  separator: {
    height: 0.5,
    backgroundColor: "#CED7DD",
  },
});
