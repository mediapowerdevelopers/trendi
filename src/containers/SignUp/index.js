/**
 * Created by InspireUI on 01/03/2017.
 *
 * @format
 */

import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Switch,
  LayoutAnimation,
  I18nManager,
  Image
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { TextInputMask } from "react-native-masked-text";
//import CountryPicker from "react-native-country-picker-modal";
import { WooWorker } from "api-ecommerce";
import { connect } from "react-redux";

import { Styles, Languages, Color, withTheme } from "@common";
import { toast, error, Validate } from "@app/Omni";
import Button from "@components/Button";
import Spinner from "@components/Spinner";
import WPUserAPI from "@services/WPUserAPI";
import { TouchableOpacity } from "react-native-gesture-handler";
import Constants from "@common/Constants";

class SignUpScreen extends Component {
  constructor(props) {
    super(props);

    let state = {
      username: Math.random().toString(36).substring(7),
      email: "",
      password: "",
      cpassword:"",
      firstName: "",
      lastName: "",
      phone: "",
      country:"",
      cca2:"IQ",
      callingCode:"964",
      useGeneratePass: false,
      isLoading: false,
    };

    const params = props.params;
    if (params && params.user) {
      state = { ...state, ...params.user, useGeneratePass: true };
    }

    this.state = state;

    this.onFirstNameEditHandle = (firstName) => this.setState({ firstName });
    this.onLastNameEditHandle = (lastName) => this.setState({ lastName });
    this.onUsernameEditHandle = (username) => this.setState({ username });
    this.onPhoneEditHandle = (phone) => this.setState({ phone });
    this.onEmailEditHandle = (email) => this.setState({ email });
    this.onPasswordEditHandle = (password) => this.setState({ password });
    this.oncPasswordEditHandle = (cpassword) => this.setState({ cpassword });


    this.onPasswordSwitchHandle = () =>
      this.setState({ useGeneratePass: !this.state.useGeneratePass });

    this.focusLastName = () => this.lastName && this.lastName.focus();
    this.focusUsername = () => this.username && this.username.focus();
    this.focusPhone = () => this.phone && this.phone.focus();
    this.focusEmail = () => this.email && this.email.focus();
    this.focusPassword = () =>
      !this.state.useGeneratePass && this.password && this.password.focus();
    this.focuscPassword = () => this.cpassword && this.cpassword.focus();  
  }

  shouldComponentUpdate() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    return true;
  }


  // onLoginButton = () => {
  //   navigate("Cart");
  // }

  onSignUpHandle = async () => {
    const { login, netInfo } = this.props;
    if (!netInfo.isConnected) return toast(Languages.noConnection);

    const {
      username,
      email,
      firstName,
      lastName,
      password,
      cpassword,
      phone,
      useGeneratePass,
      isLoading,
    } = this.state;
    //const {username} = Math.random().toString(36).substring(7);
    if (isLoading) return;
    this.setState({ isLoading: true });

    const _error = this.validateForm();
    if (_error) return this.stopAndToast(_error);

    const user = {
      username,
      email,
      firstName,
      lastName,
      phone,
      password: useGeneratePass ? undefined : password,
    };
    //console.log(user.username);
    //return false;
    const json = await WPUserAPI.register(user);
    console.log("jsonnnn",json);
    if (json === undefined) {
      return this.stopAndToast("Server don't response correctly");
    } else if (json.code) {
      return this.stopAndToast(json.message);
    }
    const customer = await WooWorker.getCustomerById(json.id);
    if (customer) {
      this.setState({ isLoading: false });
      login(customer, json.cookie);
    } else {
      toast("Can't register user, please try again.");
    }
  };

  validateForm = () => {
    const {
      username,
      email,
      password,
      cpassword,
      firstName,
      lastName,
      phone,
      useGeneratePass,
    } = this.state;
    if (
      Validate.isEmpty(
        phone,
        email,
        firstName,
        lastName,
        useGeneratePass ? "1" : password,
        cpassword
      )
    ) {
      // check empty
      return "Please fill the complete form";
    } else if (!Validate.isEmail(email)) {
      return "Please enter a valid email";
    }else if (password!=cpassword) {
      return "Password and Confirm Password does not match";
    }else if (!(phone.length >= 10 && phone.length <= 16)) {
      return "Phone Number is invalid";
    }
    return undefined;
  };

  stopAndToast = (msg) => {
    toast(msg);
    error(msg);
    this.setState({ isLoading: false });
  };

  render() {
    const {
      username,
      phone,
      email,
      password,
      cpassword,
      firstName,
      lastName,
      useGeneratePass,
      isLoading,
    } = this.state;
    const {
      theme: {
        colors: { background, text, placeholder },
      },
    } = this.props;

    const params = this.props.params;
    return (
      <View style={[styles.container, { backgroundColor: background }]}>
        <KeyboardAwareScrollView
          showsVerticalScrollIndicator={false}
          enableOnAndroid>
          <View style={styles.formContainer}>
            <Text style={[styles.labelTop, { color: text }]}>
              {Languages.profileDetail}
            </Text>
            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.firstName = comp)}
              placeholder={Languages.firstName}
              onChangeText={this.onFirstNameEditHandle}
              onSubmitEditing={this.focusLastName}
              autoCapitalize="words"
              returnKeyType="next"
              value={firstName}
              placeholderTextColor={placeholder}
            />
            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.lastName = comp)}
              placeholder={Languages.lastName}
              onChangeText={this.onLastNameEditHandle}
              onSubmitEditing={this.focusEmail}
              autoCapitalize="words"
              returnKeyType="next"
              value={lastName}
              placeholderTextColor={placeholder}
            />

            <Text style={[styles.label, { color: text }]}>
              {Languages.accountDetails}
            </Text>
            {/* <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.username = comp)}
              placeholder={Languages.username}
              onChangeText={this.onUsernameEditHandle}
              onSubmitEditing={this.focusEmail}
              autoCapitalize="none"
              returnKeyType="next"
              value={username}
              placeholderTextColor={placeholder}
            /> */}
           
            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.email = comp)}
              placeholder={Languages.email}
              onChangeText={this.onEmailEditHandle}
              onSubmitEditing={this.focusPassword}
              keyboardType="email-address"
              returnKeyType={useGeneratePass ? "done" : "next"}
              value={email}
              placeholderTextColor={placeholder}
            />
            {params && params.user ? (
              <View style={styles.switchWrap}>
                <Switch
                  value={useGeneratePass}
                  onValueChange={this.onPasswordSwitchHandle}
                  thumbTintColor={Color.accent}
                  onTintColor={Color.accentLight}
                />
                <Text
                  style={[
                    styles.text,
                    {
                      color: useGeneratePass
                        ? Color.accent
                        : Color.blackTextSecondary,
                    },
                  ]}>
                  {Languages.generatePass}
                </Text>
              </View>
            ) : null}
            {useGeneratePass ? (
              <View />
            ) : (
              <TextInput
                style={styles.input(text)}
                underlineColorAndroid="transparent"
                ref={(comp) => (this.password = comp)}
                placeholder={Languages.password}
                onChangeText={this.onPasswordEditHandle}
                onSubmitEditing={this.focuscPassword}
                secureTextEntry
                returnKeyType="next"
                value={password}
                placeholderTextColor={placeholder}
              />
            )}

            <TextInput
                style={styles.input(text)}
                underlineColorAndroid="transparent"
                ref={(comp) => (this.cpassword = comp)}
                placeholder={Languages.cpassword}
                onChangeText={this.oncPasswordEditHandle}
                onSubmitEditing={this.focusPhone}
                secureTextEntry
                returnKeyType="next"
                value={cpassword}
                placeholderTextColor={placeholder}
              />    

            <View style={styles.phone}>
            <View style={{marginBottom:6}}>
            
            <Image source={require('@images/iraq.png')} style={{width:40,height:25}}  />
            </View>
                  <Text style={{ color: '#000',marginLeft:5,marginRight:5}}>{this.state.callingCode ? `+${this.state.callingCode}`:null}</Text>
                  <View style={{marginLeft: 10, marginRight: 10, width: StyleSheet.hairlineWidth, backgroundColor: '#000', height: 30}} />
                                       

            
            <TextInput
              style={{flex:2,textAlign: I18nManager.isRTL ? "left" : "left"}}
              type={'cel-phone'}
              placeholder="0000-0000"
              keyboardType={'numeric'}
              ref={(comp) => (this.phone = comp)}
              onSubmitEditing={this.onSignUpHandle}
              returnKeyType="done"
              options={{
                maskType: 'INT',
                withDDD: false,
                //dddMask: '(99) '
              }}
              value={phone}
              onChangeText={this.onPhoneEditHandle}
            />
            
            </View>
            <Button
              containerStyle={styles.signUpButton}
              text={Languages.signup2}
              onPress={this.onSignUpHandle}
            />
            <Text style={styles.alreadyText}>{Languages.already}</Text>
            <TouchableOpacity
              style={styles.loginButton}
              //text={Languages.login2}
              onPress={this.props.onLoginButton}
            ><Text style={styles.loginText}>{Languages.login2}</Text></TouchableOpacity>
          </View>
          {isLoading ? <Spinner mode="overlay" /> : null}
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  formContainer: {
    padding: Styles.width * 0.1,
  },
  label: {
    fontWeight: "bold",
    fontSize: Styles.FontSize.medium,
    color: Color.blackTextPrimary,
    marginTop: 20,
  },
  labelTop: {
    fontWeight: "bold",
    fontSize: Styles.FontSize.medium,
    color: Color.blackTextPrimary,
    marginTop: 0,
  },

  input: (text) => ({
    borderBottomWidth: 1,
    borderColor: text,
    height: 40,
    marginTop: 10,
    padding: 0,
    margin: 0,
    // flex: 1,
    textAlign: I18nManager.isRTL ? "right" : "left",
    color: text,
  }),
  phone:{
    flex:1,
    flexDirection:I18nManager.isRTL ?'row-reverse':'row',
    alignItems:'center',
    justifyContent:'center',
   // backgroundColor:'red',
    borderBottomWidth: 1,
    borderColor: "#000",
    height: 40,
    marginTop: 10,
    padding: 0,
    margin: 0,
    // flex: 1,
    textAlign: I18nManager.isRTL ? "left" : "left",
    color: "#000",
  },
  signUpButton: {
    marginTop: 30,
    backgroundColor: Color.primary,
    borderRadius: 5,
    elevation: 1,
  },
  loginButton: {
    marginTop: 10,
    backgroundColor: "#fff",
    borderRadius: 5,
    elevation: 1,
    borderColor:"#000",
    borderWidth:0.5,
    padding:13,
    justifyContent:'flex-start',
    alignItems:'center'
  },
  switchWrap: {
    ...Styles.Common.RowCenterLeft,
    marginTop: 10,
  },
  text: {
    marginLeft: 10,
    color: Color.blackTextSecondary,
  },
  loginText:{
    //marginLeft: 10,
    color: "#000",
    fontFamily:Constants.fontHeader
  },
  alreadyText:{
    color: "#000",
    textAlign:'center',
    marginTop:30,
    fontFamily:Constants.fontFamily
  }
});

const mapStateToProps = (state) => {
  return {
    netInfo: state.netInfo,
  };
};

const mapDispatchToProps = (dispatch) => {
  const { actions } = require("@redux/UserRedux");
  return {
    login: (user, token) => dispatch(actions.login(user, token)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTheme(SignUpScreen));
