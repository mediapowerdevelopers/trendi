/**
 * Created by InspireUI on 20/12/2016.
 *
 * @format
 */

import { Dimensions,I18nManager } from "react-native";


const { width, height } = Dimensions.get("window");

const Constants = {
  RTL: false, // default to set redux. Only use first time
  useReactotron: false,
  
  Language: "en", // ar, en. Default to set redux. Only use first time
  fontFamily: I18nManager.isRTL?"JannaLT-Regular":"OpenSans",
  fontHeader: I18nManager.isRTL?"OpenSans-Bold":"OpenSans-Bold",
  fontHeaderAndroid: I18nManager.isRTL?"JannaLT-Regular":"OpenSans-Bold",
  WordPress: {
    defaultDateFormat: "YYYY-MM-DD HH:mm:ss",
    checkout: "trendi-checkout",
  },
  SplashScreen: {
    Duration: 1000,
  },
  AsyncCode: {
    Intro: "async.intro",
  },
  EmitCode: {
    SideMenuOpen: "OPEN_SIDE_MENU",
    SideMenuClose: "CLOSE_SIDE_MENU",
    SideMenuToggle: "TOGGLE_SIDE_MENU",
    Toast: "toast",
    MenuReload: "menu.reload",
  },
  Dimension: {
    ScreenWidth(percent = 1) {
      return Dimensions.get("window").width * percent;
    },
    ScreenHeight(percent = 1) {
      return Dimensions.get("window").height * percent;
    },
  },
  LimitAddToCart: 10,
  TagIdForProductsInMainCategory: 263,
  Window: {
    width,
    height,
    headerHeight: (65 * height) / 100,
    headerBannerAndroid: (55 * height) / 100,
    profileHeight: (45 * height) / 100,
  },

  PostImage: {
    small: "small",
    medium: "medium",
    medium_large: "medium_large",
    large: "large",
  },
  tagIdBanner: 273, // cat ID for Sticky Products
  stickyPost: true, // default is true (else false)
  PostList: {
    // Custom get All Products in Home Screen
    order: "desc", // or asc - default is "desc" column
    orderby: "date", // date, id, title and slug - default is "date" column
  },
  Layout: {
    card: 'card',
    twoColumn: 'twoColumn',
    simple: 'simple',
    list: 'list',
    advance: 'advance',
    threeColumn: 'threeColumn',
    horizon: 'horizon',
    twoColumnHigh: 'twoColumnHigh',
    BannerLarge: 'bannerLarge',
    Banner: 'banner',
    BannerSale: 'bannerSale',
    Blog: "Blog",

    circleCategory: 'circleCategory',
    mainCategory:'mainCategory',
    BannerHigh: 'bannerHigh',
    BannerSlider: 'bannerSlider',
    BannerImage: 'bannerImage',
  },
  pagingLimit: 10,

  fontText: {
    size: 16,
  },
  productAttributeColor: "color",

  CategoriesLayout: {
    card: 'card',
    sideMenu: 'side-menu',
    column: 'column',
    topMenu: 'top-menu'
  }
};

export default Constants;
