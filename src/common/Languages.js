/** @format */

import LocalizedStrings from "react-native-localization";

export default new LocalizedStrings({
  en: {

    chooseCity:"Choose city",
    cityError:"Please select city",
    SKIP:"SKIP",
    FINISH:"FINISH",
    NEXT:"NEXT",
    showMore:"Show More",
    Exit: "Exit",
    ExitConfirm: "Are you sure you want to exit this app",
    YES: "YES",
    OK: "OK",
    ViewMyOrders: "View My Oders",
    CANCEL: "CANCEL",
    Confirm: "Confirm",

    //home screen categories

    Home:"Home",
    Women:"Women",
    Kids:"Kids",
    Men:"Men",

    HomeTab:"Home",
    CategoriesTab:"Categories",
    SearchTab:"Search",
    CartTab:"Cart",
    ProfileTab:"Profile",
    OrdersTab:"Orders",

    // Scene's Titles
    Home: "Home",
    Intro: "Intro",
    Product: "Product",
    Cart: "Cart",
    WishList: "WishList",

    // Home
    products: "products",

    // TopBar
    ShowFilter: "Sub Categories",
    HideFilter: "Hide",
    Sort: "Sort",
    textFilter: "Recent",

    // Category
    ThereIsNoMore: "There is no more product to show",

    // Product
    AddtoCart: "Add to Cart",
    AddtoWishlist: "Add to Wishlist",
    ProductVariations: "Variations",
    NoVariation: "This product don't have any variation",
    AdditionalInformation: "Description",
    NoProductDescription: "No Product Description",
    ProductReviews: "Reviews",
    NoReview: "This product don't have any reviews ...yet",
    BUYNOW: "BUY NOW",
    ProductLimitWaring: "You can't add more than 10 product",
    EmptyProductAttribute: "This product don't have any attributes",
    ProductFeatures: "Features",
    ErrorMessageRequest: "Can't get data from server",
    NoConnection: "No internet connection",
    ProductRelated: "You May Also Like",

    // Cart
    NoCartItem: "There is no product in cart",
    Total: "Total",
    EmptyCheckout: "Sorry, you can't check out an empty cart",
    RemoveCartItemConfirm: "Remove this product from cart?",
    MyCart: "Cart",
    Order: "Order",
    ShoppingCart: "Shopping Cart",
    ShoppingCartIsEmpty: "Your Cart is Empty",
    Delivery: "Delivery",
    AddProductToCart: "Add a product to the shopping cart",
    TotalPrice: "Total Price:",
    YourDeliveryInfo: "Your delivery info:",
    ShopNow: "Shop Now",
    YourChoice: "Your wishlist:",
    YourSale: "Your Sale:",
    SubtotalPrice: "Subtotal Price:",
    BuyNow: "Buy Now",
    Items: "items",
    Item: "item",
    ThankYou: "Thank you",
    FinishOrderCOD: "You can use to number of order to track shipping status",
    FinishOrder:
      "Thank you for placing the order. A confirmation email has been sent to your email. To check progress of your order please view my orders. You may also receive a phone call shortly to confirm your order.",
    NextStep: "Next Step",
    ConfirmOrder: "Confirm Order",
    RequireEnterAllFileds: "Please enter all fields",
    Error: "Error",
    InvalidEmail: "Invalid email address",
    Finish: "Finish",

    // Wishlist
    NoWishListItem: "There is no item in wishlist",
    MoveAllToCart: "Add all to cart",
    EmptyWishList: "Empty wishlist",
    EmptyAddToCart: "Sorry, the wishlist is empty",
    RemoveWishListItemConfirm: "Remove this product from wishlist?",
    CleanAll: "Clear All",

    // Sidemenu
    SignIn: "Log In",
    SignOut: "Log Out",
    GuestAccount: "Guest Account",
    CantReactEmailError:
      "We can't reach your email address, please try other login method",
    NoEmailError: "Your account don't have valid email address",
    EmailIsNotVerifiedError:
      "Your email address is not verified, we can' trust you",
    Shop: "Shop",
    News: "News",
    Contact: "Contact us",
    Setting: "Setting",
    Login: "Login",
    Logout: "Logout",
    Category: "Category",
    AboutUs:"About Us",
    ShippingPolicy:"Shipping Policy",
    PrivacyPolicy:"Privacy Policy",
    TermsAndConditions:"Terms & Conditions",
    ReturnAndExchange:"Returns",

    // Checkout
    Checkout: "Checkout",
    ProceedPayment: "Proceed Payment",
    Purchase: "Purchase",
    CashOnDelivery: "Pay with cash upon delivery",
    Paypal: "Paypal",
    Stripe: "Stripe",
    CreditCard: "Credit Card",
    PaymentMethod: "Payment Method - Not select",
    PaymentMethodError: "Please select your payment method",
    PayWithCoD: "Your purchase will be pay when goods were delivered",
    PayWithPayPal: "Your purchase will be pay with PayPal",
    PayWithStripe: "Your purchase will be pay with Stripe",
    ApplyCoupon: "Apply",
    CouponPlaceholder: "COUPON CODE",
    APPLY: "APPLY",
    Back: "Back",
    CardNamePlaceholder: "Name written on card",
    BackToHome: "Back to Home",
    OrderCompleted: "Your order was completed",
    OrderCanceled: "Your order was canceled",
    OrderFailed: "Something went wrong...",
    OrderCompletedDesc: "Your order id is ",
    OrderCanceledDesc:
      "You have canceled the order. The transaction has not been completed",
    OrderFailedDesc:
      "We have encountered an error while processing your order. The transaction has not been completed. Please try again",
    OrderTip:
      'Tip: You could track your order status in "My Orders" section from side menu',
    Payment: "Payment",
    Complete: "Complete",
    EnterYourFirstName: "Enter your First Name",
    EnterYourLastName: "Enter your Last Name",
    EnterYourEmail: "Enter your email",
    EnterYourPhone: "Enter your phone",
    EnterYourAddress: "Enter your address",
    CreateOrderError: "Cannot create new order. Please try again later",
    AccountNumner: "Account number",
    CardHolderName: "Cardholder Name",
    ExpirationDate: "Expiration Date",
    SecurityCode: "CVV",

    // myorder
    OrderId: "Order",
    MyOrder: "My Orders",
    NoOrder: "You don't have any orders",
    OrderDate: "Order Date: ",
    OrderStatus: "Status",
    OrderPayment: "Payment method: ",
    OrderTotal: "Total: ",
    OrderDetails: "Show detail",
    ShippingAddress: "Shipping Address:",
    Refund: "Refund",

    PostDetails: "Post Details",
    FeatureArticles: "Feature articles",
    MostViews: "Most views",
    EditorChoice: "Editor choice",

    // settings
    Settings: "Settings",
    BASICSETTINGS: "BASIC SETTINGS",
    Language: "Language",
    INFO: "INFO",
    About: "About us",
    changeRTL: "Switch RTL",

    // language
    AvailableLanguages: "Available Languages",
    SwitchLanguage: "Switch Language",
    SwitchLanguageConfirm: "Switch language require an app reload, continue?",
    SwitchRtlConfirm: "Switch RTL require an app reload, continue?",

    // about us
    AppName: "Trendi",
    AppDescription: "Trendi mCommerce",
    AppContact: " Contact us at: Trendi IQ",
    AppEmail: " Email: support@trendi.iq@gmail.com",
    AppCopyRights: "© Trendi 2019",

    // contact us
    contactus: "Contact us",
    instagram:"Follow us",

    // form
    NotSelected: "Not selected",
    EmptyError: "Address missing",
    DeliveryInfo: "Delivery Info",
    FirstName: "First Name",
    LastName: "Last Name",
    Address: "Address",
    Area: "Area/Address",
    City: "City",
    State: "State",
    NotSelectedError: "Please choose one",
    Postcode: "Postcode",
    Country: "Country",
    Email: "Email",
    Phone: "Phone Number",
    Note: "Note",

    // search
    Search: "Search",
    SearchPlaceHolder: "Search products",
    // SearchPlaceHolder: "Search products by name",
    NoResultError: "Your search keyword did not match any products.",
    Details: "Details",

    // filter panel
    Categories: "Categories",

    // sign up
    profileDetail: "Profile Details",
    firstName: "First name",
    lastName: "Last name",
    accountDetails: "Account Details",
    username: "Username",
    address:"Address",
    email: "Email",
    generatePass: "Use generate password",
    password: "Password",
    cpassword: "Confirm Password",
    signup: "Create Account",
    signup2: "SIGN UP",
    login2:"LOGIN",
    already:"Already have an account?",

    // filter panel
    Loading: "LOADING...",
    welcomeBack: "Welcome  ",
    seeAll: "View All",

    // Layout
    cardView: "Card ",
    simpleView: "List View",
    twoColumnView: "Two Column ",
    threeColumnView: "Three Column ",
    listView: "List View",
    default: "Default",
    advanceView: "Advance ",
    horizontal: "Horizontal ",

    couponCodeIsExpired: "This coupon code is expired",
    alreadyAvailed:"Sorry, you have already availed this coupan",
    invalidCouponCode: "This coupon code is invalid",
    remove: "Remove",
    reload: "Reload",
    applyCouponSuccess: "Congratulations! Coupon code applied successfully ",

    OutOfStock: "OUT OF STOCK",
    ShippingType: "Shipping charges",
    ShippingDuration:"Shipping duration",
    ShippingDurationText:"Expected delivery within 6-14 business days (Sunday – Thursday) depending on availability of stock",

    // Place holder
    TypeFirstName: "Type your first name",
    TypeLastName: "Type your last name",
    TypeAddress: "Type address",
    TypeCity: "Select your city",
    TypeState: "Type your state",
    TypeNotSelectedError: "Please choose one",
    TypePostcode: "Type postcode",
    TypeEmail: "Type email (Ex. acb@gmail.com), ",
    TypePhone: "Type phone number",
    TypeNote: "Note",
    TypeCountry: "Select country",
    SelectPayment: "Select Payment method",
    close: "CLOSE",
    noConnection: "NO INTERNET ACCESS",
    helpPhone:"Phone number missing",
    helpCity:"City missing",

    // user profile screen
    AccountInformations: "Account Informations",
    PushNotification: "Push notification",
    DarkTheme: "Dark Theme",
    Privacy: "Privacy policies",
    SelectCurrency: "Select currency",
    Name: "Name",
    Currency: "Currency",
    Languages: "Languages",

    GetDataError: "Can't get data from server",
    UserOrEmail: "Email Address",
    Or: "Or",
    FacebookLogin: "Facebook Login",
    forgetPassword:"Forgot Password?",
    DontHaveAccount: "Don't have an account?",

    // Horizontal
    flashSale: "Flash Sale",
    bagsCollections: "Bags Collections",
    womanBestSeller: "Woman Best Seller",
    mostPopular: "Most Popular",

    // Modal
    Select: "Select",
    Cancel: "Cancel",
    Guest: "Guest",
    changePassword:"Change Password",

    LanguageName: "English",

    // review
    vendorTitle: "Vendor",
    comment: "Leave a review",
    yourcomment: "Your comment",
    placeComment:
      "Tell something about your experience or leave a tip for others",
    writeReview: "Add Review",
    thanksForReview:
      "Thanks for the review, your content will be verify by the admin and will be published later",
    errInputComment: "Please input your content to submit",
    errRatingComment: "Please rate to submit",
    send: "Send",

    termCondition: "Term & Condition",
    Subtotal: "Subtotal",
    Discount: "Total Discount Availed",
    Shipping: "Shipping Charges",
    Recents: "Recents",
    Filters: "Filters",
    Pricing: "Price Range (IQD)",
    Filter: "Filter",
    ClearFilter: "Clear Filter",
    ProductCatalog: "Product Categories",
    ProductTags: "Product Tags",
    AddToAddress: "Add to Address",
    SMSLogin: "SMS Login",
    OrderNotes: 'Order Notes',

    selectSize:'Please select size to proceed!',
    selectColor:'Please select color to proceed!',

    welcome: "WELCOME",
    discover: "Discover the best trendiest clothes and accessories for Men, Women & Kids.",
    whyUs: "WHY US?",
    freeDelivery: "Cash on Delivery",
    continue: "CONTINUE",
    browse: "Browse designer clothes & accessories"
  },
  ar: {

    chooseCity:"اختر المدينة",
    cityError:"الرجاء اختيار المدينة",
    SKIP:"تخطى",
    FINISH:"إنهاء",
    NEXT:"التالى",

    showMore:"أظهر المزيد",
    welcome: "اهلاً بك",
    discover: "اكتشف افخر انواع الملابس والإكسسوارات للرجال والنساء والأطفال",
    whyUs: "لماذا نحن؟",
    freeDelivery: "تصفح صيحات الموضة الجديدة مع خدمة الدفع عند الوصول",
    continue: "اكمل",
    browse: "حدد نوعية المنتجات التي ترغب بشرائها",

    Exit: "خروج",
    ExitConfirm:"هل أنت متأكد من رغبتك في الخروج من التطبيق",
    YES: "نعم",
    OK: "حسنًا",
    ViewMyOrders: "الاطلاع على الطلبات الخاصة بي",
    CANCEL: "إلغاء",
    Confirm: "تأكيد",

    //home screen categories

    Home:"الصفحة الرئيسية",
    Women:"النساء",
    Kids:"الأطفال‎",
    Men:"الرجال",

    HomeTab:"الرئيسية",
    CategoriesTab:"الفئات",
    SearchTab:"البحث",
    CartTab:"حقيبة التسوق",
    ProfileTab:"صفحتي‎",
    OrdersTab:"قائمة طلباتي",

    // Scene's Titles
    Home: "الرئيسة",
    Intro: "مقدمة",
    Product: "منتج",
    Cart: "العربة",
    WishList: "القائمة المفضلة",

    // Home
    products: "المنتجات",

    // TopBar
    ShowFilter: "الفئات الفرعية",
    HideFilter: "إخفاء",
    Sort: "فرز",
    textFilter: "الحالي",

    // Category
    ThereIsNoMore: "ليس هناك أي منتجات حالية يمكن عرضها",

    // Product
    AddtoCart: "إضافة للعربة",
    AddtoWishlist: "إضافة للقائمة المفضلة",
    ProductVariations: "الاختلافات",
    NoVariation: "لا يحتوي هذا المنتج على أي اختلافات",
    AdditionalInformation: "البيان",
    NoProductDescription: "لا يحتوي  المنتج على أي بيان",
    ProductReviews: "استعراض",
    NoReview: "لا يحتوي هذا المنتج على أي عمليات استعراض حتى الآن",
    BUYNOW: "اشتر الآن",
    ProductLimitWaring: "لا يمكنك إضافة أكثر من 10 منتجات",
    EmptyProductAttribute: "لا يحتوي هذا المنتج على أي مواصفات",
    ProductFeatures: "الخصائص",
    ErrorMessageRequest: "لا يمكن الحصول على البيانات من الخادم",
    NoConnection: "لا يوجد أي اتصال بالإنترنت",
    ProductRelated: "يمكنك الإعجاب أيضًا بما يلي",
    // Cart
    NoCartItem: "لا يوجد منتجات في العربة",
    Total: "الإجمالي",
    EmptyCheckout: "عفوًا، لا يمكنك تفقد العربة الفارغة",
    RemoveCartItemConfirm: "هل ترغب في إزالة هذا المنتج من العربة؟",
    MyCart: "عربة التسوق",
    Order: "طلب",
    ShoppingCart: "عربة التسوق",
    ShoppingCartIsEmpty: "عربة التسوق فارغة",
    Delivery: "التسليم",
    AddProductToCart: "إضافة منتج إلى عربة التسوق",
    TotalPrice: "السعر الإجمالي",
    YourDeliveryInfo: "معلومات التوصيل",
    ShopNow: "تسوق الآن",
    YourChoice: "اختيارك:",
    YourSale: "مبيعاتك:",
    SubtotalPrice: "إجمالي السعر الفرعي:",
    BuyNow: "اشتر الآن",
    Items: "البنود",
    Item: "البند",
    ThankYou:"شكرًا",
    FinishOrderCOD: "يمكنك استخدام ترقيم الطلبات لتتبع حالة الشحن",
    FinishOrder:
      "شكراً لتسوقك مع تريندي. لقد تم استلام طلبك وسيتم شحنه قريبا. سوف تتلقى بريد إلكتروني لإثبات الطلب. يمكن ان يأتيك مكالمة صوتيه أيضًا لإثبات الطلب",
    NextStep: "الخطوة التالية",
    ConfirmOrder: "أكد الطلب",
    RequireEnterAllFileds: "يرجى ملء جميع الخانات ",
    Error: "خطأ",
    InvalidEmail: "عنوان بريد إلكتروني غير صالح",
    Finish: "إنهاء",

    // Wishlist
    NoWishListItem: "لا يوجد أي بنود بالقائمة المفضلة",
    MoveAllToCart: "إضافة الجميع للعربة",
    EmptyWishList: "القائمة المفضلة فارغة",
    EmptyAddToCart: "عذرا، قائمة الرغبات فارغة",
    RemoveWishListItemConfirm: "هل ترغب في إزالة هذا المنتج من القائمة المفضلة؟",
    CleanAll: "إزالة الجميع",

    // Sidemenu
    SignIn: "الدخول",
    SignOut: "الخروج",
    GuestAccount: "حساب العميل",
    CantReactEmailError: "لم نتمكن من الوصول لعنوان بريدك الإلكتروني، يرجى محاولة استخدام أي وسيلة دخول أخرى",
    NoEmailError: "لا يحتوي حسابك على عنوان بريد إلكتروني صالح",
    EmailIsNotVerifiedError:
      "لم يجرى التحقق من عنوان بريدك الإلكتروني لذا لا يمكننا الوثوق بك",
    Shop: "تسوق",
    News: "الأخبار",
    Contact: "اتصل بنا",
    Setting: "الإعدادات",
    Login: "الدخول",
    Logout: "الخروج",
    Category: "الفئات",
    AboutUs:"معلومات عنا",
    ShippingPolicy:"سياسة الشحن",
    PrivacyPolicy:"سياسة خاصة",
    TermsAndConditions:"البنود و الظروف",
    ReturnAndExchange:"العودة و الصرف",

    // Checkout
    Checkout: "تفقد",
    ProceedPayment: "استكمال عملية الدفع",
    Purchase: "شراء",
    CashOnDelivery:"  عندالأستلام كاش الدفع",
    Paypal: "التحويل عبر باي بال",
    Stripe: "سترايب",
    CreditCard: "بطاقات الائتمان",
    PaymentMethod: "طريقة الدفع - عدم اختيار",
    PaymentMethodError: "يرجى اختيار طريقة الدفع",
    PayWithCoD: "الدفع عند استلام البضائع",
    PayWithPayPal: "الدفع بواسطة Paypal",
    PayWithStripe: "سيتم دفع الشراء مع Stripe",
    ApplyCoupon: "تقديم",
    CouponPlaceholder: "رمز القسيمة",
    APPLY: "تقديم",
    Back: "عودة",
    CardNamePlaceholder: "الاسم مكتوب على البطاقة",
    BackToHome: "العودة لرئيسة",
    OrderCompleted: "اكتمال الطلب",
    OrderCanceled: "إلغاء الطلب",
    OrderFailed: "حدوث خطأ",
    OrderCompletedDesc: "هوية الطلب هي",
    OrderCanceledDesc: "قمت بإلغاء الطلب. لم يجرى استكمال المعاملة",
    OrderFailedDesc: "حدوث خطأ أثناء معالجة الطلب، ولم تكتمل المعاملة لذا يرجى المحاولة مرة أخرى",
    OrderTip:
        "يمكنك تتبع حالة طلبك بقسم الطلبات الموجود بالقائمة الجانبية",
    Payment: "الدفع",
    Complete: "اكمال...",
    EnterYourFirstName: "أدخل اسمك الأول",
    EnterYourLastName: "أدخل اسم الأخير",
    EnterYourEmail: "أدخل البريد الإلكتروني",
    EnterYourPhone: "رقم الهاتف",
    EnterYourAddress: "إدخال العنوان",
    CreateOrderError: "لا يمكن إنشاء طلب جديد. يرجى المحاولة مرة أخرى",
    AccountNumner: "رقم حساب",
    CardHolderName: "إسم صاحب البطاقة",
    ExpirationDate: "تاريخ الانتهاء",
    SecurityCode: "رمز التحقق من البطاقة",

    // myorder
    OrderId: "رقم تعريف الطلب",
    MyOrder: "الطلبات الخاصة بي",
    NoOrder: "ليس لديك أي طلبات ",
    OrderDate: "تاريخ الطلب",
    OrderStatus: "الحالة",
    OrderPayment: "طريقة الدفع",
    OrderTotal: "الإجمالي",
    OrderDetails: "بيان التفاصيل",
    ShippingAddress: "عنوان الشحن:",
    Refund: "استرداد الأموال",

    PostDetails: "بيانات البريد",
    FeatureArticles: "المقالات الرئيسة",
    MostViews: "الأكثر مشاهدة",
    EditorChoice: "اختيار المحرر",

    // settings
    Settings: "الإعدادات",
    BASICSETTINGS: "الإعدادات الأساسية",
    Language: "اللغة",
    INFO: "معلومات",
    About: "من نحن",
    changeRTL: "التبديل من اليسار إلى اليمين",

    // language
    AvailableLanguages: "اللغات المتوفرة",
    SwitchLanguage: "تبديل اللغة",
    SwitchLanguageConfirm:
      "تغيير اللغة يتطلب الاستمرار في إعادة تحميل التطبيق؟",
    SwitchRtlConfirm:
      "التغيير من اليسار إلى اليمين يتطلب الاستمرار في إعادة تحميل التطبيق؟",

    // about us
    AppName: "المتاجر الخلوية",
    AppDescription: "الرد على النموذج الأصلي لأغراض التجارة الخلوية",
    AppContact: " الاتصال بنا على: trendi.io",
    AppEmail: " البريد الإلكتروني: support@trendi.io",
    AppCopyRights: "© Trendi 2019",

    // contact us
    contactus: "اتصل بنا",
    instagram:" تابعنا ",

    // form
    NotSelected: "لم يجرى اختياره",
    EmptyError: "هذه الخانة فارغة",
    DeliveryInfo: "بيانات التسليم",
    FirstName: "الاسم الاول",
    LastName: "الاسم الأخير ",
    Address: "عنوان",
    Area: "المنطقة",
    City: "المدينة",
    State: "الدولة",
    NotSelectedError: "يرجى اختيار واحدة",
    Postcode: "الرمز البريدي",
    Country: "الدولة",
    Email: "البريد الإلكتروني",
    Phone: "رقم المحمول",
    Note: "ملاحظات",

    // search
    Search: "بحث",
    SearchPlaceHolder: "البحث عن المنتج بالاسم",
    NoResultError: "كلمات البحث لا تطابق أي من المنتجات",
    Details: "التفاصيل",

    // filter panel
    Categories: "الفئات",

    // sign up
    profileDetail: "بيانات الملف",
    firstName: "الاسم الاول",
    lastName: "الاسم الأخير",
    accountDetails: "بيانات الحساب",
    username: "اسم المستخدم",
    address:"عنوان",
    email: "البريد الإلكتروني",
    generatePass: "استخدام الرقم السري الموجود",
    password: "الرقم السري",
    cpassword:"تأكيد كلمة المرور",
    signup: "تسجيل الدخول",
    signup2: "تسجيل الدخول",
    login2:"تسجيل الدخول",
    already:"هل لديك حساب",


    // filter panel
    Loading: "تحميل ...",
    welcomeBack: "  مرحبًا بك",
    seeAll: "إظهار الجميع",

    // Layout
    cardView: "بطاقة",
    simpleView: "قائمة مبسطة",
    twoColumnView: "عمودين",
    threeColumnView: "ثلاثة أعمدة",
    listView: "قائمة عمودية",
    default: "الافتراضي",
    advanceView: "متقدم",
    horizontal: "أفقي ",

    couponCodeIsExpired: "رمز القسيمة منتهي",
    alreadyAvailed:"coupan استفادت بالفعل",
    invalidCouponCode: "رمز القسيمة غير صالح",
    remove: "إزالة",
    applyCouponSuccess: "تهانيًا! رمز القسيمة مستخدم بنجاح",
    reload: " إعادة تحميل",

    OutOfStock: "نفاذ الرصيد",
    ShippingType: "الشحن",
    ShippingDuration:"مدة الشحن",
    ShippingDurationText:"التسليم المتوقع خلال 6-14 أيام عمل (الأحد - الخميس) حسب توفر المخزون",

    // Place holder
    TypeFirstName: "كتابة الاسم الأول",
    TypeLastName: "كتابة الاسم الأخير",
    TypeAddress: "كتابة العنوان",
    TypeCity: "كتابة المدينة/ المنطقة",
    TypeState: "كتابة الولاية أو المدينة",
    TypeNotSelectedError: "يرجى اختيار واحد",
    TypePostcode: "كتابة الرمز البريدي",
    TypeEmail: "كتابة البريد الإلكتروني (Ex acb@gmailcom)  ",
    TypePhone: "كتابة رقم الهاتف المحمول",
    TypeNote: "ملاحظات",
    TypeCountry: "اختيار الدولة",
    SelectPayment: "اختيار طريقة الدفع",
    close: "إغلاق",
    noConnection: "غير متصل بالإنترنت",
    helpPhone:"رقم الهاتف مفقود",
    helpCity:"المدينة في عداد المفقودين",


    // user profile screen
    AccountInformations: "معلومات الحساب",
    PushNotification: "إخطارات بالأحداث الجديدة",
    DarkTheme: "موضوع الظلام",
    Privacy: "سياسة الخصوصية",
    SelectCurrency: "اختيار العملة",
    Name: "الاسم",
    Currency: "العملة",
    Languages: "اللغات",

    GetDataError: "لا يمكن الحصول على أي بيانات من الخادم",
    UserOrEmail: "اسم المستخدم أو البريد الإلكتروني",
    Or: "أو",
    FacebookLogin: "الدخول باستخدام حساب فيسبوك",
    DontHaveAccount: "ليس لديك حساب",
    forgetPassword:"نسيت كلمة المرور",


    // Horizontal
    flashSale: "تخفيضات",
    bagsCollections: "تشكيلة الحقائب",
    womanBestSeller: "افضل مبيعات منتجات المرأة",
    mostPopular: "الأكثر شعبية",

    // Modal
    Select: "اختيار",
    Cancel: "إلغاء",
    Guest: "عميل",
    changePassword:"تغيير كلمة المرور",

    LanguageName: "اللغة الإنجليزية ",

    // review
    vendorTitle: "البائع",
    comment: "ترك تقييم",
    yourcomment: "تعليقك",
    placeComment: "اخبرنا عن تجربتك أو اترك نصيحة للآخرين",
    writeReview: "تقييم",
    thanksForReview:
      "شكرًا على التقييم، جاري التحقق من المحتوى من قبل الإدارة وسيتم نشره في وقت لاحق",
    errInputComment: "يرجى إدخال المحتوى لتقديمه",
    errRatingComment: "الرجاء إعداد التقييم ليتم تقديمه",
    send: "إرسال",

    termCondition: "الشروط والأحكام",
    Subtotal: "المجموع",
    Discount: "خصم",
    Shipping: "الشحن",
    Recents: "آخر المستجدات",
    Filters: "مرشحات",
    Pricing: "التسعير",
    Filter: "فرز",
    ClearFilter: "إزالة الفرز",
    ProductCatalog: "دليل المنتجات",
    ProductTags: "علامات المنتجات",
    AddToAddress: "إضافة للعنوان",
    SMSLogin: "تسجيل الدخول باستخدام رسالة نصية",
    OrderNotes: "ملاحظات حول الطلب",

    selectSize:"يرجى اختيار حجم للمتابعة",
    selectColor:'يرجى اختيار اللون للمتابعة'
  },
  kr: {

    chooseCity:"شار هەڵ ببژێرە",
    cityError:"تكايە شار هەڵ ببژێرە",
    SKIP:"بازدان",
    FINISH:"تەواو دەبێت",
    NEXT:"داهاتوو",

    showMore:"زیاتر",
    welcome: "بەخێربێ",
    discover: "گونجاوترین و راقیترین جل و ئێکسسواراتی پیاو و ژن و منداڵ بەدەست بێنە",
    whyUs: "بۆچی ئێمە؟",
    freeDelivery: "هەزارەها ستایلی جیاواز ببینە لەگەڵ خزمەتگوزاری پارەدان لە کاتی گەیشتن",
    continue: "بەردەوامبە",
    browse: "جۆرەکانی کاڵا بەپێی حەزەکانت هەڵبژێرە",

    Exit: "چوونه‌ ده‌ره‌وه‌",
    ExitConfirm:"دڵنیایت كه‌ ده‌ته‌وێت له‌ ئه‌‌پلیكه‌یشنه‌كه بچیته‌ ده‌ره‌وه‌؟‌",
    YES: "به‌ڵێ",
    OK: "باشه‌",
    ViewMyOrders: "داواكردنه‌كانم پیشان بده",
    CANCEL: "پووچه‌ڵ بكه‌ره‌وه‌",
    Confirm: "دڵنیا بكه‌ره‌وه‌",  

    //home screen categories

    Home:"سەرەکی",
    Women:"ژنان",
    Kids:"منداڵان",
    Men:"پیاوان",

    HomeTab:"سەرەکی",
    CategoriesTab:"ليست",
    SearchTab:"هەڵبژيرە",
    CartTab:"سەلک من   ",
    ProfileTab:"پروفايل",
    OrdersTab:"داواکاريەکانم",

    // Scene's Titles
    Home: "سەرەکی",
    Intro: "پێشه‌كی",
    Product: "به‌رهه‌م",
    Cart: "عەرەبانە",
    WishList: "ليستە ئارەزوو بكە",

    // Home
    products: "به‌رهه‌مه‌كان",

    // TopBar
    ShowFilter: "به‌شه‌ لاوه‌كییه‌كان",
    HideFilter: "بشاره‌وه‌",
    Sort: "ریز بكه‌",
    textFilter: "به‌رهه‌می نوێ",

    // Category
    ThereIsNoMore:"ناتوانین به‌رهه‌می زیاتر پیشان بده‌ین",

    // Product
    AddtoCart: "بۆ ناو سه‌به‌ته‌",
    AddtoWishlist: "بۆ لیستی خواسته‌كان",
    ProductVariations: "جۆره‌كان",
    NoVariation: " ئه‌م به‌رهه‌مه‌ جۆری دیكه‌ی نیه‌",
    AdditionalInformation: "پێناسه‌",
    NoProductDescription: "ئه‌م به‌رهه‌مه‌ پێناسه‌ی نیه‌",
    ProductReviews: "پێداچوونەوە",
    NoReview: "ئه‌م به‌رهه‌مه‌ بۆچوونی له‌سه‌ر نیه‌",
    BUYNOW: "ئێستا بیكڕه‌",
    ProductLimitWaring: "ناتوانیت زیاتر له‌ ١٠ به‌رهه‌م هه‌ڵبژێریت",
    EmptyProductAttribute: "ئه‌م به‌ر‌هه‌مه‌ تایبه‌تمه‌ندی نیه‌",
    ProductFeatures: "تایبه‌تمه‌ندییه‌كان",
    ErrorMessageRequest: "ناتوانین زانیارییه‌كان له‌ سێرڤه‌ر وه‌ربگرین",
    NoConnection: "ئینته‌رنێت نیه‌",
    ProductRelated: "ره‌نگه‌ ئه‌م به‌رهه‌مانه‌ت به‌ دڵ بێت",
    // Cart
    NoCartItem: "هیچ به‌رهه‌مێك له‌ سه‌به‌ته‌كه‌دا نیه‌",
    Total: "كۆ",
    EmptyCheckout: "ببوره‌! ناتوانیت سه‌به‌ته‌ی به‌تاڵ بكڕیت",
    RemoveCartItemConfirm: "ده‌ته‌وێت به‌رهه‌مێك له‌ سه‌به‌ته‌كه‌ بسڕیته‌وه‌؟ ",
    MyCart: "عەرەبانەى من",
    Order: "فەرمان",
    ShoppingCart: "سه‌به‌ته‌ی بازاڕكردن",
    ShoppingCartIsEmpty: "سه‌به‌ته‌كه‌ت به‌تاڵه‌",
    Delivery: "گه‌یاندن",
    AddProductToCart: "به‌رهه‌م بۆ سه‌به‌ته‌كه‌ زیاد بكه‌",
    TotalPrice:  "كۆی نرخ",
    YourDeliveryInfo: " زانیارییه‌كانی گه‌یاندن",
    ShopNow: "ئێستا بازاڕی بكه‌",
    YourChoice: "هه‌ڵبژارده‌كه‌ت",
    YourSale: "كڕدراوه‌كانت",
    SubtotalPrice: "نرخی به‌رهه‌مه‌كانی",
    BuyNow: "ئێستا بیكڕه‌",
    Items: "كاڵاكان",
    Item: "كاڵا",
    ThankYou:"سوپاس",
    FinishOrderCOD: "ده‌توانیت له‌ ڕێگای ژماره‌ی داواكردنه‌كانت ڕه‌وشی بازاڕكرد‌نه‌كه‌ت بزانیت",
    FinishOrder:
      "سوپاس بۆ ناردنی داواکارییەکەت. ئیمێڵێکی دڵنیابوونەوە بۆ تۆ نێردرا، بۆ بینین و زانینی زانیاری زیاتر لەسەر داواکارییەکانت، سەیری بەشی 'داوکارییەکانم' بکە. لەوانەیە لە ماوەیەکی کەمدا پەیوەندیت پێوەبکرێت بۆ دڵنیابوونەوە لە داواکارییەکەت",
    NextStep: "هه‌نگاوی دواتر",
    ConfirmOrder: "داواكردن دڵنیا بكه‌ره‌وه‌",
    RequireEnterAllFileds: "تكایه‌ سه‌رجه‌م خانه‌كان پڕ بكه‌ره‌وه‌",
    Error: "هه‌ڵه‌ت كرد!",
    InvalidEmail: "ناونیشانی ئیمێڵه‌كه‌ت هه‌ڵه‌یه‌",
    Finish: "كۆتایی پێ بهێنه‌",

    // Wishlist
    NoWishListItem: "هیچ كاڵایه‌ك له‌ لیستی خواسته‌كاندا نیه‌",
    MoveAllToCart: "هه‌موویان بخه‌ره‌ ناو سه‌به‌ته‌كه‌",
    EmptyWishList: "لیستی خواسته‌كان به‌تاڵه‌",
    EmptyAddToCart: "ببوره‌! لیستی خواسته‌كان به‌تاڵه‌",
    RemoveWishListItemConfirm: "ده‌ته‌وێت ئه‌م به‌رهه‌مه‌ له‌ لیستی خواسته‌كانت بسڕیته‌وه‌؟",
    CleanAll: "هه‌موویان بسڕه‌وه‌",

    // Sidemenu
    SignIn: "چوونه‌ ژووره‌وه‌",
    SignOut: "چوونه‌ ده‌ره‌وه‌",
    GuestAccount: "چوونه‌ ژووره‌وه‌ وه‌كوو میوان",
    CantReactEmailError: "ناتوانین په‌یوه‌ندی به‌ ئیمێڵه‌‌كه‌ته‌وه‌ بكه‌ین. تكایه‌ رێگایه‌كی دیكه‌ هه‌ڵبژێره بۆ چوونه‌ ژووره‌وه‌‌",
    NoEmailError: "هه‌ژماره‌كه‌ت ئیمێڵێكی دروستی نیه‌",
    EmailIsNotVerifiedError:
      "ئیمێڵه‌كه‌ت په‌سه‌ند نه‌كراوه‌ ناتوانین متمانه‌ت پێ بكه‌ین",
    Shop: "فرۆشگا",
    News: "هه‌واڵ",
    Contact: "په‌یوه‌ندیمان پێوه‌ بكه‌",
    Setting: "دانان",
    Login: "چوونه‌ ژووره‌وه‌",
    Logout: "چوونه‌ ده‌ره‌وه‌",
    Category: "به‌شه‌كان",
    AboutUs:"دەربارەی ئيمە",
    ShippingPolicy:"مەرجەکانی گەهاندن",
    PrivacyPolicy:"مه رجه كاني تايبەتمەنديێ",
    TermsAndConditions:"مەرج و رەوتشتەکان",
    ReturnAndExchange:"گه راندن يان گهورين",

    // Checkout
    Checkout: "كاشێر",
    ProceedPayment: "پاره‌ بده‌",
    Purchase: "بكڕه‌",
    CashOnDelivery:"له‌ كاتی گه‌یشتن پاره‌ ده‌ده‌م",
    Paypal: "په‌ی پاڵ",
    Stripe: "سترایپ",
    CreditCard: "كرێدت كارت",
    PaymentMethod: "رێگه‌ی پاره‌دانت هه‌ڵنه‌بژاردووه‌",
    PaymentMethodError: "تكایه‌ رێگه‌ی پاره‌دان هه‌ڵبژێره‌",
    PayWithCoD: "له‌ كاتی گه‌یشتنی كاڵاكه‌ پاره‌ ده‌ده‌یت",
    PayWithPayPal: "له‌ رێگه‌ی په‌ی پاڵ پاره‌ ده‌ده‌یت",
    PayWithStripe: "له‌ رێگه‌ی سترایپ پاره‌ ده‌ده‌یت",
    ApplyCoupon: " جێبه‌جێ بكه‌",
    CouponPlaceholder: "كۆدی كوپۆن",
    APPLY: " جێبه‌جێ بكه‌",
    Back: "گه‌ڕانه‌وه‌",
    CardNamePlaceholder: "ناوی سه‌ر كارته‌كه‌ چیه‌؟",
    BackToHome: "گه‌ڕانه‌وه‌ بۆ لاپه‌ڕه‌ی سه‌ره‌كی",
    OrderCompleted: "داواكردنه‌كه‌ت سه‌ركه‌وتوو بوو",
    OrderCanceled: "داواكردنه‌كه‌ت پوچه‌ڵ كرا",
    OrderFailed: "هه‌ڵه‌یه‌ك ڕوویدا",
    OrderCompletedDesc: "ناسنامه‌ی داواكردنه‌كه‌ت",
    OrderCanceledDesc: "داواكردنه‌كه‌ت پوچه‌ڵ كرده‌وه‌. ئاڵوگۆڕه‌كه‌ جێبه‌جێ نه‌بوو",
    OrderFailedDesc: "هه‌ڵه‌یه‌ك روویدا. ئاڵوگۆڕه‌كه‌ جێبه‌جێ نه‌بوو تكایه‌ دووباره‌ هه‌وڵ بده‌",
    OrderTip:
        "ده‌توانیت ڕه‌وشی داواكردنه‌كه‌ت له‌ به‌شی داواكردنه‌كانم ببینیت كه‌ له‌ لیستی لا ته‌نیشته‌",
    Payment: "پاره‌دان",
    Complete: "جێبه‌جێكراوه‌",
    EnterYourFirstName: "ناوی یه‌كه‌مت بنووسه‌",
    EnterYourLastName: "پاشناوت بنووسه‌",
    EnterYourEmail: "ئیمێڵه‌كه‌ت بنووسه‌",
    EnterYourPhone: "ژماره‌ی په‌یوه‌ندیت بنووسه‌",
    EnterYourAddress: "ناونیشانت بنووسه‌",
    CreateOrderError: "داواكردنه‌كه‌ت سه‌ركه‌وتوو نه‌بوو تكایه‌ دووباره‌ هه‌وڵ بده‌",
    AccountNumner: "ژماره‌ی هه‌ژمار",
    CardHolderName: "ناوی هه‌ڵگری كارت",
    ExpirationDate: "به‌رواری به‌سه‌رچوون",
    SecurityCode: "سی ڤی ڤی",

    // myorder
    OrderId:" ژماره‌ی ناسنامه‌ی گه‌یاندن",
    MyOrder: "داواكردنه‌كانم",
    NoOrder: "داواكردنت نیه‌",
    OrderDate: "به‌رواری داواكردن",
    OrderStatus: "ڕه‌وش",
    OrderPayment: "رێگه‌ی  پاره‌دان",
    OrderTotal: "كۆی داواكردنه‌كان",
    OrderDetails: "ورده‌كارییه‌كانی داوكاردنه‌كان",
    ShippingAddress: "ناونیشانی گه‌یاندن",
    Refund: "گه‌ڕاندنه‌وه‌ی پاره‌",

    PostDetails: "ورده‌كارییه‌كانی پۆست",
    FeatureArticles: "وتاری تایبه‌ت",
    MostViews: "خاوه‌نی زۆرترین بینین",
    EditorChoice: "هه‌ڵبژارده‌ی سه‌رنووسه‌ر",

    // settings
    Settings: "دانان",
    BASICSETTINGS: "دانانه‌ بناغه‌ییه‌كان",
    Language: "زمان",
    INFO: "زانیارییه‌كان",
    About: "ده‌رباره‌ی ئێمه‌",
    changeRTL: "RTL گۆڕینی",

    // language
    AvailableLanguages: "زمانه‌كان",
    SwitchLanguage: "گۆڕینی زمان",
    SwitchLanguageConfirm:
      "گۆڕینی زمان پێویستی به‌ ریفرێش كردنه‌، به‌رده‌وام ده‌بیت؟",
    SwitchRtlConfirm:
      "گۆڕینی RTL پێویستی به‌ ریفرێش كردنه‌، به‌رده‌وام ده‌بیت؟",

    // about us
    AppName: "ئێم ستۆر",
    AppDescription: "",
    AppContact: " په‌یوه‌ندیمان پێوه‌ بكه‌ن له‌ : trendi.iq",
    AppEmail: "support@trendi.iq",
    AppCopyRights: "© Trendi 2020",

    // contact us
    contactus: "په‌یوه‌ندیمان پێوه‌ بكه‌ن",
    instagram:" ئينستاگرام",

    // form
    NotSelected: "هه‌ڵبژاردن نه‌كراوه‌",
    EmptyError: "ئه‌م خانه‌یه‌ به‌تاڵه‌",
    DeliveryInfo: "زانیاری گه‌یاندن",
    FirstName: "ناو",
    LastName: "پاشناو",
    Address: "ناونیشان",
    Area: "ڕووبەر",
    City: "شار",
    State: "پارێزگا",
    NotSelectedError: "تكایه پارێزگایه‌ك هه‌ڵبژێره‌",
    Postcode: "پۆست كۆدی پارێزگاكه‌ت",
    Country: "وڵات",
    Email: "ئیمێڵ",
    Phone: "ژماره‌ی په‌یوه‌ندی",
    Note: "تێبینی",

    // search
    Search: "بگه‌ڕێ",
    SearchPlaceHolder: "بگه‌ڕێ بۆ ناوی به‌رهه‌م",
    NoResultError: "هیچ به‌رهه‌مێك نیه‌ به‌و ناوه‌",
    Details: "ورده‌كارییه‌كان",

    // filter panel
    Categories: "به‌شه‌كان",

    // sign up
    profileDetail: "ورده‌كارییه‌كانی پڕۆفایل",
    firstName: "ناو",
    lastName: "پاشناو",
    accountDetails: "ورده‌كارییه‌كانی هه‌ژمار",
    username: "ناوی به‌كارهێنه‌ر",
    address:"ناوونيشان",
    email: "ئیمێڵ",
    generatePass: "وشه‌ی نهێنی دروست بكه‌ ",
    password: "وشه‌ی نهێنی ",
    cpassword:"وشەى نهێنى تەئكيد بكەوە",
    signup: "هه‌ژمار دروست بكه‌",
    signup2: "هه‌ژمار دروست بكه‌",
    login2:"هه‌ژمار دروست بكه‌",
    already:"هەرئێستا تۆمارى كرد",


    // filter panel
    Loading: "چاوه‌ڕوان به‌",
    welcomeBack: "به‌خێر بێیته‌وه‌!",
    seeAll: "هه‌مووی پیشان بده‌",

    // Layout
    cardView: "كارت",
    simpleView: "بینین به‌ شێوازی لیست",
    twoColumnView: "دوو ستوون",
    threeColumnView: "سێ ستوون",
    listView: "بینین به‌ شێوازی لیست",
    default: "شێوازی ئاسایی",
    advanceView: "پێشكه‌وتوو",
    horizontal: "شێوازی ئاسۆیی",

    couponCodeIsExpired: "كۆدی كوپۆنه‌كه‌ به‌سه‌رچووه‌",
    alreadyAvailed:"كۆبۆن هەرئێستا كۆد دەكات سووددەبەخشێت",
    invalidCouponCode: "كۆدی كوپۆنه‌كه‌ دروست نیه‌",
    remove: "بسڕه‌وه‌",
    applyCouponSuccess: "ده‌ستخۆش! كۆدی كوپۆنه‌كه په‌سه‌ند كرا",
    reload: "ریفرێش بكه‌",

    OutOfStock: "ئه‌و به‌رهه‌مه‌ نه‌ماوه‌",
    ShippingType: "رێگه‌ی بازاڕی كردن",
    ShippingDuration:"بار كردن ماوە",
    ShippingDurationText:"گەياندنى چاوەڕێ كرد لە نێو ٦-١٤ ڕۆژ كار دەكەن (يەكشەممە - پێنج شەممە) پشت بەستن بە لەسەر هەبوونى كۆگا",

    // Place holder
    TypeFirstName: "ناوی خۆت بنووسه‌",
    TypeLastName: "پاشناوی خۆت بنووسه‌",
    TypeAddress: "ناونیشانت بنووسه‌",
    TypeCity: "ناوی شاره‌كه‌ت",
    TypeState: "ناوی وڵاته‌كه‌ت",
    TypeNotSelectedError: "تكایه‌ ناوی شار هه‌ڵبژێره‌",
    TypePostcode: "پۆست كۆدی شاره‌كه‌ت",
    TypeEmail: "ئیمێڵ (نموونه‌: abc@gmail.com)",
    TypePhone: "ژماره‌ی په‌یوه‌ندیت بنووسه‌",
    TypeNote: "تێبینی",
    TypeCountry: "وڵات هه‌ڵبژێره‌",
    SelectPayment: "رێگه‌ی پاره‌دان",
    close: "دابخه‌",
    noConnection: "ئینته‌رنێت نیه‌!",
    helpPhone:"رقم الهاتف مفقود",
    helpCity:"شار بزر بوون",


    // user profile screen
    AccountInformations: "زانیارییه‌كانی هه‌ژمار",
    PushNotification: " نۆتیفیكه‌یشن",
    DarkTheme: "ثیمی رەش",
    Privacy: "رامیارییه‌كانی پاراستنی نهێنییه‌كان",
    SelectCurrency: "هه‌ڵبژاردنی دراو",
    Name: "ناو",
    Currency: "دراو",
    Languages: "زمان",

    GetDataError: "ناتوانین زانیارییه‌كان له‌ سێرڤه‌ر وه‌ربگرین",
    UserOrEmail: "ناوی به‌كارهێنه‌ر یان ئیمێڵ",
    Or: "یان",
    FacebookLogin:"له‌ رێگه‌ی فه‌یسبوك بچۆ ژووره‌وه‌",
    DontHaveAccount: "هه‌ژمارت نیه‌؟",
    forgetPassword:"وشەى نهێنى لەياد بكە",


    // Horizontal
    flashSale: "داشکاندن",
    bagsCollections: "جانتاكان",
    womanBestSeller: "باشترین كاڵاكانی خانمان",
    mostPopular: "خۆشەویسترین کاڵ",

    // Modal
    Select: "هه‌ڵبژێره‌",
    Cancel: "پوچه‌ڵ بكه‌ره‌وه‌",
    Guest: "میوان",
    changePassword:"وشەى نهێنى بگۆڕە",

    LanguageName: "ئینگلیزی",

    // review
    vendorTitle: "فرۆشیار",
    comment: "بۆچوونت بنووسه‌",
    yourcomment: "كۆمێنت بنووسه‌",
    placeComment: "وه‌كوو به‌كارهێنه‌ر، باسی ئه‌زموونی خۆت  بكه‌ یان ئامۆژگاریه‌ك بۆ كه‌سانی دیكه‌ بنووسه‌",
    writeReview: "بۆچوونه‌كانت",
    thanksForReview:
      "سوپاس بۆ بۆچوونه‌كه‌ت. نووسراوه‌كه‌ت له‌ لایه‌ن به‌ڕێوبه‌ر وردبینی بۆ ده‌كرێت و پاشان لێره‌ پۆست ده‌كرێت",
    errInputComment: "تكایه‌ كۆمێنته‌كه‌ت لێره‌ بنووسه‌",
    errRatingComment: "ره‌یتینگی خۆت بده‌",
    send: "بنێره‌",

    termCondition: "هه‌ل و مه‌رجه‌كان",
    Subtotal: "نرخی به‌رهه‌مه‌كان",
    Discount: "داشكاندن",
    Shipping: "گه‌یاندن",
    Recents: "نوێكان",
    Filters: "فلته‌ره‌كان",
    Pricing: "نرخه‌كان",
    Filter: "فلته‌ر",
    ClearFilter: "لابردنی فلته‌ر",
    ProductCatalog: "كه‌ته‌لۆگی به‌رهه‌مه‌كان",
    ProductTags: "تاگی به‌رهه‌مه‌كان",
    AddToAddress: "بیخه‌ره سه‌ر ناونیشان",
    SMSLogin:"چوونه‌ ژووره‌وه‌ به‌ نامه‌ی كورت",
    OrderNotes: "تێبینی له‌سه‌ر داواكردنه‌كان",

    selectSize:"قەبارە هەڵ ببژێرە",
    selectColor:"ڕەنگ هەڵ ببژێرە"
  },
});
