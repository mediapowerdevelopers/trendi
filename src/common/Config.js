/** @format */

import Images from "./Images";
import Constants from "./Constants";
import Icons from "./Icons";
import Languages from "./Languages";
import { I18nManager } from "react-native";

// function checkUrl(){
//   //setTimeout(() => {
//     alert(Languages._language);
//    return I18nManager.isRTL?(Languages._language==='ar'?"https://trendi-iq.com/shipping-policy-arabic/":"https://trendi-iq.com/shipping-policy-kurdish/"):"https://trendi-iq.com/shipping-policy/";
//  // }, 100);
// }


export default {
  /**
   * Step 1: change to your website URL and the wooCommerce API consumeKey
   * Moved to AppConfig.json
   */

  /**
     Step 2: Setting Product Images
     - ProductSize: Explode the guide from: update the product display size: https://mstore.gitbooks.io/mstore-manual/content/chapter5.html
     The default config for ProductSize is disable due to some problem config for most of users.
     If you have success config it from the Wordpress site, please enable to speed up the app performance
     - HorizonLayout: Change the HomePage horizontal layout - https://mstore.gitbooks.io/mstore-manual/content/chapter6.html
       Update Oct 06 2018: add new type of categories
       NOTE: name is define value --> change field in Language.js
       Moved to AppConfig.json
     */
  ProductSize: {
    enable: true,
  },

  MainCategories: [
    {
      category: 28,
     // image: require("@images/categories_icon/ic_dress.png"),
      colors: ["#F8CB53", "#fff"],
      name:Languages.WomenCat
    },
    {
      category: 29,
    //  image: require("@images/categories_icon/ic_tops.png"),
      colors: ["#1AE393", "#fff"],
      name:Languages.MenCat
    },
    {
      category: 30,
    //  image: require("@images/categories_icon/ic_jacket.png"),
      colors: ["#FF4D77", "#fff"],
      name:Languages.KidsCat
    },
    // {
    //   category: 31,
    // //  image: require("@images/categories_icon/ic_bottoms.png"),
    //   colors: ["#B251FB", "#fff"],
    // }
   ],

   

  HomeCategories: [
    {
      category: 64,
      //image: {'url':'http://dev80.onlinetestingserver.com/wp/wp-content/uploads/2019/11/double-king-size-bed@3x-small.png'},
      colors: ["#B251FB", "#fff"],
    },
    {
      category: 65,
      //image: require("@images/categories_icon/ic_tops.png"),
      colors: ["#1AE393", "#fff"],
    },
    {
      category: 66,
      //image: require("@images/categories_icon/ic_jacket.png"),
      colors: ["#FF4D77", "#fff"],
    },
    {
      category: 67,
      //image: require("@images/categories_icon/ic_bottoms.png"),
      colors: ["#F8CB53", "#fff"],
    },
  ],
  WomensCategories: [
    {
      category:187,
      //image: require("@images/categories_icon/ic_dress.png"),
      colors: ["#F8CB53", "#fff"],
    },
    {
      category: 188,
      //image: require("@images/categories_icon/ic_tops.png"),
      colors: ["#1AE393", "#fff"],
    },
    {
      category: 189,
      //image: require("@images/categories_icon/ic_jacket.png"),
      colors: ["#FF4D77", "#fff"],
    },
    {
      category: 190,
     // image: require("@images/categories_icon/ic_bottoms.png"),
      colors: ["#B251FB", "#fff"],
    },
    {
      category: 110,
     // image: require("@images/categories_icon/ic_shoes.png"),
      colors: ["#63ACF5", "#fff"],
    },
    {
      category: 191,
     // image: require("@images/categories_icon/ic_bottoms.png"),
      colors: ["#3CDB12", "#fff"],
    },
    {
      category: 192,
     // image: require("@images/categories_icon/ic_bottoms.png"),
      colors: ["#E51B1B", "#fff"],
    },
    {
      category: 134,
     // image: require("@images/categories_icon/ic_bottoms.png"),
      colors: ["#25D6B6", "#fff"],
    },
   
  ],
  KidsCategories: [
    {
      category: 33,
      //image: require("@images/categories_icon/ic_dress.png"),
      colors: ["#F8CB53", "#fff"],
    },
    {
      category: 201,
     // image: require("@images/categories_icon/ic_tops.png"),
      colors: ["#FF4D77", "#fff"],
    },
    {
      category: 202,
     // image: require("@images/categories_icon/ic_jacket.png"),
      colors: ["#1AE393", "#fff"],
    },
    
    {
      category: 115,
     // image: require("@images/categories_icon/ic_shoes.png"),
      colors: ["#63ACF5", "#fff"],
    },
    {
      category: 32,
     // image: require("@images/categories_icon/ic_bottoms.png"),
      colors: ["#E51B1B", "#fff"],
    },
    {
      category: 203,
     // image: require("@images/categories_icon/ic_jacket.png"),
      colors: ["#E16AF9", "#fff"],
    },
    {
      category: 204,
     // image: require("@images/categories_icon/ic_jacket.png"),
      colors: ["#B66AF9", "#fff"],
    },
    {
      category: 120,
     // image: require("@images/categories_icon/ic_jacket.png"),
      colors: ["#6AF7F9", "#fff"],
    },
   
  ],
  KidsGirls: [
    {
      category: 116,
      //image: require("@images/categories_icon/ic_dress.png"),
      colors: ["#F8CB53", "#fff"],
    },
    {
      category: 117,
     // image: require("@images/categories_icon/ic_tops.png"),
      colors: ["#FF4D77", "#fff"],
    },
    {
      category: 118,
     // image: require("@images/categories_icon/ic_jacket.png"),
      colors: ["#1AE393", "#fff"],
    },
    {
      category: 119,
     // image: require("@images/categories_icon/ic_bottoms.png"),
      colors: ["#B251FB", "#fff"],
    },
    {
      category: 120,
     // image: require("@images/categories_icon/ic_shoes.png"),
      colors: ["#63ACF5", "#fff"],
    },
   
  ],
  KidsBoys: [
    {
      category: 111,
      //image: require("@images/categories_icon/ic_dress.png"),
      colors: ["#F8CB53", "#fff"],
    },
    {
      category: 112,
     // image: require("@images/categories_icon/ic_tops.png"),
      colors: ["#FF4D77", "#fff"],
    },
    {
      category: 113,
     // image: require("@images/categories_icon/ic_jacket.png"),
      colors: ["#1AE393", "#fff"],
    },
    {
      category: 114,
     // image: require("@images/categories_icon/ic_bottoms.png"),
      colors: ["#B251FB", "#fff"],
    },
    {
      category: 115,
     // image: require("@images/categories_icon/ic_shoes.png"),
      colors: ["#63ACF5", "#fff"],
    },
   
  ],
  KidsMainCategories:[
    {
    category:32,
    },
    {
      category:33,
    }
],
  MensCategories: [
    {
      category: 193,
     // image: require("@images/categories_icon/ic_dress.png"),
      colors: ["#B251FB", "#fff"],
    },
   
    {
      category: 194,
    //  image: require("@images/categories_icon/ic_jacket.png"),
      colors: ["#FF4D77", "#fff"],
    },
    {
      category: 195,
    //  image: require("@images/categories_icon/ic_bottoms.png"),
      colors: ["#7AF35A", "#fff"],
    },
    {
      category: 196,
    //  image: require("@images/categories_icon/ic_bottoms.png"),
      colors: ["#5E95F6", "#fff"],
    },
    {
      category: 197,
    //  image: require("@images/categories_icon/ic_bottoms.png"),
      colors: ["#F3EA5A", "#fff"],
    },
    {
      category: 45,
    //  image: require("@images/categories_icon/ic_shoes.png"),
      colors: ["#63ACF5", "#fff"],
    },
    {
      category: 42,
    //  image: require("@images/categories_icon/ic_shoes.png"),
      colors: ["#F3AD5A", "#fff"],
    },
    {
      category: 47,
    //  image: require("@images/categories_icon/ic_shoes.png"),
      colors: ["#B7F35A", "#fff"],
    },
    {
      category: 43,
    //  image: require("@images/categories_icon/ic_tops.png"),
      colors: ["#1AE393", "#fff"],
    },
   
  ],
  
  /**
     step 3: Config image for the Payment Gateway
     Notes:
     - Only the image list here will be shown on the app but it should match with the key id from the WooCommerce Website config
     - It's flexible way to control list of your payment as well
     Ex. if you would like to show only cod then just put one cod image in the list
     * */
  Payments: {
    cod: require("@images/payment_logo/cash_on_delivery.png"),
    paypal: require("@images/payment_logo/PayPal.png"),
    stripe: require("@images/payment_logo/stripe.png"),
  },

  /**
     Step 4: Advance config:
     - showShipping: option to show the list of shipping method
     - showStatusBar: option to show the status bar, it always show iPhoneX
     - LogoImage: The header logo
     - LogoWithText: The Logo use for sign up form
     - LogoLoading: The loading icon logo
     - appFacebookId: The app facebook ID, use for Facebook login
     - CustomPages: Update the custom page which can be shown from the left side bar (Components/Drawer/index.js)
     - WebPages: This could be the id of your blog post or the full URL which point to any Webpage (responsive mobile is required on the web page)
     - intro: The on boarding intro slider for your app
     - menu: config for left menu side items (isMultiChild: This is new feature from 3.4.5 that show the sub products categories)
     * */
  shipping: {
    visible: true,
    zoneId: 1, // depend on your woocommerce
    time: {
      free_shipping: "4 - 7 Days",
      flat_rate: "1 - 4 Days",
      local_pickup: "1 - 4 Days",
    },
  },
  showStatusBar: true,
  LogoImage: require("@images/logo-main.png"),
  LogoWithText: require("@images/logo_with_text.png"),
  LogoLoading: require("@images/logo.png"),

  showAdmobAds: false,
  AdMob: {
    deviceID: "pub-2101182411274198",
    rewarded: "ca-app-pub-2101182411274198/5096259336",
    interstitial: "ca-app-pub-2101182411274198/8930161243",
    banner: "ca-app-pub-2101182411274198/4100506392",
  },
  appFacebookId: "436993056989897",
  CustomPages: { contact_id: 10941 },
  WebPages: { marketing: "http://trendi-iq.com" },

  intro: [
    {
      key: "page1",
      title: "Welcome to Trendi IQ!",
      text: "Get the hottest fashion by trend and season right on your pocket.",
      icon: "ios-basket",
      colors: ["#f6f6f6", "#171a1b"],
    },
    {
      key: "page2",
      title: "Secure Payment",
      text: "All your payment infomation is top safety and protected",
      icon: "ios-card",
      colors: ["#f6f6f6", "#171a1c"],
    },
    {
      key: "page3",
      title: "High Performance",
      text: "Saving your value time and buy product with ease",
      icon: "ios-finger-print",
      colors: ["#f6f6f6", "#171a1d"],
    },
  ],

  intro_newEN: [
    {
      key: "page1",
      image: require("./../images/intro/home/home_screen.png")
    },
    {
      key: "page2",
      image: require("./../images/intro/flash_sale/flash_sale.png")
    },
    {
      key: "page3",
      image: require("./../images/intro/product_screen/product_screen.png")
    },
    {
      key: "page4",
      image: require("./../images/intro/cart_screen/cart_screen.png")
    },
    {
      key: "page5",
      image: require("./../images/intro/shipping_info/shipping_info.png")
    },
    {
      key: "page6",
      image: require("./../images/intro/order_confirmation/order_confirmation.png")
    }
  ],

  intro_newAR: [
    {
      key: "page1",
      image: require("./../images/intro/home/Arabic_home_screen.png")
    },
    {
      key: "page2",
      image: require("./../images/intro/flash_sale/Arabic_flash_sale.png")
    },
    {
      key: "page3",
      image: require("./../images/intro/product_screen/Arabic_product_screen.png")
    },
    {
      key: "page4",
      image: require("./../images/intro/cart_screen/Arabic_cart_screen.png")
    },
    {
      key: "page5",
      image: require("./../images/intro/shipping_info/Arabic_shipping_info.png")
    },
    {
      key: "page6",
      image: require("./../images/intro/order_confirmation/Arabic_order_confirmation.png")
    }
  ],

  intro_newKR: [
    {
      key: "page1",
      image: require("./../images/intro/home/Kurdish_home_screen.png")
    },
    {
      key: "page2",
      image: require("./../images/intro/flash_sale/Kurdish_flash_sale.png")
    },
    {
      key: "page3",
      image: require("./../images/intro/product_screen/Kurdish_product_screen.png")
    },
    {
      key: "page4",
      image: require("./../images/intro/cart_screen/Kurdish_cart_screen.png")
    },
    {
      key: "page5",
      image: require("./../images/intro/shipping_info/Kurdish_shipping_info.png")
    },
    {
      key: "page6",
      image: require("./../images/intro/order_confirmation/Kurdish_order_confirmation.png")
    }
  ],

  /**
   * Config For Left Menu Side Drawer
   * @param goToScreen 3 Params (routeName, params, isReset = false)
   * BUG: Language can not change when set default value in Config.js ==> pass string to change Languages
   */
  menu: {
    // has child categories
    isMultiChild: true,
    // Unlogged
    listMenuUnlogged: [
      {
        text: "Login",
        routeName: "LoginScreen",
        params: {
          isLogout: false,
        },
        icon: Icons.MaterialCommunityIcons.SignIn,
      },
    ],
    // user logged in
    listMenuLogged: [
      
      {
        text: "Logout",
        routeName: "LoginScreen",
        params: {
          isLogout: true,
        },
        icon: Icons.MaterialCommunityIcons.SignOut,
      },
     
    ],
    // Default List
    listMenu: [
      {
        text: "Home",
        routeName: "Home",
        icon: Icons.MaterialCommunityIcons.Home,
      },
      // {
      //   text: "News",
      //   routeName: "NewsScreen",
      //   icon: Icons.MaterialCommunityIcons.News,
      // },
      {
        text: "AboutUs",
        routeName: "CustomPage",
        params: {
          //id: 2017,
          url:I18nManager.isRTL?(Languages._language==='ar'?"https://trendi-iq.com/about-us-arabic/":"https://trendi-iq.com/about-us-kurdish/"):"https://trendi-iq.com/about-us/",
          title: "aboutus",
        },
        icon: Icons.MaterialCommunityIcons.Pin,
      },
      // {
      //   text: "forget password",
      //   routeName: "CustomPage",
      //   params: {
      //     url:"https://dev80.onlinetestingserver.com/wp/forget-password",
      //   },
      //   icon: Icons.MaterialCommunityIcons.Pin,
      // },
      {
        text: "ShippingPolicy",
        routeName: "CustomPage",
        params: {
          //id: 2009,
          url:I18nManager.isRTL?(Languages._language==='ar'?"https://trendi-iq.com/shipping-policy-arabic/":"https://trendi-iq.com/shipping-policy-kurdish/"):"https://trendi-iq.com/shipping-policy/",
          title: "shipping",
        },
        icon: Icons.MaterialCommunityIcons.Pin,
      },
      {
        text: "PrivacyPolicy",
        routeName: "CustomPage",
        params: {
          //id: 3,
          url:I18nManager.isRTL?(Languages._language==='ar'?"https://trendi-iq.com/privacy-policy-arabic/":"https://trendi-iq.com/privacy-policy-kurdish/"):"https://trendi-iq.com/privacy-policy/",
          title: "privacy",
        },
        icon: Icons.MaterialCommunityIcons.Pin,
      },
      {
        text: "TermsAndConditions",
        routeName: "CustomPage",
        params: {
          //id: 2010,
          url:I18nManager.isRTL?(Languages._language==='ar'?"https://trendi-iq.com/terms-conditions-arabic/":"https://trendi-iq.com/terms-conditions-kurdish/"):"https://trendi-iq.com/terms-conditions/",
          title: "terms",
        },
        icon: Icons.MaterialCommunityIcons.Pin,
      },
      {
        text: "ReturnAndExchange",
        routeName: "CustomPage",
        params: {
         // id: 2008,
          url:I18nManager.isRTL?(Languages._language==='ar'?"https://trendi-iq.com/return-exchange-arabic/":"https://trendi-iq.com/return-exchange-kurdish/"):"https://trendi-iq.com/return-exchange/",
          title: "return",
        },
        icon: Icons.MaterialCommunityIcons.Pin,
      },
      // {
      //   text: "About",
      //   routeName: "CustomPage",
      //   params: {
      //     url: "http://inspireui.com/about/",
      //   },
      //   icon: Icons.MaterialCommunityIcons.Email,
      // },
    ],
  },

  // define menu for profile tab
  ProfileSettings: [
    {
      label: "WishList",
      routeName: "WishListScreen",
    },
    {
      label: "MyOrder",
      routeName: "MyOrders",
    },
    {
      label: "Address",
      routeName: "Address",
    },
    // {
    //   label: "Currency",
    //   isActionSheet: false,
    // },
    // only support mstore pro
    // {
    //   label: "Languages",
    //   routeName: "SettingScreen",
    // },
    // {
    //   label: "PushNotification",
    // },
    // {
    //   label: "DarkTheme",
    // },
    {
      label: "contactus",
      isWhatsapp:true
    },
    {
      label: "instagram",
      isInsta:true
    },
    // {
    //   label: "Privacy",
    //   routeName: "CustomPage",
    //   params: {
    //     url: "https://inspireui.com/privacy",
    //   },
    // },
    // {
    //   label: "termCondition",
    //   routeName: "CustomPage",
    //   params: {
    //     url: "https://inspireui.com/term-of-service",
    //   },
    // },
    // {
    //   label: "About",
    //   routeName: "CustomPage",
    //   params: {
    //     url: "http://inspireui.com",
    //   },
    // },
  ],

  // Homepage Layout setting
  layouts: [
    {
      layout: Constants.Layout.card,
      image: Images.icons.iconCard,
      text: "cardView",
    },
    {
      layout: Constants.Layout.simple,
      image: Images.icons.iconRight,
      text: "simpleView",
    },
    {
      layout: Constants.Layout.twoColumn,
      image: Images.icons.iconColumn,
      text: "twoColumnView",
    },
    {
      layout: Constants.Layout.threeColumn,
      image: Images.icons.iconThree,
      text: "threeColumnView",
    },
    {
      layout: Constants.Layout.horizon,
      image: Images.icons.iconHorizal,
      text: "horizontal",
    },
    {
      layout: Constants.Layout.advance,
      image: Images.icons.iconAdvance,
      text: "advanceView",
    },
  ],

  // Default theme loading, this could able to change from the user profile (reserve feature)
  Theme: {
    isDark: false,
  },

  // new list category design
  CategoriesLayout: Constants.CategoriesLayout.card,

  // WARNING: Currently when you change DefaultCurrency, please uninstall your app and reinstall again. The redux saved store.
  DefaultCurrency: {
    symbol: "IQD ",
    name: "Iraqi Dinar",
    code: "IQD",
    name_plural: "Iraqi Dinars",
    decimal: ".",
    thousand: ",",
    precision: 0,
    format: "%s%v", // %s is the symbol and %v is the value
  },

  DefaultCountry: {
    code: "en",
    RTL: false,
    language: "English",
    countryCode: "IQ",
    hideCountryList: true, // when this option is try we will hide the country list from the checkout page, default select by the above 'countryCode'
  },

  /**
   * Config notification onesignal, only effect for the Pro version
   */
  OneSignal: {
    appId: "746c2024-9118-4be8-a1ce-06c84936ffa3",
  },
  //
  //6c71628d-a782-4ab5-bcc2-1fe631982477 sg developer account
  //43948a3d-da03-4e1a-aaf4-cb38f0d9ca51 mstore
  /**
   * Login required
   */
  Login: {
    RequiredLogin: false, // required before using the app
    AnonymousCheckout: false, // required before checkout or checkout anonymous
  },

  Layout: {
    HideHomeLogo: true,
    HideLayoutModal: false
  },

  Affiliate: { enable: false }
};
